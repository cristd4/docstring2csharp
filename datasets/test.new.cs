public override void Serialize(ILittleEndianOutput out1){out1.Write(field_1_venter);}
public virtual void addAll(IList<E> src){for (int i = 0; i < src.count; i++){System.Array.Copy(src, i, this._enclosing.elements, i + 1, src.count);i += src[i];}}
public override void write(byte b){throw new System.NotImplementedException();}
public virtual ObjectId GetObjectId(){return ObjectId.FromRaw(this.Bytes, IdOffset, Length);}
public virtual void Delete(){domainTable.Delete(this.UserName);domainTable.Delete(this.Password);}
public virtual int BytesUsed(){return termSize;}
public virtual IMessage GetFullMessage(){return GetFullMessage();}
public POIFSFileSystem Create(File path){throw new System.NotImplementedException();}
public virtual void initialize(byte b){throw new System.NotImplementedException();}
public virtual void SetSubmodulePath(string path){this.m_path = path;}
public virtual IList<Ingress> GetIngredients(){return new List<Ingress>(1);}
public TokenManager(ICharStream stream){this.m_c = stream;this.m_state = State.INIT;this.m_pos = 0;this.m_err = 0;this.m_last = null;this.m_lastPos = -1;this.m_errState = null;}
public override Sharpen.Iterator<T> GetIterator(int shard){return new _ShardIterator_276(this, shard);}
public ModifyStrategyRequest(): base("{strategyName}", StringComparison.Ordinal) {return ModifyStrategyRequest();}
public override bool ready(){return false;}
public String GetOptRecord(){return optRecord;}
public int ReadLengthBytes(BytesRef bs){return bs.ReadUpto(this, littleEndian.GetInt(bs, offset + LEN_BYTES_INT32)).Length;}
public IEnumerator<NLPSentenceBreak> GetBreakIterator(int start, int end){return new NLPSentenceBreakIterator(start, end);}
public virtual void print(string @string){print(@string);}
public NotImplementedFunctionException(string functionName, Exception cause){throw new NotImplementedFunctionException(functionName);}
public virtual E peek(){E e = this.nextEntryInTheList;return e == null? E_NEXT : e.value;}
public int Read(byte[] buf, int offset, int len){return Read(buf, offset, len);}
public virtual void Tag(){this.enq = true;this.enq = true;}
public virtual void clear(){this._enclosing.clear();}
public virtual void ModifyCacheSubnetGroup(string subnetId){cacheSubnetGroupId = subnetId;}
public void SetLanguageAndVariantParameters(string language){this.language = language;}
public virtual void Delete(){this.nextEntry = null;this.previousEntry = null;this.nextEntry = null;}
public virtual bool Equals(object o){return o == this;}
public virtual InstanceInformation GetInstanceInformation(){return InstanceInformation.FromRaw(GetInstanceInformation())}
public Polygon Create(IList<IPoint> points){return new Polygon(points);}
public String GetSheetName(int index){return null;}
public virtual Dashboards Describe(){return dashboards;}
public void Associate(AWSAccount account){regions[0] = account;}
public void AddBlanks(IList<Blank> blanks){_blanks = blanks;}
public virtual string quote(string @string){return "'" + @string + "'";}
public void WriteInt(int value){_limit += 1;}
public void Set(int x, int y){if (x < 0 || y < 0){throw new System.IndexOutOfRangeException(x, y);}if (x < 0){throw new System.IndexOutOfRangeException(x, y);}
public IceServerConfiguration GetServerConfiguration(){return IceServerConfiguration.Default;}
public override string ToString(){return this.ToString(CultureInfo.InvariantCulture);}
public override string ToString(){return this.ToString() + "(join " + this.join + ")";}
public virtual void incRefCount(int value){count++;}
public virtual void Update(void){base.Update(null);}
public virtual int GetXBATOffset(){return 0;}
public BigInteger Multiply(BigInteger number, int powerOfTen){return Multiply(number, powerOfTen);}
public virtual string ToString(){return "File " + fullPath;}
public credentials fetcher(CredentialsFetcher fetcher){_credentialsFetcher = fetcher;}
public virtual void SetProgressMonitor(ProgressMonitor m){this.m_progressMonitor = m;}
public virtual void Reset(){this.m_parser = null;this.m_state = STATE_IDLE;this.m_parseState = null;this.m_lastError = -1;this.m_lastIndex = -1;this.m_lastIndex = -1;this.m_lastIndex = -1;this.m_lastIndex = -1;this.m_lastIndex = -1;this.m_parseState = STATE_IDLE;}
public virtual E previous(){return this.next();}
public virtual string GetNewPrefix(){return newPrefix;}
public int Get(int index){if (index < 0){return -1;}return map[index] = value;}
public virtual IList<string> GetUniqueStems(string word){return stemsOf(word);}
public virtual GatewayResponse ListGatewayResponses(){return gatewayResponses;}
public override void SetFilePointer(long filePointer){numberOfLines = 0;}
public override void Skip(int count){if (count < skip){return;}int bytesToSkip = skip(count);}
public BootstrapActionConfig SetBootstrapActionConfig(BootstrapActionConfig config){this._bootstrapActionConfig = config;}
public override void Serialize(Shapeid o){if (o is LittleEndianOutput){out1.Write(o);}}
public virtual int lastIndexOf(char c){if (c == 0){return -1;}return lastIndexOf(c, 1);}
public virtual void push(E e){if (this.e == null){this.e = e;this.w = this._enclosing.push(e);this.e = this._enclosing.pop(e);this.n = this._enclosing.push(e);}}
public void unset(string section){throw new System.NotImplementedException();}
public virtual string GetTag(){return null;}
public void AddSubRecord(Record record){this._subRecords.Add(record);}}
public virtual void remove(object o){if (!(o is java.util.MapClass.Entry<K, V>)){remove(o);}}
public override TreeFilter Create(TreeFilter parent){return new DoubleMetaphoneFilter(parent);}
public override long GetSize(){return file.GetSize();}
public void setValue(V value){_value = value;}
public pair of content sources. {1, 2, 3}
public V get(int index){if (index < 0 || index >= values.Length){throw new IndexOutOfRangeException(index.ToString());}V value = values[index];if (value == null){throw new ArgumentException("value is null"); }return value;}
public virtual RepositoryRequest SetRequestParameters(RepositoryRequest request){var options = new InvokeOptions();options.RequestMarshaller = RepositoryRequestMarshaller.Instance;options.ResponseUnmarshaller = RepositoryResponseUnmarshaller.Instance;return Invoke<RepositoryRequest>(request, options);}
public virtual bool IsDeltaBaseAsOffset(Vector2d other){return IsDeltaBase(other);}
public virtual void clear(){list.clear();}
public virtual MergeShardsResponse Merge(MergeShardsResponse * sharpen) {return MergeShards(sharpen.Collections.Generic.List<MergeShardsResponse>(sharpen.Collections.UnmodifiableList(sharpen.Collections.UnmodifiableList(sharpen.Collections.NormalNormalised(sharpen.Extensions.ValueOf(sharpen.Extensions.ValueOf(sharpen.Extensions.ValueOf(Sharpen.Extensions.ValueOf(sharpen)));}
public HostedConnection AllocatedConnection(HostedConnection hostConnection){return hostConnection.AllocateConnection(hostConnection);}
public int startOfRange(){return startOfRange;}
public override IList<WeightedTerm> GetWeights(WeightedQuery query){IList<WeightedTerm> terms = new List<WeightedTerm>();foreach (WeightedQuery other in query){if (other.m_weight == WeightedQuery.DEFAULT_WEIGHT){terms.Add(other);}}return terms;}
public java.nio.ByteBuffer getCompactedBuffer(){return this._le.getCompactedBuffer();}
public override void Decode(int n){if (n < 1){throw new System.ArgumentException("Bad number of bytes: " + n);}if (n > 1){throw new System.ArgumentException("Bad number of bytes: " + n);}
public string getName(){return path.GetName();}
public virtualNotebookInstanceLifecycleConfig GetNotebookInstanceLifecycleConfig(){return aNotebookInstanceLifecycleConfig;}
public string GetAccessKeySecret(){return GetAccessKey(null);}
public VPNConnection(string VPNEndpoint){return VPNConnection(VPNEndpoint);}
public Voice List(){return Voice;}
public virtual IList<Execution> GetMonitoredExecutions(){return list(monitoredExecutions());}
public string vaultName and jobId { get; private set; }
public EscherRecord Get(int index){return null;}
public virtual DescribeApplicationsResponse DescribeApplications(){return DescribeApplications(null);}
public virtual void DeleteSmsChannel(){Dispose (this.m_channel);}
public virtual TrackingRefUpdate GetTrackingRefUpdate(){return trackingRefUpdate;}
public virtual bool print(bool b){return b? "true" : "false";}
public virtual ICharTerm GetFirstChild(){return this.FirstChild;}
public override void SetDirectoryTreeIndex(int index){workingDirectory = index;}
public override void Read(int recordID){if (recordID < 0){throw new InvalidOperationException("Invalid record ID: " + recordID + " (current record id=" + currentRecordId + ")");}if (recordID >= _areaCount){throw new InvalidOperationException("Invalid record ID: " + recordID + " (current record id=" + _areaCount + ")");}
public static GetThumbnailsRequest(): base("{protocol:" + GetProtocolVersion() + ", path:" + Path + "/thumbnails", customHeaders: customHeaders, verify : false}
public virtual ICollection<VPCAttachment> GetVPCAttachments(string gatewayName){return VPCAttachments.FromGateway(GatewayName);}
public StreamingConfiguration(StreamingConfiguration config){var options = new InvokeOptions();options.StreamingConfiguration = config;return Invoke<StreamingConfiguration>(config, options);}
public ordinal range(int start, int end){return ordinalRange(start, end);}
public virtual string ToString(){return ToString(this);}
public virtual E first(){E e = _list.first();return e;}
public virtual ListWorkspacesResponse ListWorkspaces(){return CreateWorkspaces(null, true);}
public virtual Object Clone(){return this;}
public virtual IList<Repository> GetRepositories(){return repositories;}
public static float[] New(float[] array, int capacity){if (capacity > 0){for (int i = 0; i < array.Length; i++){if (i < capacity){if (array[i] == null){if (i >= 0){if (i >= 0){if (i < 0){if (i >= array[i].Length){if (i >= 0){if (i < 0){if (i >= array[i].Length){if (i >= 0){if (array[i]){if (initialCapacity > 0){for (int i = 0; i < array.Length){if (initialCapacity > i){if ((object)array[i] == null){if (initialCapacity > i) {if (initialCapacity > i) {if (initialCapacity > i) {if (initialCapacity > i) {if (initialCapacity > i) {if (initialCapacity > i) {if (initialCapacity > i) {if (initialCapacity > i) {array[i] = array[i];}}}}}}}}}}}}}}}}}}}}}}return array;}
public Filter<HyphenatedWord>(){return new Filter<HyphenatedWord>(this);}
public virtual Distribution Create(string[] tags){return new Distribution(tags);}
public java.io.RandomAccessFile from a file name.
public virtual void DeleteImage(){this._enclosing.DeleteImage(this.getImageId());}
public override string ToString(){return "0x" + baseToString();}
public virtual void Update(){this.lastDocID = System.DateTime.UtcNow.currentDocID;this.lastDocID = System.DateTime.UtcNow.currentDocID;}
public HSLColor GetColor(int index){return GetColor(index);}
public virtual void Evaluate(){_lastEval = Evaluate(null);_lastEval = Evaluate(null);}
public override void Serialize(ObjectOutput out1){out1.Write(this.GetType().GetTypeInfo().FullName);}
public virtual DescribeDatabases(){return describeDatabases();}
public void SetFontIndex(int fontIndex){this.fontIndex = fontIndex;}
public java.nio.charset.Charset to a big-endian byte array.  class _to_big_endian_byte_array(java.nio.charset.Charset a){throw new System.NotImplementedException();}
public virtual void Upload(File file){throw new NotSupportedException(MessageFormat.Format(JGitText.Get().isAFileWithExtensions, file));}
public virtual IList<IToken> GetHiddenTokensToLeft(){return GetHiddenTokensToLeft(true);}
public virtual bool Equals(object o){return ((AutomatonQuery)o).equal(q);}
public SpanClause CreateSpanClause(WeightedSpanQuery query, GreaterThanTokenQuery other){return CreateSpanClause(query, other);}
public virtual Stash Create(IDictionary<string, object> args){return new Stash(args);}
public override FieldInfo GetField(string fieldName){return null;}
public virtual IEventSource Information(string eventName){return SourceInformation;}
public virtual AnalysisDetail GetAnalysisDetail(){return null;}
public virtual void Cancel(){_updateStack.Clear();}}
public virtual void ModifyLoadBalancerAttributes(string loadBalancerId){var loadBalancerAttributes = GetLoadBalancerAttributes(loadBalancerId);}
public virtual void SetInstanceProtection(bool enable){instanceProtection = enable;}
public virtual void Save(){this._enclosing.Save(null);}}
public java.text.StringBuilder appendRange(java.lang.CharSequence[] ranges){appendRange(ranges);return this;}
public static FetchLibrariesRequest Create(FetchLibrariesRequest request){var options = new InvokeOptions();options.RequestMarshaller = FetchLibrariesRequestMarshaller.Instance;options.ResponseUnmarshaller = FetchLibrariesRequestUnmarshaller.Instance;return new FetchLibrariesRequest(request, options);}
public virtual bool Exists(){return Exists();}
public virtual void close(){stream = null;}
public virtual void SetClusterRequestParameters(string node){if (node == null){node = new byte[0];}if (node.isCluster){return;}
public TimeConstraint(){return new TimeConstraintAnonymousInnerClassHelper(this);}
public override IList<string> GetParentPaths(){return parentPaths;}
public virtual IList<CacheSubnetGroup> GetCacheSubnetGroups(){return cacheSubnets;}
public void SetSharedFormulaFlag(bool flag){sharedFormula = flag;}
public override bool shouldReuse(){return false;}
public virtual IErrorNode AddError(){return this;}
public override StemFilter Create(int argc, int argh, int argh, int argh){return new LatvianStemFilter(argc, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh, argh_len_acc, argh_len_acc, argh_len_acc_arg_arg_arg_arg_count_acc, argh_len_acc, argh_len_acc, argh_len_acc, argh_len_acc, argh_len_acc, argh_len_acc, argh_len_acc, argh_len_acc, argh_len_acc, argh_len_acc)
public virtual void unsubscribe(){source.unsubscribe();}
public TokenFilterFactory(string name, object[] args){return new TokenFilterFactory(name, args);}
public new AddAlbumPhotosRequest(params){return new AddAlbumPhotosRequest(params);}
public ThreatIntelSet Retrieve(int threatIntelSetId){return _threatIntelSetTable[threatIntelSetId];}
public virtual TreeFilter Clone(){return this;}
public override bool IsArmenianStemmer(object o){return false;}
public bool isArray(){return value is PropertyArray;}
public virtual void Update(UpdateStruct other){throw new NotSupportedException(MessageFormat.Format(JGitText.Get().isAStaticFlagAndHasNoreferrer(), other.ToString()));}
public void UnwriteProtect(Worksheet ws){sheet.Unprotect(ws);}
public bool SetDedupAndExpand(bool useCaseSensitive){return true;}
public virtual RequestSpotInstancesResponse RequestSpotInstances(RequestSpotInstancesRequest request){var options = new InvokeOptions();options.RequestMarshaller = RequestSpotInstancesRequestMarshaller.Instance;options.ResponseUnmarshaller = RequestSpotInstancesResponseUnmarshaller.Instance;return Invoke<RequestSpotInstancesResponse>(request, options);}
public byte array(){return data;}
public virtual ContactAttributes GetAttributes(){return contactAttributes;}
public override string ToString(){return key + "=" + value;}
public virtual IList<TextTranslationJob> GetTextTranslationJobs(){return translationJobs;}
public virtual ContactMethodsResponse ContactMethods(ContactMethodResponse response){return Response.ForJson(GetContactMethodsResponse(response));}
public int GetFunctionName(String name){try{return findFunction(name, false).FunctionIndex;}catch (MissingFunctionException e){return -1;}}
public virtual AnomalyDetector[] GetAnomalyDetectors(){return anomalyDetectors;}
public virtual ObjectId Insert(ObjectId id){return new ObjectId(id).Bytes(Constants.CHARSET).Get(id).CreateObjectId();}
public override int size(){return this._enclosing._size;}
public virtual void ImportMedia(MediaStream stream){ImportMedia(stream);}}
public virtual void Update(){status = "available";}
public virtual int ReadNumber(){return BitConverter.Int64BitsToDouble(ReadByte()).Int64BitsToFloat(ReadByte());}
public virtual EncryptionConfig GetConfiguration(){return null;}
public virtual DescribeGuardDutyDetectorResponse DescribeGuardDutyDetector(){return DescribeGuardDutyDetector(null);}
public virtual void ReportInstanceStatus(){_status = InstanceStatus.NOT_APPLICABLE;}
public void DeleteAlarm(Alarm altr){if (altr.IsDead){Alarm o = (Alarm)altr.Delete();}else{Alarm o = (Alarm)altr.Delete();}}
public override StemFilter Create(int arg0, int arg1){if (arg1 < 0){throw new ArgumentException("arg0 should be a positive integer number, got " + arg1 + " and got " + arg2 + " instead.");}return new PortugueseStemFilter(arg0, arg1, arg2,...);}
public FtCblsSubRecord Create(){return new FtCblsSubRecord(this.GetType().GetTypeInfo().AssemblyQualifiedName, this.GetFileName());}
public virtual void clear(){_size = 0;}
public virtual string GetDetails(){return "Dedicated IP address (0.0.0.0/0)";}
public virtual string ToString(){return this.ToString();}
public virtual IList<StreamProcessor> GetStreamProcessors(){return streamProcessors;}
public virtual void DeleteLoadBalancerPolicy(string loadBalancerName, string policyName){DeleteLoadBalancerPolicy(loadBalancerName, policyName);}
public virtual void SetWindowProtections(){window.OnWindowProtected = SetWindowProtections;}
public java.nio.CharStream newUnbufferedCharStream(byteBuffer, int bufferSize){throw new System.NotImplementedException();}
public virtual DescribeOperationsResponse DescribeOperations(){return DescribeOperations(null, true);}
public override void CopyTo(byte[] array){for (int i = 0; i < this.size; i++){byte b = this[i];array[i] = b;}}
public Record(Stream in1){return this;}
public virtual void Stop(){m_workspaces.Clear();}
public override void Close(){if (this.lost == this.lost){throw new java.io.IOException("Already closed the file");}this.lost = this.writer.Close();}
public virtual IList<m_matchmaking.RuleSet> GetMatchmakingRuleSets(string matchgroup){return GetMandatoryRuleSets(matchgroup, null, true);}
public virtual Pronunciation GetProbability(int wordId){return null;}
public string getPath(){return path;}
public virtual double GetDevSquared(double[] x, double[] y){return dev(x, y) * dev(y, x + y);}
public virtual ResizeInfo GetResizeInfo(){return info;}
public override bool Evaluate(){return true;}
public virtual string end(){return java.text.CharacterIteratorClass.DONE;}
public void Traverses(ISheet sheet){Traverses(sheet.Sheet1);}
public virtual int GetReadIndex(){return _rc4.Xor(0);}
public virtual bool Equals(Term term){return _term!= null;}
public virtual string Normalize(string sequence){return Sharpen.StringHelper.Resolvesequence(sequence, "");}
public virtual void Serialize(ObjectOutput out1){out1.Write(this.GetType().GetTypeInfo().FullName);}
public bool setExactOnly(){return exactOnly;}
public virtual void validate(){throw new System.NotImplementedException();}
public virtual Assignment Describe(){return Information about an assignment. }
public virtual bool contains(string id){return false;}
public bool IncludeAllGroupsInSearch(bool enableAllGroupsInSearch){return _groupSearch.IncludeAllGroupsInSearch;}
public bool IsMultiValues { get; set; }
public virtual int countCells(){return m_cells.Count;}
public virtual DeleteGuardDutyVoiceConnectorResponse DeleteGuardDutyVoiceConnector(DeleteGuardDutyConnectorRequest request){var options = new InvokeOptions();options.RequestMarshaller = DeleteGuardDutyConnectorRequestMarshaller.Instance;options.ResponseUnmarshaller = DeleteGuardDutyVoiceConnectorResponseUnmarshaller.Instance;return Invoke<DeleteGuardDutyVoiceConnectorResponse>(request, options);}
public virtual void DeleteLifecyclePolicy(string lifecyclePolicyId){if (lifecyclePolicyId == null){throw new InvalidOperationException("lifecycle policy id is required but has no value.");}lifecyclePolicies[lifecyclePolicyId] = null;}
public void Write(byte[] b){Write(b, 0, b.Length);}
public virtual RefUpdate GetRebaseResult(){return result;}
public int GetSizeGt(int maxValue){int size = 0;foreach (int value in m_values){size += value.GetSizeGt(maxValue);}return size;}
public virtual Dashboards Describe(){return dashboards;}
public Segment Create(Segment segment){this.segment = segment;}
public String ToString(){return "this";}
public virtual UndeletedList GetUndeletedList(){return _undeleted;}
public virtual string ToString(){return "this(" + this.GetType().FullName + ")";}
public virtual Scheduler clone(){return this;}
public DictionaryReader(TextReader reader){return new DictionaryReader(reader.ReadSubtree()) as DictionaryReader;}
public java.lang.StringBuilder append(char c){append0(c);return this;}
public virtual IList<Stack> GetStackes(){return StacksList(stacksForAccount);}
public virtual double getAVEDev(int vector){return computeAVE(vector);}
public virtual ByoipCidrs Describe(){return byoipCidrs;}
public virtual DiskInformation GetDiskInformation(){return null;}
public ClusterParameterGroup(string name, List<string> args){return new ClusterParameterGroup(name, args, 0, args.Length);}
public java.nio.CharBuffer createCharBuffer(char[] chars, int start, int length) : c.newCharBuffer(chars, start, length);{for (int i = start, length = count; i < length; i++){char c = chars[i];if (c > 0){c = c[i];}}}return new java.nio.CharBuffer(this, c);}
public override Type GetType(){return this.GetType().GetTypeInfo().Assembly.GetName();}
public virtual GroupInfo GetGroupInfo(){return GroupInfo(this);}
public string pattern(){return this.pattern;}
public void setValue(V value){_value = value;}
public StringBuilder Stem(string word){return new StringBuilder(word).Append("-");}
public RenameFaceRequest(): base("https", "RenameFace"){defaultParameters = new Hashtable();}
public virtual char char(){return (char)in1.Chars;}
public virtual string ToString(IFormattableDocument doc){return tree.ToString(doc, null, doc.DocType);}
public override string ToString(){return "deleted";}
public async Task<AzureOperationResponse<IEnumerable<Operation>>> GetLogsAsync(webhookId, WebhookToken cancellationToken, CancellationToken cancellationToken).Await(false);return await webhook.ListLogMessagesAsync(webhookToken, cancellationToken).Await(false);}
public override void Unlock(string appName){if (appName == null){throw new ArgumentNullException("appName cannot be null");}if (appName.IndexOf('.')!= -1){throw new ArgumentException("appName cannot be null or empty");}return;}
public virtual void SetResourceId(int resourceId){throw new System.NotSupportedException("Resource id is a required property and must be set before making this call.");}
public int id(char c){if (c < 0){throw new ArgumentException("c=" + c + " not in " + ToString(c) + " is not a valid Charge Id (0x" + c + ")");}return -1;}
public virtual void SetReceiveCommands(ICollection<ReceiveCommand> receiveCommands){_receiveCommands = receiveCommands;}
public bool IsExternSheet(int sheet){return sheet < 0;}
public override bool Equals(object @object){lock (mutex){return @object.Equals(@object);}}
public BooleanQuery BuildBooleanQuery(AnyQueryNode node){return BuildBooleanQuery(node.Field, node.Value);}
public virtual StreamProcessor Information(){return streamProcessor;}
public virtual Permissions GetPermissions(){return dashboard.Permissions;}
public virtual Ref Get(string name){return null;}
public override long GetBytesUsed(){return base.GetBytesUsed()? base.GetBytesUsed() : 0;}
public virtual Suggests GetSuggestions(){return _suggestions;}
public virtual IStackEvents GetStackEvents(){return stackEvents;}
public virtual void SetRule(int ruleIndex){this.ruleIndex = ruleIndex;}
public ResolverResolver(){return new ResolverResolverAnonymousInnerClassHelper(this);}
public SeriesIndexRecord(Stream in1){return new SeriesIndexRecord(in1);}
public GetStylesRequest(): base(){return new GetStylesRequest(defaultParameters);}
public override void Serialize(FieldOutput out1){out1.Write(field_1_gridset_flag);}
public virtual bool Equals(Toffs other){return false;}
public GatewayGroup(Gateway group){this.gatewayGroup = group;}
public virtual ParticipantConnection CreateParticipantConnection(ParticipantConnection participant){return participant;}
public Irr coefficient(int nIncome){return _irrCoeff * nIncome;}
public void RegisterExistingWorkspaceDirectory(FilePath workspaceDirectory){DirectoryInfo existingDirectory = workspace.Directory;if (existingDirectory!= null){DirectoryInfo oldDirectory = existingDirectory.GetDirectoryInfo();if (oldDirectory!= null){DirectoryInfo b = new DirectoryInfo(oldDirectory);if (b.DirectoryInfo!= null){try{b.DirectoryInfo.SetDirectoryInfo(new DirectoryInfo(oldDirectory));}DirectoryInfo w = new DirectoryInfo(existingDirectory);if (w.DirectoryInfo.Exists){DirectoryInfo wp = new DirectoryInfo(existingDirectory);}}}
public virtual void Commit(){revert.AddItem(this);}}
public virtual double Evaluate(double[] x, double[] y){return Evaluate(x, y) ;}
public V get ( ) {return this.lastEntryInThisMap;}
public short ReadShort(){return _le.ReadShort();}
public virtual ModifySnapshotAttributeResponse ModifySnapshotAttribute(ModifySnapshotAttributeRequest request){var options = new InvokeOptions();options.RequestMarshaller = ModifySnapshotAttributeRequestMarshaller.Instance;options.ResponseUnmarshaller = ModifySnapshotAttributeResponseUnmarshaller.Instance;return Invoke<ModifySnapshotAttributeResponse>(request, options);}
public virtual IList<string> GetPaymentIDs(string bonusType){return GetStandardPaymentIDs(bonusType);}
public int value(char sequence){return 0;}
public override TokenStream Create(ITokenStream tokenStream){return new TokenStreamTokenFilter(tokenStream);}
public virtual string getPath(){return path;}
public virtual void init(){lastDocID = -1;wordNum = -1;word = 0;}
public virtual string insert(int offset, string @string){return insert(offset, @string);}
public virtual ISequence<long?> Decode(long? blocks, start, end){return Decode(blocks, start, end);}
public ElisionFilter(TokenStream input){return new ElisionFilter(input);}
public void EatCells(IRow row, IRemap array){EatCells(row, remap);}
public virtual IToken GetToken(int index){return tokens[index];}
public string ToString(){return "Array(\"" + this.elements + "\")";}
public FolderInformation GetFolderInformation(File folder){return FolderInformation.Create(folder);}
public virtual void push(E e){if (this.e == null){throw new System.InvalidOperationException();}this._enclosing.push(e);}
public override void Initialize(DataOutput os){os.SetOutput(os.GetOutput()) }
public virtual BuildRule CreateBuildRule(){return new BuildRule(this);}
public override void SetBaseReference(AreaEval areaEval){_baseRef = areaEval.GetBaseReference();}}
public void SetDrawingGroup(int group){this.m_group = group;}
public void Reset(bool b){reset(b? "true" : "false");}
public virtual void Reset(){decoder = null;}
public java.io.BufferedReader reader { set; }
public virtual CodeRepository Information(){return Repository;}
public DBSubnetGroup(string subnetName){return new DBSubnetGroup(subnetName);}
public virtual string GetOldName(){return branch.GetName();}
public override bool ShouldDelete(){return false;}
public virtual void Stop(){this.compilationJob = null;}
public virtual void SetSecondaryProgress(float amount){setSecondaryProgress(amount);}
public virtual void Clear(){_limit = 0;}
public string getRawPath(){return path;}
public virtual SourceAccountInformation GetSourceAccountInformation(){return sourceAccountInformation;}
public virtual ExportJob CreateExportJob(){return new ExportJob(this);}
public IPPool(){return new _IPPool_132(this, 10, 10, 10, false);}
public bool Equals(Style other){return this == other;}
public virtual void Release(){list.Clear();}
public override bool Equals(object @object){return this == @object;}
public virtual void SetReferenceLogMessage(string message){this.m_referenceLogMessage = message;}
public StreamIdRecord(StreamIdRecord in1){return in1.ReadStream(recordId);}
public virtual RecognizeCarResponse RecognizeCar(RecognizeCarRequest request){var options = new InvokeOptions();options.RequestMarshaller = RecognizeCarRequestMarshaller.Instance;options.ResponseUnmarshaller = RecognizeCarResponseUnmarshaller.Instance;return Invoke<RecognizeCarResponse>(request, options);}
public override java.nio.ByteOrder get(byte order){return _byteOrder;}
public virtual int GetAheadCount(){return ahead;}
public virtual bool isNew(){return this.fragmentIsANewFragment;}
public CloudFrontOriginAccessIdentityConfiguration(string accessId){return CloudFrontOriginAccessIdentityConfiguration(accessId);}
public override bool Equals(object o){return this == o;}
public virtual void Destroy(){transitGateway = null;}
public virtual void resize(int size_1){resize(size_1);}
public override void Create(){base.Create();}}
public virtual void SetPersonIdent(string personIdent){this.personIdent = personIdent;}
public virtual LaunchTemplateData GetLaunchTemplateData(string appName, string password){return null;}
public ProfilingATNSimulator(ATNSimulator instance){this.instance = instance;}
public void SetQQNames(){this.qq = string.Empty;}
public virtual void PromoteReadonlyCluster(Cluster cluster){this.Promote(cluster.ReadWriteCluster);}
public virtual CapacityReservation Describe(){return CapacityReservation.Collect(null);}
public override string ToString(){return "Lucene.Net.IndexSearcher(" + name + ")";}
public virtual void IncrementToken(){this.token += 1;}
public override void Serialize(ObjectOutput out1){out1.Write(this.GetType().GetTypeInfo().FullName);}
public virtual int[] Decode(int bits){return Decode(bits, 0, bits.Length);}
public virtual bool expected(IToken token){return true;}
public virtual void Update(){endOfStreamWhileHandling(true);}
public override void Evaluate(double value){if (double.IsNaN(value)){throw new EvaluationException(ErrorEval.NA);}double result = Evaluate(value);if (result == double.NaN){result = double.NaN;}return result;}
public virtual string ToString(){return "this@" + this;}
public override IList<Assignment> GetAssignments(int HIT){return AllAssignments(HIT);}
public virtual void DeleteAccessControlRule(){this._enclosing.DeleteRule(this.accessControlRuleIndex);}
public virtual Arc getArc (int arcNumber){return getFirstArc(arcNumber);}
public virtual long[] Decode(long[] data){return Decode(data, 0, data.Length);}
public virtual void skip(int count){if (count < 0){throw new java.io.IOException("Did not skip " + count + " chars from input stream");}skip(count);}}
public virtual Dictionary<string, Object> GetMap(){return new Dictionary<string, Object>();}
public virtual void Update(){throw new NotImplementedException("API key has no associated data.");}
public java.io.InputStream getStream(boolean brief){return _isBrief? _isBrief : _isA(boolean brief)?_isA(boolean brief)?_isA(boolean brief)?_is : _isA(boolean brief)?_is {brief}? _is : null;}
public void Clear(){array = new object[0];}
public virtual Amazon GuardDutyDetectorVersion Update(){return new AmazonGuardDutyDetectorVersion(this);}
public virtual void resize(){resize(true);}
public RevFlagSet(ICollection<RevFlag> RevFlagSet){return new RevFlagSet(this, RevFlagSet.AllOf(RevFlag objects));}
public virtual int size(){return this._enclosing._size;}
public virtual long GetLong(){return buf.GetLong();}
public virtual void writeLong(long value){throw new System.NotImplementedException();}
public virtual NGit.Turkish.LowercaseFilter GetLowercaseFilter(){return GetLowercaseFilter();}
public override bool Equals(object o){return base.Equals(o);}
public void AddWord(string word){AddWord(word);}
public Merger(Object o){return new InCoreMerger(o);}
public virtual float Score(){return 0.0f;}
public virtual IParseTree Evaluate(IParseTree tree){return Evaluate(tree.root, tree.level, tree.steps);}
public virtual string ToString(){return this.ruleID.ToString();}
public virtual ServiceUpdates Describe(){return ServiceUpdates.Instance;}
public string name(int index){return name;}
public virtual IList<Location> GetLocations(){return locations;}
public virtual string ToString(){return "this[" + this.phase + ", memory=" + this.bytesUsed + "]";}
public virtual DirCache.Entry GetDirectoryCacheEntry(){return directoryCache.Entry;}
public virtual void putIntCount(int byteCount){m_buf.putInt(m_lostIndex + 2 * byteCount, 0);}
public virtual void trim(){to = trimEnd();}
public virtual interfaces.VirtualInterface[] virtualInterfaceNames(){return virtualInterfaceNames();}
public override StemFilter Create(int arg0, int arg1){return new RussianLightStemFilter(arg0, arg1);}
public virtual object Clone(){return new object[elements.Length];}
public BasicAuthSessionCredentials(SessionToken sessionToken){_sessionToken = sessionToken;}
public override int ReadShort(){return delegate1.ReadShort();}
public virtual void activate(){this._enclosing.activate();}
public virtual ReceiptRuleSet GetDetails(){return receiptRuleSet;}
public virtual void SetFilter(string name){this.filter = name;}
public void putDouble(double value){if (double.IsNaN(value)){throw new System.ArgumentException("value is a NaN.Value in the buffer is read - only property.");}if (double.IsInfinity(value)){throw new java.nio.DoubleBufferException("value is a NaN.Value in the buffer is read- only property.");}
public traffic policy { get; private set; }
public virtual void SetUp(void){SetUp(null);}}
public override void write(int b){if (m_len >= m_buf.Available()) { m_buf.write(b);}else{ m_len = m_buf.Available(); }
public FileSystemResolver(Properties fileResolverProperties){this._fileResolverProperties = fileResolverProperties;}
public ValueEval Create(Ref3DPxg arg0){return Create(arg0, arg1, arg2,...);}
public void Delete(){this._enclosing.DeleteDataset(this.datasetName);}
public override void Start(){base.Start();}
public virtual DescribeReservedCacheNodesOfferingsResponse DescribeReservedCacheNodesOfferings(DescribeReservedCacheNodesOfferingsResponse result) {return DescribeReservedCacheNodesOfferings(result.Nodes);}
public static int PMT(complex number){return PMT(number);}
public virtual VersioningInfo GetDocumentVersions(){return Versioning.GetDocumentVersions(this.Document, this.Site, this.Locale);}
public virtual IList<Destination> GetDestinations(){return new List<Destination> { this.local.GetDestination(this.local.GetDestination(this.local.GetDestination(this.local.GetDestination()));}
public void DeleteAlias(){AccountAlias at = null;}
public virtual void resize(){if (count < 1){throw new System.IndexOutOfRangeException();}else{resize(count++);}}
public string representation(OutputObject output){return output.ToString();}
public void DeleteCell(){_evaluator.NotifyDataSetDeleted(this.Row, this.Column);}
public java.lang.String replaceAll(String str, int start, int end){if (start < end && end > start){throw new java.lang.StringIndexOutOfBoundsException("start");}else{throw new java.lang.StringIndexOutOfBoundsException("end");}return str;}
public IdentityPoolConfiguration(IDefaults){return identityPoolConfiguration;}
public int k(){return k - 1;}
public virtual void setValueAt(int index, V value){if (index < 0 || index >= values.Length){throw new IndexOutOfRangeException(index.ToString());}else if (index < 0){throw new IndexOutOfRangeException(index.ToString());}this._value = value;}
public virtual string ToString(){return "this " + this.ToString();}
public virtual int sum(int start, int end){return ords.Get(start) + end;}
public virtual bool IsReadOnly(){return readonly;}
public void Remove(ICell cell){_1.Remove(cell);}
public virtual Sharpen.List<E> subList(){return list;}
public virtual FilePath GetFileHeader(){return file;}
public virtual void Attach(object o){Attach(o);}}
public virtual InitiateJobRequest Create(InitiateJobRequest request){var options = new InvokeOptions();options.RequestMarshaller = InitiateJobRequestMarshaller.Instance;options.ResponseUnmarshaller = InitiateJobRequestUnmarshaller.Instance;return Invoke<InitiateJobRequest>(request, options);}
public string ToString(){return "Lucene.Util.SerializeValue(this)";}
public virtual void write(){out1.write();}
public virtual void Add(string field){index.Add(field);}
public virtual void Delete(){this._enclosing.Delete(this.m_stackSet);}
public virtual BuildRuleList GetBuildRuleList(Repository repository){var options = new InvokeOptions();options.Repository = repository;return BuildRuleList<T>(this, options);}
public new java.util.concurrent.CopyOnWriteArrayList.CowArrayCreate(object[] elements, int capacity_1, int capacity_2){object[] snapshot = elements;return new java.util.concurrent.CopyOnWriteArrayList.CowArrayCreate(object[]{elements, 0, capacity_2});}
public InvokeService(InvokeServiceRequest request){var options = new InvokeServiceOptions();options.RequestMarshaller = InvokeServiceRequestMarshaller.Instance;options.ResponseUnmarshaller = InvokeServiceResponseUnmarshaller.Instance;return Invoke<InvokeService>(request, options);}
public virtual IList<Photo> GetAlbumPhotos(){return GetPhotoList(null, null, null, null);}
public virtual bool hasPrevious(){return this._enclosing.hasPrevious();}
public void Delete(){this.hsmConfig = null;}
public virtual CreateLoadBalancerRequest(string loadBalancerName){return new CreateLoadBalancerRequest(loadBalancerName);}
public virtual UserInfo GetUserInfo(){return userInfo;}
public virtual TagCreateOrUpdateTagResponse TagCreateOrUpdateTag(TagCreateOrUpdateTagRequest request){var options = new InvokeOptions();options.RequestMarshaller = TagCreateOrUpdateTagRequestMarshaller.Instance;options.ResponseUnmarshaller = TagResponseUnmarshaller.Instance;return Invoke<TagCreateOrUpdateTagResponse>(request, options);}
public virtual string GetReferenceName(){return referenceName;}
public virtual Query Build(){return Query;}
public bool subTotal(double[] data){return subTotal(data, 0, data.Length);}
public virtual AssociationsCollection GetAll(){return new AssociationsCollection(this);}
public VoiceConnector Proxy(){return _connector;}
public java.util.MapClass.Entry<K, V>(object, string, bool) { return this._enclosing.entryFor(object, object) = this._enclosing.entryFor(object, object);}
public DateTime ToDate(double value){return new DateTime(value * 1000);}
public virtual void StartGuardDutyTracking(string app){startGuardDutyTracking(app);}
public override long size(){return this._enclosing._size;}
public Route Info(string name){return Info(name);}
public virtual void Delete(){Cluster c = (Cluster)Get(c.GetName());c.Delete();}
public virtual string ToString(){return "MMS(" + ssaName + ")";}
public FileBasedConfig Create(FilePath configFilePath){return new FileBasedConfig(configFilePath);}
public virtual void MoveToNextSentence(int position){if (position < 0){throw new ArgumentException("position " + position + " is before the end of the sentence");}movedTo = position + 1;}
public virtual void Update(string name, string value){if (name == null){throw new ArgumentException("name cannot be null or empty");}string name = name.Intern();string value = Get(name, value);if (value == null){throw new ArgumentException("value cannot be null"); }
public virtual Object Clone(){return this;}
public virtual float GetErrorPercent(){return GetErrorPercent() * Error.GetValue(this, shapeData);}
public override int getCodePointAt(int index){return index;}
public void SetPasswordVerifier(String passwordVerifier){this.passwordVerifier = passwordVerifier;}
public vaults.ListVaultsResponse ListVaults(VaultToken cancellationToken){return vaults.ListPrivileged(vaultToken).ConfigureAwait(false);}
public DateTimeFormatter formatter = null;
public static GetVideoCoverRequest Create(VideoCoverRequest request){var options = new InvokeOptions();options.RequestMarshaller = GetVideoCoverRequestMarshaller.Instance;options.ResponseUnmarshaller = GetVideoCoverRequestUnmarshaller.Instance;return new GetVideoCoverRequest(request, options);}
public int indexOf(E e){if (e is java.util.MapClass.Entry<K, V>){return indexOf(e.getKey(), e.getValue());}throw new java.util.Collections.NotFoundException(e.getType());}
public virtual Spot FleetResponse Spot (Spot FleetRequest){return Spot FleetResponse(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetRequest(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse(Spot FleetResponse)));})));}
public void IndexFaces(FacesContext faces){if (faces == null){faces = faces.Current;}if (faces.Count == 0){faces = faces.Count - 1;}if (faces.Count == 1){faces = faces;}else{faces = faces.ToArray();}}
public java.text.BreakIterator biter(byte[] text, int line, int col){return null;}
public string ToString(DCONREF file){return ToString(file);}
public virtual int GetOpenFilesInRepository(Repository repository){return repository.OpenFilesInRepository(repo);}
public override string ToString(){return "Feature " + featureNumber;}
public java.nio.charset.UnicodeLEByteArray convert(string to_le){return convert(to_le);}
public IList<string> GetFooterLines(string key){return GetOrAddHeaderLines(key, null);}
public virtual void refresh(){list.clear();}
public float get(int index){return 0.0f;}
public virtual void DeleteGuardDutyDetector(){this.detector = null;}
public virtual void grow(int capacity){if (capacity > 0){throw new System.BufferUnderflowException(resourceDescription + ": capacity is bigger than the array's capacity (" + capacity + ")");}array.grow(capacity);}
public virtual IList<Exclusion> GetExclusions(){return exclusions;}
public override SpatialStrategy GetSpatialStrategy(int round){return SpatialStrategy.GetSpatialStrategy(round);}
public void RestoreClusterData(ClusterData cluster){this._enclosing.RestoreClusterData(cluster);}
public void Serialize(ILittleEndianOutput out1){out1.Write(this.m_format);}
public AgentProfile CreateOrUpdate(string profileName){return new AgentProfile(profileName);}
public virtual ParserRuleContext Compile(ParserRuleContext context){return Compile(context.GetParsingRule(reporter));}
public virtual void Backtrack(){this._enclosing.Backtrack();}
public override string ToString(){return Strategy;}
public virtual void copyTo(byte[] array){throw new System.NotImplementedException();}
public java.text.LineMapCreate(byte[] data){return new java.text.HashMap<K, V>.LineMap(this, data);}
public virtual HashSet<E> emptySet(){return new HashSet<E>(3);}
public override long GetBytesUsed(){return delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetBytesUsed() && delegate1.GetValue();}
public String ToXml(Formatter formatter){return ErrorConstants.GetText(ErrorConstants.ERROR_REF) + ":" + ToString(ErrorConstants.ERROR_REF) + ":" + ToString(ErrorConstants.ERROR_REF) + ":" + ToString(ErrorConstants.ERROR_REF) + ":" + ToString(ErrorConstants.ERROR_REF) + ":" + ErrorMessage);}
public override StemFilter Create(int stemId){return new GalicianMinimalStemFilter(stemId);}
public virtual string ToString(){return "this@" + this;}
public virtual void addAdditionalParameters(){throw new System.NotSupportedException();}
public OptionGroup(String name){this.name = name;}
public void Associate(MemberAccount @object){if (@object!= null){try{@object.SetMemberAccountData(@object);}catch (MissingAccountException ){throw new InvalidOperationException("You must associate a member account with your account.");}}
public virtual void runRefreshProgressTask(){base.runRefreshProgressTask();}
public virtual void SetTerminationProtection(bool protect){termination = protect;}
public override string GetErrorCode(){return "Internal Server Error" ;}
public java.nio.CharBuffer getReadOnlyBuffer(){return new java.nio.ReadWriteCharBuffer(this);}
public virtual void Stop(){sentimentDetection = null;}
public Dictionary<object, ObjectId> GetObjectIdMap(){return new Dictionary<object, ObjectId>();}
public virtual void Clear(){hashTable.Clear();}
public void Reset(bool b){start = false;end = true;}
public override void Read(int input){throw new InvalidOperationException("Unsupported operation (" + this.GetType().Name + " does not support writing RefErrorPtg"));}
public virtual void Suspend(){serverGroup = null;}
public ValueEval Evaluate(int sourceRow, int sourceColumn){return _expression;}
public virtual SetRepoRequest SetRepoRequest(IRequest request){this.request = request;}
public DateTime SetDate(){if (null == date){date = new DateTime(1900, 1, 1);}else{date = new DateTime(date, 1, 1);}}
public GermanMinimalStemFilter(){return new GermanMinimalStemFilter(this);}
public virtual object Clone(){return (object)this.CloneViaReserialise();}
public void write(char[] chars){write(chars, 0, chars.Length);}
public override RevFilter With(DateTime since){return new CommitTimeRevFilter(since);}
public void SetDeleteGroupPolicy(string groupName){this.groupName = groupName;this.policyName = null;}
public virtual void Deregister(MulticastGroup group){this.Deregister(group);}}
public virtual void Delete(){BatchDelete(null, this);}
public Algorithm(byte[] data){buffer = new byte[data.Length];offset = 0;}
public int ReadByte(){return _in.ReadByte();}
public virtual void setLength(int length_1){buffer.length = length_1;}
public virtual DescribeScalingProcesses(){return DescribeScalingProcesses();}
public List<ResourceRecordSet> ListAll(RecordRecordSet[] records){return records;}
public virtual void RecoverInlineToken(){this.Type = TokenConstants.Inline;}
public void SetTags(string[] tags){for (int i = 0; i < tags.Length; i++){tags[i] = Tags[i];}}
public ModifyStrategyRequest(string strategyId){throw new System.NotSupportedException("Strategy Id is a required property and must be set before making this call.");}
public virtual IEndpointServices Describe(){return EndpointServices;}
public virtual void Init(EventArgs a){a.AddHandler(a);}
public override bool contains(object o){return this._enclosing.containsValue(o);}
public SheetRangeName(String rangeName){_rangeName = rangeName;}
public string domainName = "";if (domainName == ""){domainName = "localhost";}if (domainName == ""){domainName = "127.0.0.1";}if (domainName == null){domainName = "localhost";}if (domainName.Length == 0){domainName = "localhost";}if (domainName.Length == 1){domainName = "." + domainName;}if (domainName.Length == 1){domainName = "." + domainName;}else{domainName = ".." + domainName;}domainName = domainName;}
public virtual ParseException: this(TokenStreamException){this.token = this.expectedToken;this.expectedToken = this.expectedTokenSeq;return new ParseException(this.message);}
public static FetchPhotosRequest(object request){var options = new InvokeOptions();options.RequestMarshaller = FetchPhotosRequestMarshaller.Instance;options.ResponseUnmarshaller = FetchPhotosResponseUnmarshaller.Instance;return Invoke<FetchPhotosRequest>(request, options);}
public java.io.PrintWriter(){return new java.io.PrintWriter(this);}
public NGramTokenizer(int argc){if (argc < 2){throw new ArgumentException("argc must be 2 or 3");}int[] args = { argc };if (argc < 1){throw new ArgumentException("argc must be 2 or 3");}int[] hash = hash.ToArray();hash.Sort(naturalSort);int index = array.IndexOf(argc, 1);hash.Add(index);}
public override bool IsDirectory(){return false;}
public virtual void SetStemDerivationalFlag(bool flag){stemDerivational = true;}
public traffic policy(string name, string type){throw new System.NotSupportedException("Unsupported type: " + type);}
public void Serialize(Password password){Serialize(password.GetOutput(), password);}
public virtual int Floor(int n){int r = 0;int b = n < 0? -1 : n;return r + b;}
public java.nio.ByteBuffer reset(byte[] b){return reset(b, 0, b.Length);}
public virtual IList<ITree> GetChildren(ITree tree){return GetChildren(tree, null);}
public virtual void Clear(){this._enclosing.Clear();}
public void RefreshAll(){this._enclosing.RefreshAll();}
public virtual DeleteNamedQueryResponse DeleteNamedQuery(DeleteNamedQueryRequest request){var options = new InvokeOptions();options.RequestMarshaller = DeleteNamedQueryRequestMarshaller.Instance;options.ResponseUnmarshaller = DeleteNamedQueryResponseUnmarshaller.Instance;return Invoke<DeleteNamedQueryResponse>(request, options);}
public GraphvizFormatter(Connection costs){return new GraphvizFormatter(costs);}
public virtual SetMultiagentRequestParameters setMultiagentRequestParameters(SetMultiagentRequestParameters request){var options = new InvokeOptions();options.RequestMarshaller = SetMultiagentRequestParametersMarshaller.Instance;options.ResponseUnmarshaller = SetMultiagentRequestParametersUnmarshaller.Instance;return Invoke<SetMultiagentRequestParameters>(request, options);}
public virtual List<UserProfile> GetProfiles(){return new List<UserProfile>(users);}
public virtual NGit.GlobalDb.Relation Create(string name, NGit.GlobalDb.Relation db){var options = new InvokeOptions();options.Name = name;options.Value = db;return new NGit.GlobalDb.Relation(options);}
public virtual void Start(){start = true;}
public virtual ICollection<string> GetIgnoredPaths(){return Sharpen.Collections.UnmodifiableSet(ignoredPaths);}
public override Object Read(Stream stream){throw new NotSupportedException("Unsupported Operations!");}
public void SetChangeAction(ChangeAction action){this._changeAction = action;}
public void Delete(){this._enclosing.DeleteImage(this.GetImageId());}
public CompositeReaderContext CreateConfigurationSet(XmlReader reader){return (CompositeReaderContext)Create(reader, 0, 0);}
public java.util.Iterator<E> iterator(){return iterator();}
public void VisitAll(Record[] records){for (int i = 0; i < records.Length; i++){Visit(records[i]);}}
public override String ToString(){FtCbls obj = new FtCbls();obj.FillInBody(this);return ObjCulture.InvariantCulture.ToString(obj);}
public BATBlock(){return new BATBlock(this);}
public NGit.Storage.File.Resource CreateOr Update(FileInfo file){throw new NotSupportedException(MessageFormat.Format(JGitText.Get().resourceIsNew, file));}
public virtual void DeleteMailboxPermissions(){DeleteMailboxPermissions(null);}
public virtual IList<DatasetGroup> GetDatasetGroups(){return datasetGroups;}
public virtual void Resume(){Sharpen.Extensions.RequireNotResumable(this);}
public virtual DescribeGuardDutyAccountResponse DescribeGuardDutyAccount(DescribeGuardDutyAccountResponse describeGuardDutyAccount){return DescribeGuardDutyAccount(describeGuardDutyAccount);}
public String ToString(){return "this";}
public static object Merge(object o){return o;}
public virtual string ToString(){return ToString(this);}
public XPath(Parser parser, Path path){return new XPath(parser, path);}
public CreateAccountAliasRequest(AccountAlias alias){var request = new CreateAccountAliasRequest(alias);request.AccountAlias = alias;return request;}
public override void Decode(int n){if (n < 1){throw new System.ArgumentException("Bad number of bytes: " + n);}if (n > 1){throw new System.ArgumentException("Bad number of bytes: " + n);}
public virtual NGit.Api.PushConnection Open(string host, string password){return Open(host, password, "push");}
public virtual void copyCharAt(int index){throw new System.NotImplementedException();}
public virtual string KeyOf(T entry){return entry.Key;}
public int size(){return elements.Length;}
public virtual void add(E e){throw new System.NotSupportedException("object is not a list-like object.");}
public virtual DomainInformation GetDomainInformation(){return DomainInformation(domainInformation);}
public void Flush(){_le.Flush();}
public PersianCharFilter Create(ParameterCollection parameters){var options = new InvokeOptions();options.Parameters = parameters;return new PersianCharFilter(this, options);}
public virtual void IncrementToken(){this.IncrementToken();}}
public java.nio.FloatBuffer allocate(int capacity_1){return new java.nio.ReadWriteFloatBuffer(capacity_1);}
public virtual NGit.Diff.Edit GetCopyAfter(NGit.Diff.Edit other){return this;}
public virtual void Update(string rule){this.rule = rule;}
public virtual List<TerminationCredentials> GetTerminationCredentials(){return null;}
public virtual DescribeDeploymentTargetResponse DescribeDeploymentTarget(DescribeDeploymentTargetRequest request){var options = new InvokeOptions();options.RequestMarshaller = DescribeDeploymentTargetRequestMarshaller.Instance;options.ResponseUnmarshaller = DescribeDeploymentTargetResponseUnmarshaller.Instance;return Invoke<DescribeDeploymentTargetResponse>(request, options);}
public virtual void SetNoChildReport(XIncludeReport noChildReport){this.noChildReport = noChildReport;}
public E get ( int location){return this[location];}
public virtual EntityCollection EntityCollection(){return EntityCollection.Create(this, EntityCollection<Entity>());}
public virtual void SetTreeIndex(int treeIndex){this.m_treeIndex = treeIndex;}
public virtual void DescribeNetworkInterfaces(){NetworkInterfaces.Describe(this);}
public bool contains(int row, int column){return a[row, column] = value;}
public string ToString(){return this.ToString(CultureInfo.InvariantCulture);}
public virtual PatchType Get(){return type;}
public java.util.Iterator<K> keys(){return this._enclosing.keys(this.entrySet().keySet().iterator()){return this._enclosing.values(this.entrySet().keySet());}
public virtual void Initialize(){this.lastDocID = -1;this.wordNum = -1;word = 0;}
public BytesRef Peek(){return this.next;}
public virtual string ToString(){return "Output:\n" + this.ToString();}
public void SetUp ( ){this._enclosing.SetAuthorizationProvider(this);}
public virtual void Unpop(){this.head.Remove(this.head);this.tail = null;this.col = null;this.rec = null;}
public EdgeNGramTokenizer(){return new EdgeNGramTokenizer(this);}
public ModifyDBParameters(string name, List<string> args){var group = GetGroup(name);args.Add(name);}
public virtual int getNetworkLimit(){return getNetworkLimit()? getNetworkLimit() : DEFAULT_NETWORK_LIMIT;}
public virtual void setValueAt(int index, V value){if (index < 0 || index >= values.Length){throw new IndexOutOfRangeException(index.ToString());}else if (index < 0){throw new IndexOutOfRangeException(index.ToString());}this._value = value;}
public virtual TreeFilter Clone(){return this;}
public virtual string ToString(){return "this[" + this.phase + ", memory=" + this.bytesUsed + "]";}
public virtual bool CanAppend(){return true;}
public virtual int lastIndexOf(char c){if (c == 0){return -1;}return lastIndexOf(c, 1);}
public virtual void DeleteNetworkACLEntry(){if (_networkACL == null){throw new System.NotImplementedException();}if (_networkACL == null){throw new System.NotImplementedException();}
public virtual void associate(string name, string value){if (name == null){throw new ArgumentNullException("name cannot be null");}string[] members = group.members;if (members == null){throw new ArgumentException("members cannot be null or empty");}foreach (string name in members){if (name.StartsWith(".") && name.Equals("$")){throw new ArgumentException("cannot associate a member with a group with a string");}foreach (string name in names){if (name.StartsWith(".") && name.Equals(name.ToCharArray())){throw new ArgumentException("cannot associate a string with a string");}groups[name] = value;}
public int GetFirstCommitterIndex(sbyte b){return b & 0x00FF;}
public virtual int getLineNumber(){return lineNumber;}
public virtual void SetPath(string path){m_paths.AddItem(path);}
public virtual PushTemplate GetPushTemplate(string id){return PushTemplate.Get(id);}
public virtual VaultResponse VaultGet(VaultRequest request){var options = new InvokeOptions();options.RequestMarshaller = VaultRequestMarshaller.Instance;options.ResponseUnmarshaller = VaultResponseUnmarshaller.Instance;return Invoke<VaultResponse>(request, options);}
public virtual DescribeVpcPeeringConnectionsResponse DescribeVpcPeeringConnections(DescribeVpcPeeringConnectionsResponse result){return DescribeVpcPeeringConnections(result);}
public override void put(long value){if (value < 0){throw new java.lang.StringIndexOutOfBoundsException("value=" + value + " is not valid");}if (value >= 0 && value < buffer.capacity){throw new java.nio.BufferOverflowException();}buffer[index] = value;}
public virtual void Register(){this.lastEntry = this.lastEntry.GetObjectId();this.lastEntry = this.lastEntry.GetObjectId();}
public NGit.Format.Format GetFormat(int id){return format;}
public virtual void Delete(){Sharpen.Runtime.Dispose (this._enclosing.m_app);}
public virtual ChannelInformation GetChannelInformation(){return channelInformation;}
public override BinaryReader GetReader(){return new BinaryReader(this);}
public bool IsSchemeChar(int index){return schemeChars[index];}
public virtual IList<string> GetAppliedSchemas(){return Sharpen.Collections.UnmodifiableList(this._appliedSchemas).Values;}
public string GetName(){return username;}
public ValueEval Evaluate(int arg0, int arg1, int arg2, bool? val){throw new EvaluationException(Error().code, Error().message);}
public virtual int indexOf(E element){return -1;}
public void RemoveName(int index){names.RemoveAt(index);workbook.Refresh();}
public void SetQueueUrlAndAttributeNames(string queueUrl){this.queueUrl = queueUrl;}
public virtual bool[] copy(){return array.Copy();}
public virtual void SetEnabled(bool value){enabled = value;}
public virtual void Delete(){this._enclosing.Delete(this.m_logPattern);}
public override bool contains(object o){return this._enclosing.containsKey(o);}
public SheetIndex GetFirstSheetIndex(int index){return _firstSheetIndex;}
public virtual bool IsValid(string command){return false;}
public virtual MergeStrategy Register(MergeStrategy mergeStrategy){return mergeStrategy;}
public virtual int byteCount(){return byteCount();}
public ZoneRef SetToHostedZone(ZoneRef zone){return this;}
public virtual DescribeGuardDutyFindingsResponse DescribeGuardDutyFindings(){return DescribeGuardDutyFindings(null, null, null);}
public virtual DescribeTopicsDetectionJob DescribeTopicsDetectionJob(){return DescribeTopicsDetectionJob(this);}
public virtual void Match(int arg){if (arg > 0){throw new System.ArgumentException("arg must be a positive integer number, got: " + arg);}if (arg < 0){throw new System.ArgumentException("arg < 0): " + arg);}if (arg > 1){throw new System.ArgumentException("arg < 0: " + arg + " > " + this.GetType().Name + " is not a valid match number.");}
public override void Write(int b){if (m_len >= m_buf.Length){throw new System.IO.IOException("too many bytes written to this stream");}m_len += 1;m_buf.Write(b);}
public virtual void printTaxonomyStats(){printTaxonomyStats();}
public override void Set(byte value){throw new System.NotImplementedException();}
public static void Initialize(){Cache.SetDefaultSeed(seed);}
public virtual void SetCacheSource(CacheSource source){this._cacheSource = source;}
public void SetAttributeNames(){if (this.attributeName == null){throw new ArgumentException("Missing required property 'name'.");}if (this.attributeType == null){throw new ArgumentException("Missing required property 'type'.");}
public virtual string join(string separator){return join(separator, strings);}
public virtual IList<TaskDefinitionFamily> GetTaskDefinitionFamilies(){return taskDefinitionFamilies;}
public CompositeReaderContext GetComponents(){return _components;}
public virtual ActivatePhotoRequest(string photoId){return ActivatePhotoRequest(photoId);}
public RuleSet(string rule){return new RuleSet(rule);}
public virtual IList<CIDRAddressRange> GetCidrRanges(){return new List<CIDRAddressRange>(CIDR_MAX_ADDRESS_RANGE);}
public virtual IDictionary<object, object> GetAsMap(){return new Dictionary<object, object>();}
public virtual void DeletePushTemplate(string pushTemplateId){if (pushTemplateId == null){throw new PushTemplateNotFoundException(pushTemplateId);}delete(pushTemplateId);}
public DomainEntry(string name, string password){return new DomainEntry(name, password);}
public override int size(){return 8;}
public TokenizerFactory(IDictionary<string, List<string>> customHeaders = null, andSinks = null, andStorage = null) throws IOException{throw new InvalidOperationException("Unsupported operation.");}
public int valueAt(int index){return values[index];}
public virtual IList<Head> GetHeads(char c){return GetHeads(c);}
public override void putShort(short value){if (shortValues == null){throw new System.IO.IOException("Value is null or has been written to and has not been written.");}else{putShort(value);}}
public override void write(object o){if (!(o is SharedObject)){throw new System.NotImplementedException();}this.o = o;this.pos = 0;this.blocks = null;}
public virtual int codePointToOffset(int codePoint){return string.GetCodePointToOffset(codePoint);}
public string GetUniqueAltNumber(IDictionary<string, IAltSet> altsets){return GetUniqueAltNumber(altsets, altsets.Count);}
public virtual DateTime GetWhen(){return when;}
public void SetRuleNames(String ruleName){this.ruleName = ruleName;this.bypassTokenType = GetTokenType(tokenType);}
public void Disable () {}
public Room(Props props){this.Props = props;}
public virtual void DeleteReplicationGroup(){this._enclosing.DeleteReplicationGroup(this.m_replicationGroupName);}
public java.nio.CharBuffer decode(byte buffer, int length) {java.nio.CharBuffer result = decode(buffer, length);return result;}
public virtual void SetDistributionIdStatus(int idStatus){this.idStatus = idStatus;this.domainName = domainName;}
public double[] toDoubleArray(){return new double[values.Length];}
public static DateWindowRecord Read(RecordInputStream in1){return new DateWindowRecord(in1);}
public virtual void SetDeleteId(string id){this.id = id;}
public override ParserExtension Get(string key){return null;}
public override void Initialize(){base.Initialize();}}
public virtual void end(){if (this.lastEntryReturned == null){this.lastEntryReturned = new Object(Vault.State.INVALID_ENTRY);this.lastEntryReturned = null;this.lastEntryReturned = null;this.lastEntryReturned = null;this.lastEntryReturned = null;this.lastEntryReturned = null;this.lastEntryReturned = null;this.lastEntryReturned = null;this.lastEntryReturned = null;this.lastEntryReturned = null;}
public virtual CharInterval[] GetCharIntervals(){return CharInterval.CopyOf(this._enclosing.intervals);}
public override int GetBytesUsed(){return base.GetBytesUsed()? base.GetBytesUsed() : 0;}
public void Register(Amazon.EC2.Instance instance){_ec2 = instance;}
public virtual DescribeClusterUserKubeconfigResponse DescribeClusterUserKubeconfig(DescribeClusterUserKubeconfigRequest request){var options = new InvokeOptions();options.RequestMarshaller = DescribeClusterUserKubeconfigRequestMarshaller.Instance;options.ResponseUnmarshaller = DescribeClusterUserKubeconfigResponseUnmarshaller.Instance;return Invoke<DescribeClusterUserKubeconfigResponse>(request, options);}
public int ReadPrecision(){return field_1_precision;}
public void Serialize(DataOutput os){os.Write(GetType().Name).Append("[MatrixPackedWriter(").0x" + os.Getpid() + ", size=" + size + "]");}
public virtual void Delete(){this.GetType().DeleteInterface(this.GetType().GetTypeInfo().FullName);}
public Entry GetEntry(Name name){try{return EntryFactory.GetEntry(name);}catch (FileNotFoundException){return null;}}
public virtual string ToString(){return "Vocabulary(" + m_vocabulary + ")";}
public virtual void DeleteOrigination(){Amazon.GuardDuty.VoiceConnector deleteConnector = originationOf(origination);}
public virtual void append(char ch){buffer.append(ch);}
public virtual Generation GetGeneration(){return generation;}
public virtual TagOption GetTagOption(string option){return null;}
public virtual void Start(){startModeration();}
public string quote(string @string){return "'" + @string + "'";}
public void setValue(V newValue){throw new System.NotSupportedException();}
public virtual void SetInputStream(java.io.InputStream @in){throw new System.NotImplementedException();}
public override long Get(){return 0;}
public virtual int readBytes(int byteCount, MemoryStream buffer){throw new System.NotImplementedException();}
public virtual void clearErrorListeners(){lastError = null;}
public TokenStream newTokenStream(ITokenSource source, TokenChannel channel){this.source = source;this.channel = channel;}
public policies(){return list(object.policies());}
public virtual string pack(E e){return GetType().FullName + ".pack(E=" + e + " )" + f"{e.GetType().FullName}";}
public virtual Stemmer GetStem(string @string){return null;}
public virtual void onError(){throw new NGit.Errors.ParseException(Error.GetText()));}
public String ToString(){return field_3_string.ToString();}
public UnlinkFaceRequest(): base(){return new UnlinkFaceRequest(defaultParameters);}
public virtual void setValue(V value){throw new System.NotSupportedException();}
public string get(string key){return key;}
public IList<string> GetMountTargetSecurityGroups(){return null;}
public virtual MappingInformation GetMappingInformation(){return MappingInformation.CreateMapping(this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.SpaceId, this.Space
public string url { get; set; }
public int field_1_len_sub_expression_count(int field_1_len_sub_expression){return field_1_len_sub_expression_count(field_1_len_sub_expression_count);}
public override int GetHighestFrequencyTerm(){return highestFreqTerm;}
public void DeleteChannel(int channel){Delete(channel, true);}
public IList<Face> GetFaces(){return faces;}
public virtual Escorer source(Escorer source){return source.getDistanceFunction();}
public char get(int index){if (index < 0 || index >= length){throw new IndexOutOfRangeException(index);}return _string[index];}
public virtual void Update(ConfigurationProfile config){this.config = config;}
public virtual IList<LifecycleHook> GetLifecycleHooks(){return lifecycleHooks;}
public virtual IList<Reservation> GetHostReservations(){return host reservations;}
public PredictionContext CreatePredictionContext(RuleContext context){return new PredictionContext(context);}
public virtual string ToString(){return "SXVDEX";}
public virtual string ToString(){return "blame(" + blame + ")";}
public virtual IList<ChangeSet> GetChanges(){return Sharpen.Collections.UnmodifiableList(this._enclosing.GetChangeSet(this.id));}
public override bool AllowNonFastForwards(){return true;}
public FeatRecord(){return new FeatRecord(this.field_1_year, this.field_2_month, this.field_3_year);}
public void PutShort(short value){if (value < 0){throw new java.nio.BufferOverflowException();}p.PutShort(value);p.Release();}
public virtual void SetQuery(string query){this.m_query = query;}
public virtual StashApplyCommand Create(string command){return new StashApplyCommand(command);}
public ICollection<string> GetNames(){return dictionaryNameToID.Keys;}
public int getEffectivePort(String scheme){return effectivePortFor(scheme, null);}
public virtual IList<AssessmentTemplate> GetAssessmentTemplates(){return new List<AssessmentTemplate>(this);}
public virtual void Restore(){cluster = null;}
public void AddShape(IShape shape){this.shapes.Add(shape);}
public override bool Equals(object @object){lock (this){return @object == null;}}
public int indexOf(char c){if (c < 0){return -1;}return indexOf(c, 0);}
public virtual bool IsDelta(){return this is a delta representation;}
public virtual void EmitEOF(){Token t = TokenConstants.EOF;this.Emit(t);}
public virtual void SetName(){userName = GetUsername();}}
public virtual Filter GetRevFilter(){return new Filter(this);}
public virtual void SetTagger(GramPhraseTagger tagger){this.tagger = tagger;}
public virtual BufferSize(){return 8;}
public virtual void trim(){trim() }
public void ReadTopMarginRecord(){WriteShort(field_1_margin);}
public virtual EnvironmentInfoResponse EnvironmentInfo(EnvironmentInfoRequest request){var options = new InvokeOptions();options.RequestMarshaller = EnvironmentInfoRequestMarshaller.Instance;options.ResponseUnmarshaller = EnvironmentInfoResponseUnmarshaller.Instance;return Invoke<EnvironmentInfoResponse>(request, options);}
public virtual void CreateOrUpdatePlayerSessions(object player){if (player == null){player = new Player(this);}if (player.isNew()){player.newSessions = new Hashtable(player.maxNumSessions);}
public ProxySession(string name, int port){throw new System.NotImplementedException();}
public virtual object GetType(){return this._enclosing.GetType();}
public string getScheme(){return scheme;}
public virtual void append(char c){append0(c);}
public new FetchAlbumTagPhotos(params){var options = new InvokeOptions();options.RequestMarshaller = FetchAlbumTagPhotosRequestMarshaller.Instance;options.ResponseUnmarshaller = FetchAlbumTagPhotosResponseUnmarshaller.Instance;return Invoke<FetchAlbumTagPhotosResponse>(request, options);}
public virtual void Delete(){this._enclosing.Delete(this.Members);}
public virtual ReachabilityResponse Reachability(ReachabilityRequest request){var options = new InvokeOptions();options.RequestMarshaller = ReachabilityRequestMarshaller.Instance;options.ResponseUnmarshaller = ReachabilityResponseUnmarshaller.Instance;return Invoke<ReachabilityResponse>(request, options);}
public virtual void clear(){bits = 0;}
public V get ( ) {return this.lastEntryInthisMap;}
public virtual ScalaStreamDistribution Create(StreamDistribution distribution){return new ScalaStreamDistribution{m_distribution = distribution, m_initial = Create(distribution);}
public bool isAbsolute(){return path.isAbsolute();}
public virtual void Disables(){this._enclosing.Dispose ();}
public virtual AliasInformation GetAliasInformation(){return Information about an alias.
public override void MoveToNext(){this.nextEntry = this.tree.GetNextEntryAsTree().GetNextEntry(this);}
public virtual TreeFilter Clone(){return this;}
public override CharFilter Create(TextReader input){return new PersianCharFilter(input);}
public string value { get; set; }
public string list(){return string.Empty ;}
public virtual SignalingChannel Information(IChannel channel){return channel.data;}
public static IP AttachesStaticIP(string ip){if (ip == null){if (_staticIp == null){_staticIp = IP.Parse(ip);}if (_staticIp.Equals(ip)){throw new System.Exception("static IP is a static IP address and cannot be attached to an app with this IP address.");}if (staticIp.Equals(ip.getAddress())){throw new System.Exception("static IP is a static address with no IP address.");}return staticIp;}
public String ToString(){return "this[" + this.phase + ", memory=" + this.bytesUsed + "]";}
public override void SetPostingsFormat(BloomFilteringPostingsFormat formats){this.formats = formats;}
public virtual IList<Template> GetTemplates(){return new List<Template>( ) { };}
public TimerThread(Resolution resolution, Counters thread){return new TimerThread(resolution, counter);}
public void Reset(Graphics2D.DrawingRecord r){drawing = r;}
public virtual IList<Directory> GetDirectories(){return directoryList;}
public virtual byte[] Decode(byte[] bytes, int, int, int, bool, ICollection<byte> keyValPairs){return Decode(bytes, keyValPairs, keyValPairs, _options);}
public virtual void disable(){this._enclosing.disable();}
public override int idealSize(){return this._enclosing._size;}
public virtual void UpdateAssessmentTarget(Task<AzureOperationResponse> task){this.task = task;}
public virtual void Modify(File existingVolume){try{OpenAPI.Util.CreateFile(existingVolume);}catch (IOException, ExodusException){throw new ExodusException(exodus.Error.FILE_INVALID_PARAMETER, exodus.Error.INVALID_PARAMETER);}}
public void MergeCells(Cells cells){Merge(cells, 0, cells.Length);}
public int ReadLengthBytes(FileStream fileStream, long position){long lengthBytes = 0;fileStream.Position = position;return _lengthBytes;}
public virtual void onActivityTaskCompleted(ActivityTask result){onTaskDone(result);}
public virtual void incrementProgress(int amount){current.incrementProgress(amount);}
public bool Equals(CacheEntry entry){return lastModified < entry.lastModified;}
public override NumberRecord Convert(Record rec){return rec.Value;}
public virtual void put(char c){if (this.charCount < c){throw new java.nio.CharBufferException(c.toString());}else{this.charCount = c.length;}
public virtual int RowCount(){return this._enclosing.RowCount;}
public override BeiderMorseFilterFactory Create(string name, List<string> args){return new BeiderMorseFilterFactory(args);}
public virtual float var(int x){return m_variance(x);}
public PersianNormalizationFilterFactory(NormalizationFilterParameters parameters){return new PersianNormalizationFilterFactory(parameters);}
public virtual WeightedTerm[] GetWeightedTerms(){return WeightedTerm.GetValues(this);}
public virtual void Delete(){this.Part = null;this.IsSynthetic = false;this.Remove(this.Part);this.Part2 = null;}
public virtual string ToString(){return "Vocabulary(" + m_vocabulary + ")";}
public short Get(int index){return _ulr[index];}
public string representation(PngImage image){return image.ToString();}
public static double EvaluateRange(double[] values, int start, int end){return EvaluateRange(values, start, end);}
public virtual void ClearWeightBySpanQuery(){ClearWeightBySpanQuery();}
public virtual int findEnd(){return end;}
public virtual SearchIndex Search(SearchIndex terms){return terms.Find(new SearchIndex(q.text, q.order_by)).FirstOrDefault();}
public virtual void Delete(){this.Key = null;this.Value = null;}
public virtual InsertTagsRequest Create(TagQuery query){var options = new InvokeOptions();options.RequestMarshaller = InsertTagsRequestMarshaller.Instance;options.ResponseUnmarshaller = InsertTagsResponseUnmarshaller.Instance;return Invoke<InsertTagsRequest>(request, options);}
public void DeletePrincipalID(string principalID){if (principalID == null){throw new ArgumentException("principalID cannot be null");}if (principalID.Length == 0){throw new ArgumentException("principalID cannot be zero length");}
public virtual NetworkInterface Information(){return this;}
public override void Serialize(byte[] b, int offset){if (offset == 0){throw new System.IndexOutOfRangeException();}if (offset == record.offset){throw new java.nio.BufferUnderflowException();}else{throw new java.nio.BufferUnderflowException();}
public SecurityConfiguration CreateSecurityConfiguration(){return securityConfiguration;}
public virtual ClientVPNConnections GetConnectionDetails(){return connectionDetails;}
public virtual void fill(double[] values, int value){fill(values, value);}
public bool hasNextCells(){return false;}
public virtual StemmerOverrideMap Reset(StemmerOverrideMap other){return this;}
public bool AllFlagsIn(BitSet flagSet){return flagSet.Any();}
public virtual ModifyGuardDutyAccountResponse ModifyGuardDutyAccount(ModifyGuardDutyAccountRequest request){var options = new InvokeOptions();options.RequestMarshaller = ModifyGuardDutyAccountRequestMarshaller.Instance;options.ResponseUnmarshaller = ModifyGuardDutyAccountResponseUnmarshaller.Instance;return Invoke<ModifyGuardDutyAccountResponse>(request, options);}
public override T Get(int k){return Token;}
public void RemoveAt(int index){this.RemoveAt(index);}
public virtual void removeName(Name name){list.remove(name);}
public bool PropertiesEqual(Properties props){return properties.Count == 0;}
public virtual BuildList GetBuildList(RepositoryInfo repo){var options = new InvokeOptions();options.Repository = repo;options.UsePositionIncrements = true;return buildList;}
public virtual void Initialize(MessageWriter writer){this.m_writer = writer;}
public void append(E record){if (record.IsBuggy){appendBuggyRecord(record);}}
public override void Close(){packfile.Close();}
public virtual ModelPackage Info(){return info;}
public Cell(String name, CellHeaderValue cell): base(name, cell){this.name = name;this.data = cell;}
public virtual Deflateor Clone(){return this;}
public virtual UpdateS3ResourcesResponse UpdateS3Resources(UpdateS3ResourcesRequest request){var options = new InvokeOptions();options.RequestMarshaller = UpdateS3ResourcesRequestMarshaller.Instance;options.ResponseUnmarshaller = UpdateS3ResourcesResponseUnmarshaller.Instance;return Invoke<UpdateS3ResourcesResponse>(request, options);}
public GroupQuery Node(){return this;}
public string getQuery(){return path.Query();}
public void RemoveComment(){_comment = null;}
public void Reset(bool b){start = false;stop = b;}
public virtual void Activate(){if (this.active){this.deactivate();}}
public virtual bool detect(){return charset!= null;}
public virtual void SetRetentionPeriod(long retentionPeriod){current.retentionPeriod = retentionPeriod;}
public virtual void DeleteClusterSubnetGroup(){ClusterSubnetGroup result = ClusterSubnetGroupDelete(this.ncloud.GetClusterGroup(), this.ncloud.getSubnetId());}
public virtual string Decode(byte[] data){return Decode(data, 0, data.Length);}
public override int GetDefaultPort(){return DEFAULT_PORT;}
public override void Stop(){base.Stop();}
public override void SeekToTerm(Term term){if (term.IsCurrent){term = term.GetNext();}}
public SeriesToChartGroupRecord(StreamReader in1){return ReadSeriesToChartGroupRecord(in1);}
public virtual void write(char c){throw new System.NotImplementedException();}
public virtual void AuthorizeSecurityGroup(SecurityGroup securityGroup){throw new System.NotSupportedException("You must specify a SecurityGroup before making this call.");}
public virtual void AddFile(FilePath file){segment.AddFile(file);}
public void SetDimensions(){m_width = 0;m_height = 0;}
public virtual bool SetPrecedenceFilterShouldSuppress(){return false;}
public static LOOKIntervalSet Create(ATNState state){return new LOOKIntervalSet(state);}
public void Serialize(Row row){Serialize(row.Row, row.Output);}
public virtual void SetDedupFlag(bool useCaseSensitive){modCount = useCaseSensitive? 2 : 1;}
public java.util.Hashtable(){return new java.util.Hashtable<K, V>.Hashtable(this._capacity, this._loadFactor);}
public virtual V get(K key){return get(key, null);}
public virtual List<HyperParameterTuningJob> GetHyperParameterTuningJobs(){return HyperParameterTuningJob.ByHyperParameterTuningJobName(HyperParameterTuningJobNameRequest.class);}
public override void Delete(){this._enclosing.DeleteTable(this.tableId);}
public virtual bool IsLessThan(ICharSequence text){return _luceneMatchVersion >= 2;}
public override void free(){if (_position < _position){throw new java.nio.BufferUnderflowException();}java.nio.Buffer before_position = free(_position);}
public virtual void UpdateHITType(){this.Type = this.Type;}
public virtual void UpdateRecommenderConfiguration(){this. recommender = new RecommenderConfiguration(this);}
public override bool Equals(BytesRef other){return _bytesRef!= other.BytesRef;}
public virtual int StemChars(string @string){return 0;}
public virtual Snapshots Describe(){return Snapshots;}
public DummyFacetField(string field_name, int dimension, string label){return new DummyFacetField(field_name, field_name, null, null, null, null, null);}
public DocsPart(){return this;}
public virtual string getValue(){return value;}
public java.nio.ShortBuffer get(){return new java.nio.ReadWriteShortBuffer(this);}
public virtual void UpdatePermissions(int permissions){var source = this;if (permissions == _undefined){permissions = dataSources.CreatePermissionSet(source);}else{permissions = permissions;}}
public Record(RecordInput in1){return new Record(in1, in1);}
public virtual int GetNumberOfTabs(){return tabCount;}
public virtual void Delete(ApplicationReference data){Delete(data.ApplicationReference);}
public virtual Version Create(){return Version;}
public java.nio.ByteBuffer slice(){return byteBuffer.slice();}
public sbyte getByte(sbyte b){return b;}
public override void put(long value){if (value < 0){throw new java.lang.StringIndexOutOfBoundsException("value=" + value + " is not valid");}if (value >= 0 && value < buffer.capacity){throw new java.nio.BufferOverflowException();}buffer[index] = value;}
public override void setValue(float newValue){throw new System.NotSupportedException();}
public virtual IList<IToken> GetExpectedTokens(){return GetTokensInRule(this.m_currentRule.m_expectedTokens, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_currentRule.m_highlightedTokens, this.m_highlightedMatchVersion, this.m_highlightedMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_luceneMatchVersion, this.m_rawMatchVersion, this.m_luceneMatchVersion, this.m_highlightedTokens, this.m_highlightedMatchVersion, this.m_highlight
public virtual string ToString(){return "this@" + this;}
public virtual void Initialize(){this.lastDocID = -1;this.wordNum = -1;word = 0;}
public virtual void SetInclude(E include){include.Add(include);}
public virtual void enable(){base.enable();}}
public ValueFiller GetValueFiller(){return this.filler;}
public void Serialize(CellValueRange range){SerializeRange(range.Row, range.Column, range.IsEmpty);}
public static Counter newCounter(long value){return newCounter(value);}
public bool getProperty(){return this.value;}
public virtual void before(){before(null);}}
public override void Reuse(){this.reuse = true;}
public new ExternalBookBlock(){this.numberOfSheets = numberOfSheets;}
public virtual string GetWindowToken(){return "sap-ui-stream";}
public virtual bool SetPushThin(bool thin){return thin;}
public int CompareRecordTimeSec(Tracker other){return RecordTimeSec - other.RecordTimeSec;}
public new ReverseStringFilter(TokenStream stream){return new ReverseStringFilter(stream);}
public BlockList(){return new BlockList<T>.BlockList(this);}
public Scorer(List<string> terms){_terms = terms;}
public override bool Equals(object @object){return Equals(@object);}
public virtual Sharpen.CharacterSet GetCharacterSet(){return _characterSet;}
public virtual Experiments Describe(){return ExperimentData;}
public ESCHERGraphicsInstance(){return (ESCHERGraphicsInstance)Activator.CreateInstance(this.GetType().GetTypeInfo().FullName);}
public virtual string GetPatternText(){return patternText;}
public virtual void Delete(){routeTable = null;}
public void Associate(VPC vpc){_group = vpc.HostedZone;}
public virtual IntegrationDetail CreateOrUpdateIntegration(IntegrationDetail result){this.result = result;}
public override void Set(string key, string value){if (string.IsNullOrEmpty(key)){throw new System.ArgumentException("key cannot be null or empty");}string c = (string)key;if (string.IsNullOrEmpty(c)){throw new java.util.HashMap.EntryNotFoundException(c);}if (c.isNew()){c.set(c.getKey(), c.getValue());}if (c.isRemove()){c.remove(c.getKey()){c.remove(c.getKey());}c.add(key);}c.clear();}
public virtual byte[] Decode(byte[] bytes, 0, bytes.Length);return Decode(bytes, 0, bytes.Length);}
public virtual void Disassociate(Connection connection){if (connection == null){throw new InvalidOperationException("connection cannot be disassociated from an existing lag.");}connection.Disassociate(connection);}
public override FileMode GetOldFileMode(){return mode;}
public virtual string ToString(){return "this@" + this;}
public virtual void Stop(){m_detectionJob = null;}
public Formula ToString(){return "this";}
public virtual IList<DetectorJob> GetDominantLanguageDetectionJobs(){return dominantLanguageDetectionJobs;}
public string slice(){return Slice.ToString(this);}
public int ParseHexInt(){return ParseInt(1, 16);}
public virtual void set (string value){throw new System.NotSupportedException("Unsupported attribute type '" + typeof(value) + "'");}
public virtual StackSetOperation GetStackSetOperation(){return Operation;}
public ICell GetCell(int cellnum){return GetCell(cellnum);}
public void Write(byte[] b){Write(b, 0, b.Length);}
public virtual ResetImageAttributeRequest ResetImageAttribute(ResetImageAttributeRequest request){var options = new InvokeOptions();options.RequestMarshaller = ResetImageAttributeRequestMarshaller.Instance;options.ResponseUnmarshaller = ResetImageAttributeResponseUnmarshaller.Instance;return Invoke<ResetImageAttributeRequest>(request, options);}
public virtual void discard(){this._enclosing.discard();this._enclosing.discard();}
public override ObjectId GetPeeledObjectId(){return this.GetUniversalObjectId();}
public virtual void Undeprecate(){domain.UndeprecateOnCreate();}
public override void Write(int value){throw new System.NotImplementedException();}
public virtual void Dequeue(){this.enq = null;this.enq = null;}
public bool SetCheckFooterAfterPack{SetCheckFooterAfterPack(true);return true;}
public virtual void swap(){elements.CopyTo(0, elements, 0, count);}
public virtual int GetPackedGitWindowSize(){return this._enclosing.GetSize();}
public MetricsUpdate(string name){return Update(name, null, true);}
public virtual Information Recording Information(ElebrityRecognition elebrityRecognition){return Information(elebrityRecognition);}
public virtual CreateQueueRequest CreateQueue(string queueName){var request = new CreateQueue(queueName);request.Queue = queueName;return request;}
public Area3DPxg from SheetIdentifier and SheetRangeIdentifier.
public virtual void SetBaselineTime(double baseline){setTime ( baseline);}
public virtual void MoveAddress(Address address){try{OpenstackEC2.Network.AddRoute(address, this);}catch (IOException e){throw new RuntimeException(e);}}
public virtual string GetName(){return "LM";}
public virtual IList<Tag> GetTags(){return new List<Tag>();}
public virtual long GetOffset(){return offset;}
public virtual void putShorts(int n){if (n < MIN_BLOCK_SIZE){throw new System.ArgumentException("too many shorts (currently only supports " + MIN_BLOCK_SIZE + " chars);}if (n < MAX_BLOCK_SIZE){throw new java.nio.BufferOverflowException();}short[] shorts = { _mainBuffer.getShorts();if (shorts == null){throw new java.nio.BufferOverflowException();}short[] chars = { _mainBuffer.getShorts(n);if (chars.Length == 0){throw new java.nio.BufferOverflowException();}short[] shorts_to_put = { _mainBuffer.getShorts(n);if (shorts.Length < MIN_BLOCK_SIZE){throw new java.nio.BufferOverflowException();}int[] shorts = { _mainBuffer.getShorts();}if (shorts.Length < Short){throw new java.nio.BufferOverflowException();}
public void Initialize(string category){Initialize(category, null);}
public void WriteByte(int b){write(b & 0xff);}
public override IList<Task> GetTasks(){return new List<Task> { this._enclosing.GetTasks(this.importImage);}
public void ReadColumnInfo(RecordInputStream in1){_in1.ReadByte();}
public virtual void SetIndexDiffStatus(IndexDiffStatus status){this._enclosing.SetStatus(status);}
public Experiment(string experimentName, List<string> args) : base(null, experimentName, args){this.experimentName = experimentName;}
public virtual UnknownRecord DeepCopy(){return this;}
public java.nio.FloatBuffer createSlice(java.nio.FloatBuffer buffer, int capacity, byte order){if (order < 0){throw new java.nio.BufferOverflowException();}java.nio.FloatBuffer result = java.nio.FloatBuffer.wrap(new java.nio.FloatBuffer(buffer), offset, length);}
public virtual IList<Schedule> GetSnapshots(){return SnapshotSchedule.ListSnapshots();}
public virtual IList<Picture> GetImages(){return Images;}
public virtual void SetValues(int[] values){foreach (int[] values in values){this.SetValue(values[i]);}}
public String ToString(){return "this";}
public ExcelParser(StreamTokenizer streamTokenizer){_is = streamTokenizer;}
public virtual IList<Tag> GetTags(Photo photo){return Tags;}
public override Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this)))));Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this), Sharpen.Extensions.Create(this), Sharpen.Extensions.Create(this, Sharpen.Extensions.Create(this, Sharpen.Extensions
public static IPAddress AllocatedIp(string name){return IPAddress.Create(name, true);}
public FeatRecord(RecordInputStream in1){return new FeatRecord(in1);}
public virtual MergeResult Merge(MergeRequest request, MergeResponse response) {throw new InvalidOperationException("Unsupported operation (" + this.operation + " does not support merging this commit."); }
public virtual SnapshotSchedule Create(string name){return new SnapshotSchedule(name, 0, 0);}
public NPOI.SS.UserModel.Record next(){return this.nextEntry();}
public virtual string toString(){return buf.ToString();}
public virtual IListTablesResponse ListTables(string table){return ListTables(table, true);}
public virtual void EnableAlarmActions(bool enabled){return;}
public CompositeReaderContext Create(TextReader reader){return (CompositeReaderContext)Create(reader, 0, 0);}
public virtual bool Equals(State[] states){return states[0];}
public EnglishPossessiveFilter(ICharStream input){return new EnglishPossessiveFilter(input);}
public virtual void ClearFormatting(){this.SetFormatting(null);}
public long get(int index) {return get(index);}
public virtual void Delete(){response.Delete(true);}
public virtual string ToString(){return "this@" + m_cnt;}
public static string CreatePresignedDomainURL(string scheme, int port){return "https:" + protocol + "//" + host + "/" + port;}
public virtual void writeChar(char c){throw new System.NotImplementedException();}
public virtual SST Record(){return sst;}
public virtual string ToString(){return "this@" + this;}
public virtual bool IsSaturated(){return saturated;}
public virtual bool SetIgnoreCase(){return ignoreCase();}
public string ToString(){return "this query(" + m_query + ")";}
public virtual void Delete(){this._enclosing.Delete(this.dataSource);}
public virtual void Reboot(){node.Reboot();}
public void ProcessChildRecords(Record[] rawChildRecords){if (rawChildRecords!= null){try{Connection connection = in1.CreateConnection(rawChildRecords);}catch (IOException e){throw new Exception(e.ToString(), e);}}
public virtual void CreateOrUpdateTagsForApp(string appID){CreateOrUpdateTagsFor(appId, tags);}
public virtual string GetSnapshot(){return file.GetSnapshot();}
public java.io.InputStream get(int size){throw new System.NotImplementedException();}
public virtual string ToString(){return "this@" + this;}
public virtual int next(){return this.current.nextIndex();}
public string ToString(){return "this.";}
public virtual Object Clone(){return this;}
public virtual bool IsOutput(){return true;}
public Interface Create(string name, List<string> args){return new List<string>(args);}
public override void Serialize(ILittleEndianOutput out1){out1.Write(password);}
public virtual void Stop(){m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;m_lastDocID = -1;_lastDocID = -1;}
public virtual void SetConnectTimeout(int milliseconds){connectTimeout = milliseconds;}
public virtual GatewayGroup DescribeGatewayGroup(){return gatewayGroup;}
public java.nio.ByteBuffer slice(){return byteBuffer.slice();}
public virtual string join(string separator, char lastSeparator){return join(separator, lastSeparator);}
public override string ToString(){return this.operator.ToString();}
public virtual ListSubscriptionsByTopicArnResponse ListSubscriptionsByTopicArn(ListSubscriptionsByTopicArnResponse listSubscriptionsByTopicArn){return ListSubscriptionsByTopicArn(listSubscriptionsByTopicArn, listSubscriptionsByTopicArn, null);}
public int ReadByte(){return _buf[_ReadIndex++];}
public virtual void Shutdown(){if (shutdown){throw new InvalidOperationException("Shutting down client VPN connections.");}shutdown = true;}
public string queueUrl { get; set; }
public void Serialize(ILittleEndianOutput out1){out1.Write(this.m_format);}
public static int commonValue(OutputOutput one){return _output.commonValue(one);}
public virtual void _new(){if (this.lastEntryOfInstance(this.lastEntryOfInstance(this.lastEntryOfInstance(this.lastEntryOfInstance(this.lastEntryOfInstance(this.lastEntryOfInstance(this.lastEntryOfInstance())))){this.lastEntryOfInstance = this.FirstEntryOfInstance;this.lastEntryOfInstance = this.FirstEntryOfInstance;}this.lastEntryOfInstance = this.FirstEntryOfInstance;}
public int get ( int index_1){if (isNull(index_1)){return -1;}return array[index_1];}
public override void Fill(object o){if (o is NGit.Util.FileHeader){return;}NGit.Util.FileHeader privateFileHeader(int field){if (field == null){throw new ArgumentException("field must be set before filling");}if (field.GetType()!= typeof(NGit.Util.FileHeader)){throw new ArgumentException("field type does not exist");}if (field.GetType()!= typeof(NGit.Util.FileHeader)){throw new ArgumentException("field type does not have default value");}
public CloudFrontOriginAccessIdentity(){return new CloudFrontOriginAccessIdentity(this);}
public virtual bool isNamespaceAware(){return true;}
public virtual void SetOverrideFlag(bool value){override = value;}
public override string ToString(){return this.GetType().FullName;}
public IndexReader GetIndexReader(){return _indexReader;}
public int indexOf(K key){return keys.IndexOf(key);}
public Record(RecordInput in1){return null;}
public long GetLength(){return fileSize;}
public override void ReadPassword(int password){throw new NotSupportedException("You must specify a password record when reading password records from file: " + name + " (currently only supports password records)");}
public java.util.HashMap<K, V>(){return new java.util.HashMap<K, V>(this._capacity, this._loadFactor);}
public virtual void run(){if (this.isMainThread()){runMainThread();}}
public virtual void DeleteLoginProfile(string userName){DeleteUser(userName, true);}
public E peek(){if (this.deque.isEmpty()){return null;}return this.deque.peek() || this.head;}
public Photo Create(Cloud Photo){return Photo;}
public virtual string GetName(){return "resolver";}
public virtual int findEndOfBoundary(){return findEndOfBoundary();}
public virtual void SetObjectChecker(){this._objectChecker = new ObjectChecker(this);}
public override void SetBaseReference(AreaEval areaEval){_baseRef = areaEval.GetBaseReference();}}
public VPCEndpoint(string name){this.name = name;}
public virtual void Deregister(){_workspaceDirectory = null;}
public ChartFRTInfoRecord(RecordInputStream in1){return new ChartFRTInfoRecord(in1);}
public Merger(Parser parser){return new Merger(parser);}
public DataSource(string location, string type, DataSource dataSource){var options = new InvokeOptions();options.Source = dataSource;options.Protocol = ProtocolType.HTTPS;options.ConnectionString = ConnectionString;options.UseValue = UseValue;return new DataSource(this, options);}
public void ClearDecisionToDFA(int n){decisionToDFA.Clear(n);}
public virtual void removeName(Name name){list.remove(name);}
public string GetRightMargin(){return " ";}
public virtual object Clone(){return new object(this.GetType().GetTypeInfo().FullName, this._enclosing);}
public virtual void AddProcessors(){AddProcessor(this);}
public String ToString(){return "Cell " + cell;}
public override void put(byte b){if (this.byteIndex < 0){throw new java.nio.BufferOverflowException();}this.byteIndex = this.byteIndex;this.size = this.byteIndex;}
public override void SetFileMode(FileMode mode){this.mode = mode;}
public java.nio.ByteBuffer slice(){return byteBuffer.slice();}
public override void setValueAt(int index, V value){base.setValue(index, value);}
public virtual void put(float v){if (m_len >= m_buf.Length){throw new System.IO.IOException();}m_len += v;}
public static double EvaluateMax(double[] values){return EvaluateMax(values, 0);}
public webhook.params = webhook.params;}
public void Delete(){domainNameItem = null;}
public String ToString(){return "this";}
public virtual bool Success(bool commit){return commit? true : false;}
public override void Set(byte b){if (m_len >= m_buf.Length){throw new System.ArgumentException("m_len <= 0");}m_buf = b;}
public virtual ConnectionsInformation GetConnection(){return connections;}
public DeletePhotos request(IDictionary<string, List<string>> customHeaders = null, useQueryString = false){return CreateDeletePhotosRequest(customHeaders, customHeaders, https:true);}
public virtual void push(E e){if (this.e == e){this.e = e;this.n = 0;this.r = e;this.s = this._enclosing.push(e);}}
public static java.nio.ReadWriteHeapBuffer newReadWriteHeapBuffer(int capacity){java.nio.ByteBuffer buffer = new java.nio.ReadWriteHeapBuffer(capacity);buffer.position = 0;buffer.limit = capacity;buffer.rewind = recycle;}
public virtual SubQuery GetSubQuery(int index){return this;}
public virtual Score GetCurrentScore(){return score;}
public virtual string ToString(){return "Vocabulary(" + m_vocabulary + ")";}
public virtual ILogPattern Information(ILogPattern pattern){return pattern.Information;}
public virtual void RegisterMulticastGroupMembers(IList<MulticastGroup> multicastGroups){_multicastGroups.Add(new MulticastGroup(this));}
public virtual PhoneNumber GetPhoneNumbers(){return PhoneNumber.GetPhoneNumbers(this.PhoneNumber);}
public virtual object getData(){return data;}
public virtual bool IsDirected(IToken token){return this.GetType().IsInstanceOf(ITokenPrecedencePredicate);}
public virtual void DeleteServerCertificate(X509Certificate certificate){if (certificate.Name == null){throw new System.ArgumentException("server certificate name cannot be null");}if (certificate.IsServerCertificate){throw new System.ArgumentException("server certificate name cannot be null");}delete(certificate.Name);}
public virtual void appendDouble(double value){append0(value);}
public virtual Information about an evaluation.
public string GetName(){return null;}
public virtual bool matches(string text){return false if there is a match in the input.
public virtual LifecyclePolicyPreview GetLifecyclePolicyPreview(){return GetLifecyclePolicyPreviewInternal(null, null);}
public virtual TokenStream Create(ITokenStream tokenStream){return new TokenStream(tokenStream, true);}
public virtual void Serialize(ObjectOutput out1){out1.Write(this.GetType().GetTypeInfo().FullName);}
public virtual string ToString(){return "this@" + this;}
public virtual void SetRemoteAddress(){remote = true;}
public void Collapse(int row, int column){startCol = row;startRow = row;startCol = column;}
public void AttachSkillGroup(SkillGroup group){this.m_skillGroup = group;}
public virtual string ToString(){return "SURT(" + m_surt + ")";}
public QueryConfigHandler GetQueryConfigHandler(){return queryConfigHandler;}
public virtual string GetType(){return this.m_type.FullName;}
