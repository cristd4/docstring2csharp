public void serialize(LittleEndianOutput out) {out.writeShort(Venter);}
public void addAll(List<E> src) {while (src!= null) {add(0, src);src = src.next;}src = null;}
public void writeByte(byte value) throws IOException {checkWritePrimitiveTypes();primitiveTypes.writeByte(value);}
public RevObject getObjectId() {return id;}
public DeleteDomainEntryResult deleteDomainEntry(DeleteDomainEntryRequest request) {request = beforeClientExecution(request);return executeDeleteDomainEntry(request);}
public long ramBytesUsed() {return ((termOffsets!=null)? termOffsets.ramBytesUsed() : 0) +((termsDictOffsets!=null)? termsDictOffsets.ramBytesUsed() : 0);}
public final String getFullMessage() {byte[] raw = buffer;int msgB = RawParseUtils.commitMessage(raw, 0);if (msgB < 0) {return ""; }return RawParseUtils.decode(guessEncoding(), raw, msgB, raw.length);}
public CreatePOIFSFileSystemResult createPOIFS(CreatePOIFSRequest request) {request = beforeClientExecution(request);return executeCreatePOIFS(request);}
public ByteBlock(int address) {assert address >= 0;this.address = address;this.bytes = new byte[address];}
public SubmoduleCommand setPath(String path) {checkCallable();this.path = path;return this;}
public ListUploadsResult listUploads(ListUploadsRequest request) {request = beforeClientExecution(request);return executeListUploads(request);}
public StandardSyntaxParserTokenManager(CharStream stream, int lexState){this(stream);SwitchTo(lexState);}
public ShardKeyIteratorIterator(int shardId) {return new ShardKeyIterator(shardId);}
public ModifyStrategyRequest() {super("cr", "2016-06-07", "ModifyStrategy", "cr");setUriPattern("/modifyStrategy");setMethod(MethodType.POST);}
public boolean ready() throws IOException {return false;}
public boolean getOptRecord() {return optRecord;}
public int read(byte[] buffer, int offset, int length) throws IOException {assert offset + length <= buffer.length;assert length > 0: "length=" + length;if (offset + length >= buffer.length) {throw new IndexOutOfBoundsException("offset=" + offset + " of " + buffer.length);}assert length > 0: "length=" + length + " of " + buffer.length;return length;}
public List<NLPSentenceBreak> getWordIterator(String sentenceOperation) {return getWordIterator(sentenceOperation, null);}
public void println(String str) {synchronized (lock) {print(str);}}
public NotImplementedFunctionException(String functionName, Exception cause) {super(cause.getMessage());this.functionName = functionName;this.cause = cause;}
public V next() {return nextEntry().value; }
public void readFully(byte[] buf, int offset, int len) {if (offset + len > buf.length) {throw new BufferUnderflowException();}if (buf == null) {return;}if (offset + len > buf.length) {throw new BufferUnderflowException();}if (pos + len > buf.length) {throw new BufferUnderflowException();}if (pos + len > buf.length) {throw new BufferUnderflowException();}if (buf == null) {throw new BufferUnderflowException();}}
public TagQueueResult tagQueue(TagQueueRequest request) {request = beforeClientExecution(request);return executeTagQueue(request);}
public void remove() {throw new UnsupportedOperationException();}
public CacheSubnetGroup modifyCacheSubnetGroup(ModifyCacheSubnetGroupRequest request) {request = beforeClientExecution(request);return executeModifyCacheSubnetGroup(request);}
public void setLanguage(String language, String country, String variant) {setLanguage(language);setCountry(country);setVariant(variant);}
public DeleteDocumentationVersionResult deleteDocumentationVersion(DeleteDocumentationVersionRequest request) {request = beforeClientExecution(request);return executeDeleteDocumentationVersion(request);}
public boolean equals(Object o) {if (o instanceof FacetLabel) {return label.equals(((FacetLabel)o).label);}return false;}
public DescribeInstanceAccessDetailsResult describeInstanceAccessDetails(DescribeInstanceAccessDetailsRequest request) {request = beforeClientExecution(request);return executeDescribeInstanceAccessDetails(request);}
public HSSFPolygon createPolygon(HSSFClientAnchor anchor) {HSSFPolygon shape = new HSSFPolygon(null, anchor);addShape(shape);onCreate(shape);return shape;}
public String getSheetName(int sheetIndex) {return _uBook.getSheetName(sheetIndex);}
public GetDashboardResult getDashboard(GetDashboardRequest request) {request = beforeClientExecution(request);return executeGetDashboard(request);}
public AssociateAWSAccountResult associateAWSAccount(AssociateAWSAccountRequest request) {request = beforeClientExecution(request);return executeAssociateAWSAccount(request);}
public void addBlanks(String[] blanks) {addBlanks(new StringBuffer(blanks.length));}
public String quote(String in) {final StringBuilder r = new StringBuilder();r.append('\'');int start = 0, i = 0;for (; i < in.length(); i++) {switch (in.charAt(i)) {case '\'':case '!':r.append(in, start, i);r.append('\'');r.append('\\');r.append(in.charAt(i));r.append('\'');start = i + 1;break;}}r.append(in, start, i);r.append('\'');return r.toString();}
public ByteBuffer putInt(int value) {throw new ReadOnlyBufferException();}
public void setArrayValue(double[] arr, int arrSize) {if (arr == null) {throw new IllegalArgumentException("arr must not be null");}if (arr.length < arrSize) {throw new IllegalArgumentException("arr size must be greater than arrSize");}this.arr = arr;this.size = len = arr.length;}
public DescribeIceServerConfigurationResult describeIceServerConfiguration(DescribeIceServerConfigurationRequest request) {request = beforeClientExecution(request);return executeDescribeIceServerConfiguration(request);}
public String toString() {return getClass().getName() + " [" +getStringValue() +"]";}
public String toString(String field) {return "ToBlockJoinQuery ("+fieldQuery + ")";}
public void incRef() {refCount.incrementAndGet();}
public UpdateConfigurationSetSendingEnabledResult updateConfigurationSetSendingEnabled(UpdateConfigurationSetSendingEnabledRequest request) {request = beforeClientExecution(request);return executeUpdateConfigurationSetSendingEnabled(request);}
public int getFirstXBATOffset() {return first_xbat_offset;}
public static double mult(double a, int b) {double result = 0;if (b < 0) {result *= a;}else if (b > 0) {result &= -b;}return result;}
public String toString() {return "fileName=" + fileName + " size=" + size;}
public void setCredentialsFetcher(AlibabaCloudCredentialsFetcher fetcher) {this.fetcher = fetcher;}
public SubmoduleAddCommand setProgressMonitor(ProgressMonitor monitor) {this.monitor = monitor;return this;}
public void reset() {if (!first()) {ptr = treeStart;nextSubtreePos = 0;currentEntry = null;currentSubtree = null;if (!eof())parseEntry();}}
public E previous() {if (expectedModCount == modCount) {try {E result = get(pos);lastPosition = pos;pos--;return result;} catch (IndexOutOfBoundsException e) {throw new NoSuchElementException();}}throw new ConcurrentModificationException();}
public char getNewPrefix() {return newPrefix;}
public int indexOfValue(E value) {if (mGarbage) {gc();}for (int i = 0; i < mSize; i++)if (mValues[i] == value)return i;return -1;}
public ListUniqueStemsResult listUniqueStems(String word) {return listUniqueStems(word, -1);}
public GetGatewayResponsesResult getGatewayResponses(GetGatewayResponsesRequest request) {request = beforeClientExecution(request);return executeGetGatewayResponses(request);}
public void mark(int readLimit) throws IOException {if (readLimit < 0) {throw new IllegalArgumentException();}synchronized (lock) {checkNotClosed();markpos = pos;}}
public long skip(long byteCount) throws IOException {if (byteCount < 0) {throw new IllegalArgumentException("byteCount < 0: " + byteCount);}synchronized (lock) {checkNotClosed();if (byteCount < 1) {return 0;}if (end - pos >= byteCount) {pos += byteCount;return byteCount;}long read = end - pos;pos = end;while (read < byteCount) {if (fillBuf() == -1) {return read;}if (end - pos >= byteCount - read) {pos += byteCount - read;return byteCount;}read += (end - pos);pos = end;}return byteCount;}}
public BootstrapActionConfig(String name, ScriptBootstrapActionConfig scriptBootstrapActionConfig) {setName(name);setScriptBootstrapActionConfig(scriptBootstrapActionConfig);}
public void serialize(LittleEndianOutput out) {out.writeShort(sid);out.writeShort(0x00);out.writeShort(0x01);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.writeShort(0x00);out.write
public int lastIndexOf(char s[], int len) {for (int i = 0; i < s.length; i++)if (s[i] == s[len-1])return i;return -1;}
public void addLast(E object) {addLastImpl(object);}
public void set(String section, String subSection) {if (null == section || null == subSection) {throw new NullPointerException();}this.section = section;this.subSection = subSection;}
public String getTag() {return tag;}
public boolean addSubRecord(SubRecord o) {return subrecords.add(o);}
public boolean remove(Object object) {Iterator<?> it = iterator();if (object!= null) {while (it.hasNext()) {if (object.equals(it.next())) {it.remove();return true;}}} else {while (it.hasNext()) {if (it.next() == null) {it.remove();return true;}}}return false;}
public DoubleMetaphoneFilter(double min, double max) {super(min, max);this.min = min;this.max = max;}
public long getSize() {return delegate().getSize();}
public void setValue(boolean value) {_value = value? 1 : 0;_isError = false;}
public CreateContentSourcesResult createContentSources(CreateContentSourcesRequest request) {request = beforeClientExecution(request);return executeCreateContentSources(request);}
public E valueAt(int index) {if (mGarbage) {gc();}return (E) mValues[index];}
public CreateRepoRequest() {super("cr", "2016-06-07", "CreateRepo", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]");setMethod(MethodType.PUT);}
public boolean isDeltaBaseAsOffset() {return false;}
public void remove() {if (last == null) {throw new IllegalStateException();}removeInternal(last);expectedModCount = modCount;last = null;}
public MergeShardsResult mergeShards(MergeShardsRequest request) {request = beforeClientExecution(request);return executeMergeShards(request);}
public AllocateHostedConnectionResult allocateHostedConnection(AllocateHostedConnectionRequest request) {request = beforeClientExecution(request);return executeAllocateHostedConnection(request);}
public int getStartIndex() {return start;}
public static final WeightedTerm[] getTerms(Query query, boolean prohibited){return getTerms(query,prohibited,null);}
public LongBuffer compact() {throw new ReadOnlyBufferException();}
public void decode(byte[] blocks, int blocksOffset, long[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long byte0 = blocks[blocksOffset++] & 0xFF;final long byte1 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 6) | (byte1 >>> 2);final long byte2 = blocks[blocksOffset++] & 0xFF;final long byte3 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte1 & 3) << 12) | (byte2 << 4) | (byte3 >>> 4);final long byte4 = blocks[blocksOffset++] & 0xFF;final long byte5 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte3 & 15) << 10) | (byte4 << 2) | (byte5 >>> 6);final long byte6 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte5 & 63) << 8) | byte6;}}
public String getName() {return name;}
public DescribeNotebookInstanceLifecycleConfigResult describeNotebookInstanceLifecycleConfig(DescribeNotebookInstanceLifecycleConfigRequest request) {request = beforeClientExecution(request);return executeDescribeNotebookInstanceLifecycleConfig(request);}
public String getAccessKeySecret() {return accessKeySecret;}
public CreateVpnConnectionResult createVpnConnection(CreateVpnConnectionRequest request) {request = beforeClientExecution(request);return executeCreateVpnConnection(request);}
public ListVoiceResult listVoice(ListVoiceRequest request) {request = beforeClientExecution(request);return executeListVoice(request);}
public ListExecutionsResult listExecutions(ListExecutionsRequest request) {request = beforeClientExecution(request);return executeListExecutions(request);}
public GetVaultJobRequest(String vaultName, String jobId) {setVaultName(vaultName);setJobId(jobId);}
public EscherRecord getEscherRecord(int index) {return escherRecords.get(index);}
public GetAppsResult getApps(GetAppsRequest request) {request = beforeClientExecution(request);return executeGetApps(request);}
public DeleteSmsChannelResult deleteSmsChannel(DeleteSmsChannelRequest request) {request = beforeClientExecution(request);return executeDeleteSmsChannel(request);}
public TrackingRefUpdate getTrackingRefUpdate() {return trackingRefUpdate;}
public void print(boolean bool) {print(String.valueOf(bool));}
public Token getFirstChild() {Token t = token;if (t.getType() == Token.EOF) {return null;}return (token.getType() == Token.EOF)? null : token.getFirstChild();}
public void setWorkingDirectoryTreeIndex(int index) {if (index < 0) {throw new IllegalArgumentException("Invalid index: " + index);} else {_workingDirectoryTreeIndex = index;}}
public AreaRecord(RecordInputStream in) {field_1_area_number = in.readShort();}
public GetThumbnailRequest() {super("CloudPhoto", "2017-07-11", "GetThumbnail", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public DescribeTransitGatewayVpcAttachmentsResult describeTransitGatewayVpcAttachments(DescribeTransitGatewayVpcAttachmentsRequest request) {request = beforeClientExecution(request);return executeDescribeTransitGatewayVpcAttachments(request);}
public PutVoiceConnectorStreamingConfigurationResult putVoiceConnectorStreamingConfiguration(PutVoiceConnectorStreamingConfigurationRequest request) {request = beforeClientExecution(request);return executePutVoiceConnectorStreamingConfiguration(request);}
public synchronized int getOrdIndex(int dimension) {return mDimensionValues[dimension];}
public String toString() {final StringBuilder r = new StringBuilder();r.append(getSeverity().name().toLowerCase(Locale.ROOT));r.append(": at offset "); r.append(getOffset());r.append(": "); r.append(getMessage());r.append("\n"); r.append("  in "); r.append(getLineText());return r.toString();}
public E getFirst() {return getFirstImpl();}
public CreateWorkspacesResult createWorkspaces(CreateWorkspacesRequest request) {request = beforeClientExecution(request);return executeCreateWorkspaces(request);}
public BottomMarginRecord clone() {return copy();}
public ListRepositoriesResult listRepositories(ListRepositoriesRequest request) {request = beforeClientExecution(request);return executeListRepositories(request);}
public int[] newSparseIntArray(int initialCapacity) {if (initialCapacity < 0) {throw new IllegalArgumentException();}return new int[initialCapacity];}
public TokenStream create(TokenStream input) {return new HyphenatedWordsFilter(input);}
public CreateDistributionWithTagsResult createDistributionWithTags(CreateDistributionWithTagsRequest request) {request = beforeClientExecution(request);return executeCreateDistributionWithTags(request);}
public static RandomAccessFile createRandomAccessFile(String file_name, int blockSize) throws IOException {if (blockSize < 0) {throw new IllegalArgumentException("blockSize must be >= 0");}if (blockSize < 0) {throw new IllegalArgumentException("blockSize must be >= 0");}return new RandomAccessFile(file_name, blockSize).getRandomAccessFile();}
public DeleteWorkspaceImageResult deleteWorkspaceImage(DeleteWorkspaceImageRequest request) {request = beforeClientExecution(request);return executeDeleteWorkspaceImage(request);}
public static String longToHex(long value) {StringBuilder sb = new StringBuilder(18);writeHex(sb, value, 16, "0x");return sb.toString();}
public UpdateDistributionResult updateDistribution(UpdateDistributionRequest request) {request = beforeClientExecution(request);return executeUpdateDistribution(request);}
public byte colorAt(int index) {return (byte) colorAt(index);}
public ValueEval evaluate(int srcRowIndex, int srcColumnIndex,ValueEval arg0, ValueEval arg1) {return func.evaluate(srcRowIndex, srcColumnIndex, arg0, arg1);}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_number_of_sheets);if(isExternalReferences()) {StringUtil.writeUnicodeString(out, field_2_encoded_url);for (String field_3_sheet_name : field_3_sheet_names) {StringUtil.writeUnicodeString(out, field_3_sheet_name);}} else {int field2val = _isAddInFunctions? TAG_ADD_IN_FUNCTIONS : TAG_INTERNAL_REFERENCES;out.writeShort(field2val);}}
public DescribeDBEngineVersionsResult describeDatabaseEngineVersions() {return describeDatabaseEngineVersions(new DescribeDBEngineVersionsRequest());}
public void setCharModel(char c, int fontIndex) {this.c = c;this.fontIndex = fontIndex;}
public static byte[] convert(char[] s, int offset, int length) {return convert(s, offset, length, true);}
public UploadArchiveResult uploadArchive(UploadArchiveRequest request) {request = beforeClientExecution(request);return executeUploadArchive(request);}
public List<Token> getHiddenTokensToLeft(int tokenIndex) {return getHiddenTokensToLeft(tokenIndex, -1);}
public boolean equals(Object obj) {if (this == obj) return true;if (obj == null) return false;if (getClass()!= obj.getClass()) return false;AutomationQuery other = (AutomationQuery) obj;if (automatonQuery!= other.automatonQuery) return false;if (automatonQuery.compareTo(other.automatonQuery)!= 0) return false;return true;}
public HSSFSpanClause createSpanClause(WeightBySpanQuery weightBySpanQuery) {if (weightBySpanQuery == null) {return null;}if (weightBySpanQuery.length > 0) {return new HSSFSpanClause(Integer.toString(weightBySpanQuery.length));} else {return new HSSFSpanClause(Integer.toString(weightBySpanQuery.length));}}
public CreateStashResult createStash(CreateStashRequest request) {request = beforeClientExecution(request);return executeCreateStash(request);}
public FieldInfo fieldInfo(String fieldName) {return byName.get(fieldName);}
public GetEventSourceResult getEventSource(GetEventSourceRequest request) {request = beforeClientExecution(request);return executeGetEventSource(request);}
public DescribeDocumentAnalysisResult describeDocumentAnalysis(DescribeDocumentAnalysisRequest request) {request = beforeClientExecution(request);return executeDescribeDocumentAnalysis(request);}
public CancelUpdateStackResult cancelUpdateStack(CancelUpdateStackRequest request) {request = beforeClientExecution(request);return executeCancelUpdateStack(request);}
public ModifyLoadBalancerAttributeResult modifyLoadBalancerAttribute(ModifyLoadBalancerAttributeRequest request) {request = beforeClientExecution(request);return executeModifyLoadBalancerAttribute(request);}
public SetInstanceProtectionResult setInstanceProtection(SetInstanceProtectionRequest request) {request = beforeClientExecution(request);return executeSetInstanceProtection(request);}
public ModifyDBProxyResult modifyDBProxy(ModifyDBProxyRequest request) {request = beforeClientExecution(request);return executeModifyDBProxy(request);}
public StringBuilder add(char[] range, int start, int end) {add(range, start, end);return this;}
public FetchLibrariesRequest() {super("CloudPhoto", "2017-07-11", "FetchLibraries", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public boolean exists() {return exists;}
public CountingOutputStream(OutputStream out) {this.out = out;}
public NewCSKClusterRequest() {super("cr", "2016-06-07", "CreateCSKCluster", "cr");setUriPattern("/v2/cluster/[CSK]\n");setMethod(MethodType.PUT);}
public static DVConstraint createTimeConstraint(int comparisonOperator, String expr1, String expr2) {if (expr1 == null) {throw new IllegalArgumentException("expr1 must be supplied");}OperatorType.validateSecondArg(comparisonOperator, expr1);String formula1 = getFormulaFromTextExpression(expr1);Double value1 = formula1 == null? convertTime(expr1) : null;String formula2 = getFormulaFromTextExpression(expr2);Double value2 = formula2 == null? convertTime(expr2) : null;return new DVConstraint(ValidationType.TIME, comparisonOperator, formula1, formula2, value1, value2, null);}
public GetObjectParentPathsResult getObjectParentPaths(GetObjectParentPathsRequest request) {request = beforeClientExecution(request);return executeGetObjectParentPaths(request);}
public DescribeCacheSubnetGroupsResult describeCacheSubnetGroups(DescribeCacheSubnetGroupsRequest request) {request = beforeClientExecution(request);return executeDescribeCacheSubnetGroups(request);}
public void setSharedFormula(boolean sharedFormula) {this.sharedFormula = sharedFormula;}
public boolean isReuse() {return false;}
public ErrorNode(TokenStream input, TokenStream tokenStream,String message) {ErrorNode err = new ErrorNode(input, message);input.add(err);err.setToken(tokenStream);return err;}
public LatvianStemFilter(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public EventSubscription removeSourceIdentifierFromEventSubscription(RemoveSourceIdentifierFromEventSubscriptionRequest request) {request = beforeClientExecution(request);return executeRemoveSourceIdentifierFromEventSubscription(request);}
public static TokenFilterFactory forName(String name, Map<String,String> args) {return new TokenFilterFactory(name, args);}
public AddAlbumPhotosRequest() {super("CloudPhoto", "2017-07-11", "AddAlbumPhotos", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public ThreatIntelSet getThreatIntelSet(GetThreatIntelSetRequest request) {request = beforeClientExecution(request);return executeGetThreatIntelSet(request);}
public RevFilter clone() {final RevFilter[] s = new RevFilter[subfilters.length];for (int i = 0; i < s.length; i++)s[i] = subfilters[i].clone();return new List(s);}
public boolean equals( Object o ) {if (o instanceof ArmenianStemmer) {return true;} else {return false;}}
public boolean matches(Object value) {if (value instanceof Object[]) {return false;}if (!(value instanceof Object)) {return false;}final Object[] value = value;if (value instanceof Object) {return true;}if (value instanceof String) {return false;}final String name = name.toLowerCase(Locale.ROOT);final String value = value.toLowerCase(Locale.ROOT);final String arraySize = value.length() + 1;for (int i = 0; i < arraySize; i++) {if (value.length() > name.length()) {return false;}}return true;}
public UpdateContributorInsightsResult updateContributorInsights(UpdateContributorInsightsRequest request) {request = beforeClientExecution(request);return executeUpdateContributorInsights(request);}
public void unwriteProtectWorkbook(ProtectWorkbookRequest request) {request = beforeClientExecution(request);executeUnwriteProtectWorkbook(request);}
public Builder(boolean dedup, boolean expand) {this.dedup = dedup;this.expand = expand;}
public RequestSpotInstanceListResult requestSpotInstanceList(RequestSpotInstanceListRequest request) {request = beforeClientExecution(request);return executeRequestSpotInstanceList(request);}
public byte[] getData() {return[];}
public GetContactAttributesResult getContactAttributes(GetContactAttributesRequest request) {request = beforeClientExecution(request);return executeGetContactAttributes(request);}
public String toString() {return key + "=" + value;}
public ListTextTranslationJobsResult listTextTranslationJobs(ListTextTranslationJobsRequest request) {request = beforeClientExecution(request);return executeListTextTranslationJobs(request);}
public GetContactMethodsResult getContactMethods(GetContactMethodsRequest request) {request = beforeClientExecution(request);return executeGetContactMethods(request);}
public int getFunctionIndex(String functionName) {for (int i = 0; i < functions.length; i++) {Function func = functions[i].getFunction();if (func!= null && func.getName().equals(nameName)) {return i;}}return -1;}
public GetAnomalyDetectorsResult getAnomalyDetectors(GetAnomalyDetectorsRequest request) {request = beforeClientExecution(request);return executeGetAnomalyDetectors(request);}
public void insertObjectId(ObjectIdInfo info) {throw new UnsupportedOperationException();}
public long getSize(Object obj) {if (obj == null) {return 0;} else {return arg.getClass().getSimpleName().length();}}
public ImportInstallationMediaResult importInstallationMedia(ImportInstallationMediaRequest request) {request = beforeClientExecution(request);return executeImportInstallationMedia(request);}
public UpdateEventHookStatusResult updateEventHookStatus(UpdateEventHookStatusRequest request) {request = beforeClientExecution(request);return executeUpdateEventHookStatus(request);}
public int readNumber() {if (_counter == 0) {return 2;} else if (_counter % 2!= 0) {_counter = 2;_counter++;} else {_counter = 0;_counter++;}return _counter;}
public GetFieldLevelEncryptionConfigResult getFieldLevelEncryptionConfig(GetFieldLevelEncryptionConfigRequest request) {request = beforeClientExecution(request);return executeGetFieldLevelEncryptionConfig(request);}
public GetDetectorResult getDetector(GetDetectorRequest request) {request = beforeClientExecution(request);return executeGetDetector(request);}
public DescribeInstanceStatusResult describeInstanceStatus(DescribeInstanceStatusRequest request) {request = beforeClientExecution(request);return executeDescribeInstanceStatus(request);}
public DeleteAlarmResult deleteAlarm(DeleteAlarmRequest request) {request = beforeClientExecution(request);return executeDeleteAlarm(request);}
public TokenStream create(TokenStream input) {return new PortugueseStemFilter(input);}
public CreateFtCblsSubRecordResult createFtCblsSubRecord(CreateFtCblsSubRecordRequest request) {request = beforeClientExecution(request);return executeCreateFtCblsSubRecord(request);}
public void remove(Object object) {throw new UnsupportedOperationException();}
public GetDedicatedIpResult getDedicatedIp(GetDedicatedIpRequest request) {request = beforeClientExecution(request);return executeGetDedicatedIp(request);}
public String toString() {return "priority[" + priority + "]";}
public ListStreamProcessorsResult listStreamProcessors(ListStreamProcessorsRequest request) {request = beforeClientExecution(request);return executeListStreamProcessors(request);}
public DeleteLoadBalancerPolicyRequest(String loadBalancerName, String policyName) {setLoadBalancerName(loadBalancerName);setPolicyName(policyName);}
public WindowProtectOptions() {super("cr", "2016-06-07", "WindowProtect", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/protection/[ProtectOptions]/options");setMethod(MethodType.POST);}
public UnbufferedCharStream(int bufferSize) {if (bufferSize < 0) {throw new IllegalArgumentException("bufferSize must be >= 0");}return new UnbufferedCharStream(bufferSize);}
public GetOperationsResult getOperations(GetOperationsRequest request) {request = beforeClientExecution(request);return executeGetOperations(request);}
public int copyTo(byte[] b, int offset) {int toCopy = 0;for (IndexableField field : fields) {toCopy += field.getSize() * (field.getSize() + 1);}toCopy += toCopy;return toCopy;}
public SingleRecord(RecordInputStream in) {field_1_row_offset = in.readUShort();field_2_col_offset = in.readShort();field_3_row_offset = in.readShort();field_4_col_offset = in.readShort();field_5_row_offset = in.readShort();field_6_col_offset = in.readShort();field_7_row_offset = in.readShort();field_8_height = in.readShort();}
public StopWorkspacesResult stopWorkspaces(StopWorkspacesRequest request) {request = beforeClientExecution(request);return executeStopWorkspaces(request);}
public void close() throws IOException {throw new UnsupportedOperationException();}
public GetMatchmakingRuleSetsResult getMatchmakingRuleSets(GetMatchmakingRuleSetsRequest request) {request = beforeClientExecution(request);return executeGetMatchmakingRuleSets(request);}
public String getPronunciation(int wordId) {return super.getPronunciation(wordId);}
public String getPath() {return path.getPath();}
public static double devSq(double d0, double v1, double v2) {double devSq = (double) (v0 * d0 + v1*v1;double devSq = (double) (v2 * v2);return devSq;}
public GetResizeResult getResize(GetResizeRequest request) {request = beforeClientExecution(request);return executeGetResize(request);}
public boolean isNonGreyPasses() {return nonGreyPasses;}
public int end(String string) {int p = string.length();if (p == 0) {return -1;}for (int i = 0; i < p; i++) {p = p;}return p;}
public void traverseSheet(String firstSheetName, String lastSheetName, int start, int end) {String lastSheetName = firstSheetName;if (lastSheetName == null) {lastSheetName = sheetName;} else if (firstSheetName.equals(lastSheetName)) {nextSheetName = lastSheetName;if (start > end) {throw new IllegalArgumentException("start > end: " + firstSheetName + " vs " + endSheetName);} else if (start > endEndName.equals(lastSheetName)) {nextSheetName = lastSheetName;} else if (start > endEndName.equals(firstSheetName)) {lastSheetName = firstSheetName;} else if (endSheetName.equals(lastSheetName)) {nextSheetName = lastSheetName;}}}
public int getReadIndex() {return index;}
public int compareTo(ScoreTerm other) {if (term.bytesEquals(other.term))return 0; if (this.boost == other.boost)return other.term.compareTo(this.term);else return Float.compare(this.boost, other.boost);}
public int normalize(char s[], int len) {for (int i = 0; i < len; i++) {switch (s[i]) {case ALEF_MADDA:case ALEF_HAMZA_ABOVE:case ALEF_HAMZA_BELOW:s[i] = ALEF;break;case DOTLESS_YEH:s[i] = YEH;break;case TEH_MARBUTA:s[i] = HEH;break;case TATWEEL:case KASRATAN:case DAMMATAN:case FATHATAN:case FATHA:case DAMMA:case KASRA:case SHADDA:case SUKUN:len = delete(s, i, len);i--;break;default:break;}}return len;}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_print_headers);}
public void setExactOnly(boolean b) {exactOnly = b;}
public AttributeValue(String attributeName, int keyType) {setAttributeName(attributeName);setKeyType(keyType);}
public GetAssignmentDetailResult getAssignmentDetail(GetAssignmentDetailRequest request) {request = beforeClientExecution(request);return executeGetAssignmentDetail(request);}
public boolean hasObject(AnyObjectId id) {for (int i = 0; i < count; i++) {if (id.equals(ids[i])) {return true;}}return false;}
public GroupingSearch setAllGroupsToInclude(boolean allGroupsToInclude) {this.allGroupsToInclude = allGroupsToInclude;return this;}
public void setMultiValued(String dim, boolean multiValued) {if (dim == null) {throw new IllegalArgumentException("dim must not be null");}_isMultiValued = multiValued;}
public int getCellsPnt() {int size = 0;for (Row row : rows)size += row.getCellsPnt();return size;}
public DeleteVoiceConnectorResult deleteVoiceConnector(DeleteVoiceConnectorRequest request) {request = beforeClientExecution(request);return executeDeleteVoiceConnector(request);}
public DeleteLifecyclePolicyResult deleteLifecyclePolicy(DeleteLifecyclePolicyRequest request) {request = beforeClientExecution(request);return executeDeleteLifecyclePolicy(request);}
public void write(byte[] b) {try {super.write(b);} catch (IOException e) {throw new RuntimeException(e);}}
public RebaseResult getRebaseResult() {return result;}
public static int getNearestSetSizeForDesiredSaturation(int maxNumberOfSaturations, int minNumberOfSaturations, int maxNumberOfSaturations) {int set = 0;for (int i = 0; i < maxNumberOfSaturations; i++) {if (i > maxNumberOfSaturations) {break;}set = getNearestSet(i, maxNumberOfSaturations, set);}return set;}
public GetDashboardResult getDashboard(GetDashboardRequest request) {request = beforeClientExecution(request);return executeGetDashboard(request);}
public CreateSegmentResult createSegment(CreateSegmentRequest request) {request = beforeClientExecution(request);return executeCreateSegment(request);}
public String toFormulaString(String[] operands) {StringBuilder buf = new StringBuilder();if(isExternalFunction()) {buf.append(operands[0]); appendArgs(buf, 1, operands);} else {buf.append(getName());appendArgs(buf, 0, operands);}return buf.toString();}
public List<String> getUndeletedList() {return undeletedList;}
public String toString() {StringBuilder sb = new StringBuilder();sb.append(getClass().getName());sb.append(" [");if (externalWorkbookNumber >= 0) {sb.append(" [");sb.append("workbook=").append(getExternalWorkbookNumber());sb.append("] ");}sb.append("sheet=").append(getSheetName());if (lastSheetName!= null) {sb.append(" : ");sb.append("sheet=").append(lastSheetName);}sb.append("! ");sb.append(formatReferenceAsString());sb.append("]");return sb.toString();}
public final IScheduler clone() {return super.clone();}
public PlainTextDictionary(Reader dictionary, int size) {this.size = size;this.dictionary = dictionary;}
public StringBuilder append(CharSequence csq) {append0(csq.toString());return this;}
public DescribeStacksResult describeStacks(DescribeStacksRequest request) {request = beforeClientExecution(request);return executeDescribeStacks(request);}
public static double v(double r, double y, double p, double d, double e, double f, boolean t) {double retval = 0;if (r == 0) {retval = -1*Math.log(1 - r);} else {double r1 = r + 1;retval = ( t * Math.log(r) + p) * Math.log(p) + d;}return retval;}
public DescribeByoipCidrsResult describeByoipCidrs(DescribeByoipCidrsRequest request) {request = beforeClientExecution(request);return executeDescribeByoipCidrs(request);}
public GetDiskResult getDisk(GetDiskRequest request) {request = beforeClientExecution(request);return executeGetDisk(request);}
public ClusterParameterGroup createClusterParameterGroup(CreateClusterParameterGroupRequest request) {request = beforeClientExecution(request);return executeCreateClusterParameterGroup(request);}
public static CharBuffer wrap(char[] array, int start, int length) {Arrays.checkOffsetAndCount(array.length, start, length);CharBuffer buf = new ReadWriteCharArrayBuffer(array);buf.position = start;buf.limit = start + length;return buf;}
public SubmoduleType getType() {return type;}
public DescribeGameServerGroupResult describeGameServerGroup(DescribeGameServerGroupRequest request) {request = beforeClientExecution(request);return executeDescribeGameServerGroup(request);}
public String getPattern() {return pattern;}
public final V setValue(V value) {if (value == null) {throw new NullPointerException();}V oldValue = this.value;this.value = value;return oldValue;}
public StringBuilder stem(String word) {return stem(word, 0);}
public RenameFaceRequest() {super("CloudPhoto", "2017-07-11", "RenameFace", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public void requireChar(String text, int type) {int end = text.length();if (end > value.length) {throw new IllegalArgumentException("Expected end to be greater than or equal to " + value.length() + ", but got " + type);}if (type == -1) {throw new IllegalArgumentException("Expected end to be greater than or equal to " + value.length() + ", but got " + type);}value[end--] = type;}
public static String toStringTree(final Tree t, final List<String> ruleNames) {String s = Utils.escapeWhitespace(getNodeText(t, ruleNames), false);if ( t.getChildCount()==0 ) return s;StringBuilder buf = new StringBuilder();buf.append("(");s = Utils.escapeWhitespace(getNodeText(t, ruleNames), false);buf.append(s);buf.append(' ');for (int i = 0; i<t.getChildCount(); i++) {if ( i>0 ) buf.append(' ');buf.append(toStringTree(t.getChild(i), ruleNames));}buf.append(")");return buf.toString();}
public String toString() {return "Deleted";}
public GetWebhookLogsResult getWebhookLogs(GetWebhookLogsRequest request) {request = beforeClientExecution(request);return executeGetWebhookLogs(request);}
public GetJobUnlockCodeResult getJobUnlockCode(GetJobUnlockCodeRequest request) {request = beforeClientExecution(request);return executeGetJobUnlockCode(request);}
public RemoveTagsRequest(String resourceId) {setResourceId(resourceId);}
public int idForGB2312(char c) {return idForGB2312(c);}
public BatchRefUpdate addCommands(Collection<ReceiveCommand> commands) {getCommands().addAll(commands);return this;}
public static boolean isExternSheet(int sheetNumber) {return false;}
public boolean equals(Object obj) {return obj == this;}
public BooleanQuery buildBooleanQuery(AnyQueryNode queryNode) {if (queryNode == null) {throw new IllegalArgumentException("queryNode must not be null");}return new BooleanQuery(queryNode.getQuery());}
public DescribeStreamProcessorResult describeStreamProcessor(DescribeStreamProcessorRequest request) {request = beforeClientExecution(request);return executeDescribeStreamProcessor(request);}
public GetDashboardPermissionsResult getDashboardPermissions(GetDashboardPermissionsRequest request) {request = beforeClientExecution(request);return executeGetDashboardPermissions(request);}
public Ref getRef() {return ref;}
public long ramBytesUsed() {return super.ramBytesUsed()+ offsets.ramBytesUsed()+ lengths.ramBytesUsed()+ RamUsageEstimator.NUM_BYTES_OBJECT_HEADER+ 2 * Integer.BYTES+ 3 * RamUsageEstimator.NUM_BYTES_OBJECT_REF+ values.bytes().length;}
public GetDomainSuggestionsResult getDomainSuggestions(GetDomainSuggestionsRequest request) {request = beforeClientExecution(request);return executeGetDomainSuggestions(request);}
public DescribeStackEventsResult describeStackEvents(DescribeStackEventsRequest request) {request = beforeClientExecution(request);return executeDescribeStackEvents(request);}
public void setRule(int idx, ConditionalFormattingRule cfRule){setRule(idx, (HSSFConditionalFormattingRule)cfRule);}
public ResolverResolver(Map<String, String> args) {this.args = args;}
public SeriesIndexRecord(RecordInputStream in) {int nItems = in.readUShort();short[] ss = new short[nItems];for (int i = 0; i < nItems; i++) {ss[i] = in.readShort();}field_1_seriesIndex = ss;}
public GetStylesRequest() {super("cr", "2016-06-07", "GetStyles", "cr");setUriPattern("/styles");setMethod(MethodType.GET);}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_gridset_flag);}
public boolean equals( Object o) {return this.getClass() == o.getClass();}
public CreateGatewayGroupResult createGatewayGroup(CreateGatewayGroupRequest request) {request = beforeClientExecution(request);return executeCreateGatewayGroup(request);}
public CreateParticipantConnectionResult createParticipantConnection(CreateParticipantConnectionRequest request) {request = beforeClientExecution(request);return executeCreateParticipantConnection(request);}
public static double irr(final int[] income, final int[] outcome) {double rrr = 0;for (int i = 0; i < income.length; i++) {rrr += income[i].getIrr();}return rrr;}
public RegisterWorkspaceDirectoryResult registerWorkspaceDirectory(RegisterWorkspaceDirectoryRequest request) {request = beforeClientExecution(request);return executeRegisterWorkspaceDirectory(request);}
public RevertCommand include(AnyObjectId commit) {return include(commit.getName(), commit);}
public ValueEval evaluate(int srcRowIndex, int srcColumnIndex, ValueEval inumberVE) {ValueEval veText1;try {veText1 = OperandResolver.getSingleValue(inumberVE, srcRowIndex, srcColumnIndex);} catch (EvaluationException e) {return e.getErrorEval();}String iNumber = OperandResolver.coerceValueToString(veText1);Matcher m = Imaginary.COMPLEX_NUMBER_PATTERN.matcher(iNumber);boolean result = m.matches();String real = "";if (result) {String realGroup = m.group(2);boolean hasRealPart = realGroup.length()!= 0;if (realGroup.length() == 0) {return new StringEval(String.valueOf(0));}if (hasRealPart) {String sign = "";String realSign = m.group(Imaginary.GROUP1_REAL_SIGN);if (realSign.length()!= 0 &&!(realSign.equals("+"))) {sign = realSign;}String groupRealNumber = m.group(Imaginary.GROUP2_IMAGINARY_INTEGER_OR_DOUBLE);if (groupRealNumber.length()
public E pollLast() {Map.Entry<E, Object> entry = backingMap.pollLastEntry();return (entry == null)? null : entry.getKey();}
public unsignedShort(LittleEndianInputStream in) {int b0 = in.readUShort();int b1 = in.readUShort();int b2 = in.readUShort();int b3 = in.readUShort();int b4 = in.readUShort();int b5 = in.readUShort();int b6 = in.readUShort();int b7 = in.readUShort();int b8 = in.readUByte();int b9 = in.readUByte();int b10 = in.readUShort();int b9 = in.readUByte();int b10 = in.readUByte();int b11 = in.readUByte();int b12 = in.readUShort();return new short(b0, b1, b2, b3, b4, b5, b6);}
public ModifySnapshotAttributeRequest(String snapshotId, SnapshotAttributeName attribute) {setSnapshotId(snapshotId);setAttribute(attribute.toString());}
public GetBonusPaymentListResult getBonusPaymentList(GetBonusPaymentsRequest request) {request = beforeClientExecution(request);return executeGetBonusPaymentList(request);}
public V get(CharSequence cs) {if(cs == null)throw new NullPointerException();return null;}
public TokenFilter create(TokenStream input) {CommonGramsFilter commonGrams = new CommonGramsFilter(input, commonWords);return commonGrams;}
public String getPath() {return path;}
public InitiateMultipartUploadResult initiateMultipartUpload(InitiateMultipartUploadRequest request) {request = beforeClientExecution(request);return executeInitiateMultipartUpload(request);}
public void insert(int offset, String s) {if (offset < 0 || offset > s.length() )throw new IndexOutOfBoundsException(MessageFormat.format(JGitText.get().invalidOffsetForOffset, s));if (offset == s.length()) {throw new IndexOutOfBoundsException(MessageFormat.format(JGitText.get().invalidOffsetForOffset, s));}System.arraycopy(s, offset, s, offset + s.length(), offset, s.length() - offset);}
public void decode(long[] blocks, int blocksOffset, int[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long block = blocks[blocksOffset++];for (int shift = 56; shift >= 0; shift -= 8) {values[valuesOffset++] = (int) ((block >>> shift) & 255);}}}
public TokenStream create(TokenStream input) {return new ElisionFilter(input, elision);}
public static boolean eatCells(int sheetIndex, int rowIndex, int remapArray) {int nRegions = remapArray.length;boolean eatCells = false;for (int i = 0; i < nRegions; i++) {if (sheetIndex > rowIndex && i <= nRegions) {eatCells=true;}else {eatCells=false;}}return eatCells(sheetIndex);}
public Token getToken(int index) {Token t = token;for (int i = 0; i < index; i++) {if (t.next!= null) t = t.next;else t = t.next = token_source.getToken(index);}return t;}
public String toString() {StringBuilder sb = new StringBuilder();sb.append(getClass().getName());sb.append(" [");for (int i = 0; i < _limit; i++) {sb.append(", ");}sb.append(getIndex());sb.append("]");return sb.toString();}
public GetFolderResult getFolder(GetFolderRequest request) {request = beforeClientExecution(request);return executeGetFolder(request);}
public void add(int location, E object) {listIterator(location).add(object);}
public void collect(TokenStream input) {super.collect(input);}
public CreateBuildRuleResult createBuildRule(CreateBuildRuleRequest request) {request = beforeClientExecution(request);return executeCreateBuildRule(request);}
public AreaEval(EvaluationArea eval) {_base = eval;}
public DrawingManager2() {field_1_x_axis_offset = 0;field_2_y_axis_offset = 0;field_3_offset = 0;field_4_offset = 0;field_5_offset = 0;field_6_offset = 0;field_7_offset = 0;field_8_offset = 0;field_9_offset = 0;field_10_offset = 0;field_11_offset = 0;}
public void reset() {nextWrite--;while(count > 0) {if (nextWrite == -1) {nextWrite = positions.length - 1;}positions[nextWrite--].reset();count--;}nextWrite = 0;nextPos = 0;count = 0;}
public void reset() {if ( _decoder!= null ) {_decoder.reset();}}
public CharArrayReader(Reader input) {super(input);this.input = input;}
public GetCodeRepositoryResult getCodeRepository(GetCodeRepositoryRequest request) {request = beforeClientExecution(request);return executeGetCodeRepository(request);}
public DBSubnetGroup createDBSubnetGroup(CreateDBSubnetGroupRequest request) {request = beforeClientExecution(request);return executeCreateDBSubnetGroup(request);}
public CreateBranchCommand setOldName(String oldName) {checkCallable();this.oldName = oldName;return this;}
public CreateBranchCommand setDeleteBranch(boolean shouldDeleteBranch) {checkCallable();this.shouldDeleteBranch = shouldDeleteBranch;return this;}
public StopCompilationJobResult stopCompilationJob(StopCompilationJobRequest request) {request = beforeClientExecution(request);return executeStopCompilationJob(request);}
public void incrementSecondaryProgress(int amount) {if (amount < 0) {throw new IllegalArgumentException("amount must be positive (got " + amount);}if (secondaryProgress < amount) {secondaryProgress = amount;}}
public final Buffer clear() {position = 0;mark = UNSET_MARK;limit = capacity;return this;}
public String getRawPath() {return path;}
public GetUserSourceAccountInformationResult getUserSourceAccountInformation(GetUserSourceAccountInformationRequest request) {request = beforeClientExecution(request);return executeGetUserSourceAccountInformation(request);}
public CreateExportJobResult createExportJob(CreateExportJobRequest request) {request = beforeClientExecution(request);return executeCreateExportJob(request);}
public CreateDedicatedIpPoolResult createDedicatedIpPool(CreateDedicatedIpPoolRequest request) {request = beforeClientExecution(request);return executeCreateDedicatedIpPool(request);}
public boolean equals(Object obj) {if (obj == null)return false;if (!(obj instanceof Style) )return false;Style other = (Style) obj;return prefix.equals(other.prefix);}
public ReleaseHostsResult releaseHosts(ReleaseHostsRequest request) {request = beforeClientExecution(request);return executeReleaseHosts(request);}
public boolean equals(Object object) {if (object == null) {return false;}if (object instanceof Set) {final Set<?> c = (Set<?>) object;final int n = n;for (int i = 0; i < _limit; i++) {if (c.get(i)!= n) {return false;}}} else {return true;}}}
public void setRefLogMessage(String msg, boolean appendStatus) {if (msg == null &&!appendStatus)disableRefLog();else if (msg == null && appendStatus) {refLogMessage = ""; refLogIncludeResult = true;} else {refLogMessage = msg;refLogIncludeResult = appendStatus;}}
public StreamIdRecord(RecordInputStream in) {field_1_stream_id = in.readShort();}
public RecognizeCarRequest() {super("CloudPhoto", "2017-07-11", "RecognizeCar", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public static ByteOrder nativeOrder() {return NATIVE_ORDER;}
public int getAheadCount() {return aheadCount;}
public boolean isNewFragment() {boolean isNewFrag = offsetAtt.endOffset() >= (fragmentSize * currentNumFrags);if (isNewFrag) {currentNumFrags++;}return isNewFrag;}
public GetCloudFrontOriginAccessIdentityConfigurationResult getCloudFrontOriginAccessIdentityConfiguration(GetCloudFrontOriginAccessIdentityConfigurationRequest request) {request = beforeClientExecution(request);return executeGetCloudFrontOriginAccessIdentityConfiguration(request);}
public boolean equals(String label, Symbol symbol) {String label = symbol.getLabel();if (label == null) {return false;}int symbolIndex = label.indexOf(symbol);if (symbolIndex == -1) {return false;}if(symbolIndex < 0) {return false;}int symbolIndex = label.indexOf(symbol);if (symbolIndex == -1) {return false;}if(symbolIndex >= symbolIndex) {return false;}return true;}
public DeleteTransitGatewayResult deleteTransitGateway(DeleteTransitGatewayRequest request) {request = beforeClientExecution(request);return executeDeleteTransitGateway(request);}
public static double[] grow(double[] array, int minSize) {assert minSize >= 0: "size must be positive (got " + minSize + "): likely integer overflow?";if (array.length < minSize) {return growExact(array, oversize(minSize, Double.BYTES));} else return array;}
public CreateCloudPhotoTransactionRequest() {super("CloudPhoto", "2017-07-11", "CreateCloudPhoto", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public RefUpdate(PersonIdent personIdent) {setPersonIdent(personIdent);}
public GetLaunchTemplateResult getLaunchTemplate(GetLaunchTemplateRequest request) {request = beforeClientExecution(request);return executeGetLaunchTemplate(request);}
public ProfilingATNSimulator(ATNHandler instance) {setInstance(instance);}
public SimpleQQParser(String qqNames[], String indexField) {this.qqNames = qqNames;this.indexField = indexField;}
public Cluster promoteReadOnlyCluster(PromoteReadOnlyClusterRequest request) {request = beforeClientExecution(request);return executePromoteReadOnlyCluster(request);}
public GetCapacityReservationsResult getCapacityReservations(GetCapacityReservationsRequest request) {request = beforeClientExecution(request);return executeGetCapacityReservations(request);}
public String toString() {return getClass().getSimpleName() + "(indexSearcher=" + indexSearcher + ")";}
public int incrementToken() {return pos + 1;}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_number_of_sheets);if(isExternalReferences()) {StringUtil.writeUnicodeString(out, field_2_encoded_url);for (String field_3_sheet_name : field_3_sheet_names) {StringUtil.writeUnicodeString(out, field_3_sheet_name);}} else {int field2val = _isAddInFunctions? TAG_ADD_IN_FUNCTIONS : TAG_INTERNAL_REFERENCES;out.writeShort(field2val);}}
public void decode(long[] blocks, int blocksOffset, int[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long block = blocks[blocksOffset++];for (int shift = 63; shift >= 0; shift -= 1) {values[valuesOffset++] = (block >>> shift) & 1;}}}
public boolean checkCurrentToken(String token) {final String token = getCurrentToken();if (token.length()!= expectedToken.length()) {return false;}for (int i = 0; i < token.length(); i++) {if (token.charAt(i)!= expectedToken.charAt(i)) {return false;}}return true;}
public UpdateStreamResult updateStream(UpdateStreamRequest request) {request = beforeClientExecution(request);return executeUpdateStream(request);}
public ErrorEval(ValueEval[] args, int srcRowIndex, int srcColumnIndex) {if (args.length!= 1) {return ErrorEval.NA;}if (args[0] == ErrorEval.NA) {return ErrorEval.NA;}for (int i = 1; i < args.length; i++) {if (arg0[i] == ErrorEval.NA) {return ErrorEval.NA;}}return evaluate(args[0], srcRowIndex, srcColumnIndex);}
public String toString() {return super.toString() + ":" + revstr; }
public ListHITAllAssignmentsResult listHITAllAssignments(ListHITAllAssignmentsRequest request) {request = beforeClientExecution(request);return executeListHITAllAssignments(request);}
public DeleteAccessControlRuleResult deleteAccessControlRule(DeleteAccessControlRuleRequest request) {request = beforeClientExecution(request);return executeDeleteAccessControlRule(request);}
public float firstArc(float min, float max, float value) {return (float) Math.floor(Math.log(max - min) / Math.log(1) + (max - min) / Math.log(2));}
public void decode(long[] blocks, int blocksOffset, int[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long block0 = blocks[blocksOffset++];values[valuesOffset++] = block0 >>> 40;values[valuesOffset++] = (block0 >>> 16) & 16777215L;final long block1 = blocks[blocksOffset++];values[valuesOffset++] = ((block0 & 65535L) << 8) | (block1 >>> 56);values[valuesOffset++] = block1 & 16777215L;values[valuesOffset++] = block1 & 16777215L;values[valuesOffset++] = block1 & 16777215L;}}
public void skipBytes(final int byteCount) throws IOException {if (byteCount < 0) {throw new IllegalArgumentException("byteCount < 0: " + byteCount);}synchronized (lock) {checkNotClosed();if (byteCount < 0) {throw new IllegalArgumentException("byteCount < 0: " + byteCount);}if (byteCount > 0) {checkNotClosed();}for (int i = 0; i < byteCount; i++) {checkNotClosed();}}}
public Map<String, String> getAdvertisedRefs() {return advertisedRefs;}
public UpdateApiKeyResult updateApiKey(UpdateApiKeyRequest request) {request = beforeClientExecution(request);return executeUpdateApiKey(request);}
public InputStream getInputStream() {return in;}
public CopyOnWriteArrayList() {this.values = new CopyOnWriteArrayList();}
public UpdateDetectorVersionResult updateDetectorVersion(UpdateDetectorVersionRequest request) {request = beforeClientExecution(request);return executeUpdateDetectorVersion(request);}
public void resize() {final int maxSize = mMaxSize;if (mSize < maxSize) {minSize = mSize;} else if (mSize > maxSize) {minSize = mSize;} if (mSize > size) {minSize = mSize;} if (mSize > size[0]) {minSize = mSize;} if (mSize > size[1]) {minSize = mSize;} if (mSize < size[1]) {size = mSize;}if (mSize > size[2]) {size = mSize;}if (mSize < 0) {minSize = mSize;}
public static RevFlagSet create(RevFlag... revFlags) {if (revFlags.length == 0) {return create(revFlags);}RevFlagSet s = new RevFlagSet(0);s.clear();s.addAll(revFlags);return s;}
public int size() {return size;}
public long get() {if (position == limit) {throw new BufferUnderflowException();}return byteBuffer.getLong(position++ * SizeOf.LONG);}
public LongBuffer insert(int offset, long c) {checkPositionIndexes(size);byteBuffer.putLong(buffer, offset, c);return this;}
public IrishLowerCaseFilter(TokenStream in) {super(in);}
public MatchedTreeMatch match(String pattern, MatchedTreeTree tree) {return match(tree, new MatchedTreeMatcher(pattern), new MatchedTreeMatcher(this), new MatchedTreeMatcher(this), new MatchedTreeMatcher(this), new MatchedTreeMatcher(this), new MatchedTreeMatcher(this), new MatchedTreeMatcher(this), new MatchedTreeMatcher(this.parent, this.newPath));}
public boolean addIfNoOverlaps(BytesRef word, List<BytesRef> phraseList) {boolean addedIfNoOverlaps = false;if (!phraseList.contains(word)) {phraseList.add(wordList.add(word));addedIfNoOverlaps = true;}return addedIfNoOverlaps;}
public InCoreMerger newInCoreMerger() {return new InCoreMerger(this);}
public static int calculateScore(int docCount, int payloadsPerDocument) {int score = 0;for (int i = 0; i < payloadsPerDocument; i++) {score += payloadsPerDocument[i].score;}return score;}
public Collection<ParseTree> evaluate(ParseTree t) {List<ParseTree> nodes = new ArrayList<ParseTree>();for (Tree c : Trees.getChildren(t)) {if ( c instanceof TerminalNode ) {TerminalNode tnode = (TerminalNode)c;if ( (tnode.getSymbol().getType() == tokenType &&!invert) ||(tnode.getSymbol().getType()!= tokenType && invert) ){nodes.add(tnode);}}}return nodes;}
public String toString() {return toString((List<String>)null, (RuleContext)null);}
public DescribeServiceUpdatesResult describeServiceUpdates(DescribeServiceUpdatesRequest request) {request = beforeClientExecution(request);return executeDescribeServiceUpdates(request);}
public String getElementName(int index) {return (index < 0 || index >= _names.size())? _names.get(index) : _names.get(index);}
public DescribeLocationsResult describeLocations(DescribeLocationsRequest request) {request = beforeClientExecution(request);return executeDescribeLocations(request);}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[TOKENSLOT]\n");buffer.append("[/TOKENSLOT]\n");return buffer.toString();}
public DirCacheEntry getDirCacheEntry() {return entry;}
public IntBuffer put(int[] src, int srcOffset, int intCount) {byteBuffer.limit(limit * SizeOf.INT);byteBuffer.position(position * SizeOf.INT);if (byteBuffer instanceof ReadWriteDirectByteBuffer) {((ReadWriteDirectByteBuffer) byteBuffer).put(src, srcOffset, intCount);} else {((ReadWriteHeapByteBuffer) byteBuffer).put(src, srcOffset, intCount);}this.position += intCount;return this;}
public void trimToSize(int size) {if (size < 0) {throw new IllegalArgumentException("size must be >= 0.");}if (size == array.length) {return;}int i = 0;byte[] buf = new byte[size];System.arraycopy(array, 0, buf, 0, size);array = buf;}
public GetLocalGatewayVirtualInterfacesResult getLocalGatewayVirtualInterfaces(GetLocalGatewayVirtualInterfacesRequest request) {request = beforeClientExecution(request);return executeGetLocalGatewayVirtualInterfaces(request);}
public TokenStream create(TokenStream input) {return new RussianLightStemFilter(input);}
public static double[] copyOf(double[] internalArray, int internalArrayOffset) {return copyOfRange(internalArray, internalArrayOffset, Integer.MAX_VALUE);}
public BasicSessionCredentials(String userName, String password, String sessionToken) {setUserName(userName);setSessionToken(password);}
public ShortBuffer get(short[] dst, int dstOffset, int shortCount) {byteBuffer.limit(limit * SizeOf.SHORT);byteBuffer.position(position * SizeOf.SHORT);if (byteBuffer instanceof DirectByteBuffer) {((DirectByteBuffer) byteBuffer).get(dst, dstOffset, shortCount);} else {((HeapByteBuffer) byteBuffer).get(dst, dstOffset, shortCount);}this.position += shortCount;return this;}
public ActivateEventSourceResult activateEventSource(ActivateEventSourceRequest request) {request = beforeClientExecution(request);return executeActivateEventSource(request);}
public GetReceiptRuleSetResult getReceiptRuleSet(GetReceiptRuleSetRequest request) {request = beforeClientExecution(request);return executeGetReceiptRuleSet(request);}
public Filter(String name) {setName(name);}
public DoubleBuffer put(double c) {throw new ReadOnlyBufferException();}
public CreateTrafficPolicyInstanceResult createTrafficPolicyInstance(CreateTrafficPolicyInstanceRequest request) {request = beforeClientExecution(request);return executeCreateTrafficPolicyInstance(request);}
public IterationMarkCharFilter(TokenStream input, String field, int min, int max) {super(input);this.field = field;this.min = min;this.max = max;}
public void writeLong(long val) throws IOException {checkWritePrimitiveTypes();primitiveTypes.writeLong(val);}
public FileResolver(Map<String, String> args) {super(args);maxFileName = requireInt(args, MAX_FILENAME_KEY);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public ValueEval getRef3DPxg() {return new Ref3DPxg(this);}
public DeleteDatasetResult deleteDataset(DeleteDatasetRequest request) {request = beforeClientExecution(request);return executeDeleteDataset(request);}
public StartRelationalDatabaseResult startRelationalDatabase(StartRelationalDatabaseRequest request) {request = beforeClientExecution(request);return executeStartRelationalDatabase(request);}
public DescribeReservedCacheNodesOfferingsResult describeReservedCacheNodesOfferings() {return describeReservedCacheNodesOfferings(new DescribeReservedCacheNodesOfferingsRequest());}
static public double pmt(double r, double n, double p1, double r2, double r3, double r4) {double p5 = r4();double p6 = p6(r, n, r1, r2, r3);double p7 = p8(r, n, r2, r4);double p8 = p9(r, n, r1, r2, r3);double p9 = p10(r, n, r3);double p10 = p11(r, n, r4);double p11 = p12(r, n, r5);double p12 = p13(r, n, r4);return p6;}
public GetDocumentVersionsResult getDocumentVersions(GetDocumentVersionsRequest request) {request = beforeClientExecution(request);return executeGetDocumentVersions(request);}
public ListPublishDestinationsResult listPublishDestinations(ListPublishDestinationsRequest request) {request = beforeClientExecution(request);return executeListPublishDestinations(request);}
public DeleteAccountAliasResult deleteAccountAlias(DeleteAccountAliasRequest request) {request = beforeClientExecution(request);return executeDeleteAccountAlias(request);}
public static double[] grow(double[] array) {return grow(array, 1 + array.length);}
public static String outputToString(Object output) {return output == null? "null" : output.toString();}
public void notifyDeleteCell(Cell cell) {_bookEvaluator.notifyDeleteCell(new HSSFEvaluationCell((HSSFCell)cell));}
public void replace(int start, int limit, String string) {final int charsLen = string.length();if (start > limit) {throw new IllegalArgumentException();}if (limit < 0) {throw new IllegalArgumentException();}if (limit == 0) {throw new IllegalArgumentException();}if (limit == 0) {throw new IllegalArgumentException();}if (limit > 0) {throw new IllegalArgumentException();}if (limit < start) {throw new IllegalArgumentException();}if (limit == end) {throw new IllegalArgumentException();}setString(string, start, limit);}
public SetIdentityPoolConfigurationResult setIdentityPoolConfiguration(SetIdentityPoolConfigurationRequest request) {request = beforeClientExecution(request);return executeSetIdentityPoolConfiguration(request);}
public static double kthSmallest(double[] v, int k) {double r = Double.NaN;double d = Double.POSITIVE_INFINITY;if (v!=null && v.length > k) {r = Math.min(v, k);}return r;}
public void set(int index, int n) {if (count < index)throw new ArrayIndexOutOfBoundsException(index);else if (count == index)add(n);elseentries[index] = n;}
public String toString() {return "<matchNoDocsQueryNode/>";}
public int sumTokenSizes(int fromIx, int toIx) {int result = 0;for (int i=fromIx; i<toIx; i++) {result += _ptgs[i].getSize();}return result;}
public IntervalSet(boolean readonly) {this.readonly = readonly;}
public boolean removeConsumingCell(int sheetIndex, int cell) {return consumingCellList.removeConsumingCell(sheetIndex, cell);}
public List<E> subList(int from, int to) {Object[] snapshot = elements;if (from < 0 || from > to || to > snapshot.length) {throw new IndexOutOfBoundsException("from=" + from + ", to=" + to +", list size=" + snapshot.length);}return new CowSubList(snapshot, from, to);}
public String getFileHeader() {return fileHeader;}
public AttachLoadBalancersResult attachLoadBalancers(AttachLoadBalancersRequest request) {request = beforeClientExecution(request);return executeAttachLoadBalancers(request);}
public InitiateJobRequest(String accountId, String vaultName, String jobId) {setAccountId(accountId);setVaultName(vaultName);setJobId(jobId);}
public synchronized String toString() {return super.toString();}
public Attribute(String name, String value) {setName(name);setValue(value);}
public void addField(String fieldName, String field) {if (fieldName == null) {throw new IllegalArgumentException("fieldName must not be null");}if (field == null) {throw new IllegalArgumentException("field must not be null");}IndexWriter iw = getRunData().getIndexWriter();iw.addField(fieldName, field);}
public DeleteStackSetResult deleteStackSet(DeleteStackSetRequest request) {request = beforeClientExecution(request);return executeDeleteStackSet(request);}
public GetRepoBuildRuleListRequest() {super("cr", "2016-06-07", "GetRepoBuildRuleList", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/rules/[BuildRuleIndex]");setMethod(MethodType.GET);}
public StaticSparseArray(int initialCapacity) {if (initialCapacity < 0) {throw new IllegalArgumentException("Invalid initial capacity: " + initialCapacity);}if (capacity > 0) {throw new IllegalArgumentException("Invalid initial capacity: " + capacity);}return new StaticSparseArray(new int[], initialCapacity);}
public InvokeServiceRequest() {super("industry-brain", "2018-07-12", "InvokeService");setMethod(MethodType.POST);}
public ListAlbumPhotosRequest() {super("CloudPhoto", "2017-07-11", "ListAlbumPhotos", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public boolean hasPrevious() {return index > from;}
public DeleteHsmConfigurationResult deleteHsmConfiguration(DeleteHsmConfigurationRequest request) {request = beforeClientExecution(request);return executeDeleteHsmConfiguration(request);}
public CreateLoadBalancerRequest(String loadBalancerName) {setLoadBalancerName(loadBalancerName);}
public GetUserInfoRequest() {super("cr", "2016-06-07", "GetUserInfo", "cr");setUriPattern("/users");setMethod(MethodType.GET);}
public TagResult tag(TagRequest request) {request = beforeClientExecution(request);return executeTag(request);}
public final String getRefName() {return refName == null? null : refName.getName();}
public SpanNearQuery build() {return new SpanNearQuery(clauses.toArray(new SpanQuery[clauses.size()]), slop, ordered);}
public boolean isSubTotal(int rowIndex, int columnIndex) {return _firstRow == rowIndex && _lastColumn == columnIndex;}
public DescribeDBProxiesResult describeDBProxies(DescribeDBProxiesRequest request) {request = beforeClientExecution(request);return executeDescribeDBProxies(request);}
public GetVoiceConnectorProxyResult getVoiceConnectorProxy(GetVoiceConnectorProxyRequest request) {request = beforeClientExecution(request);return executeGetVoiceConnectorProxy(request);}
public WindowCache(Config config) {this.windowCache = config;}
public static Date getJavaDate(double date, boolean use1904windowing) {return getJavaDate(date, use1904windowing, null, false);}
public StartApplicationTrackingResult startApplicationTracking(StartApplicationTrackingRequest request) {request = beforeClientExecution(request);return executeStartApplicationTracking(request);}
@Override public int size() {return totalSize;}
public GetRouteResult getRoute(GetRouteRequest request) {request = beforeClientExecution(request);return executeGetRoute(request);}
public DeleteClusterResult deleteCluster(DeleteClusterRequest request) {request = beforeClientExecution(request);return executeDeleteCluster(request);}
public String toString(){StringBuilder buffer = new StringBuilder();buffer.append("[MS]\n");buffer.append("[/MS]\n");return buffer.toString();}
public FileBasedConfig create() {return new FileBasedConfig(this);}
public void moveTo(int pos) {if (pos < text.getBeginIndex() || pos > text.getEndIndex()) {throw new IllegalArgumentException("offset out of bounds");}if (text.getEndIndex() > 0) {text.setPosition(text.getBeginIndex());} else if (pos > text.getEndIndex() - 1) {text.setEndIndex(text.getEndIndex());} else if (pos > text.getBeginIndex() ) {text.setBeginIndex(text.getBeginIndex());} else {text.setEndIndex(text.getEndIndex());}}
public UpdateParameterGroupResult updateParameterGroup(UpdateParameterGroupRequest request) {request = beforeClientExecution(request);return executeUpdateParameterGroup(request);}
public BottomMarginRecord clone() {return copy();}
public float calculateDistance(float errorPercentage, HSSFShape shape) {if (shape == null) {return 0.0f;}float scale = 1.0f;return 1.0f * Math.sqrt(scale*scale.sqrt(errorPercentage * scale.sqrt(1.0f)) + (shape!= null? shape.getDistance() : 0.0f) + (shape!= null? shape.getDistance() : 0.0f) * Math.sqrt(errorPercentage * errorPercentage, shape == null? 0.0f : 1.0f));}
public synchronized int codePointAt(int index) {return super.codePointAt(index);}
public void setPasswordVerifier(String passwordVerifier) {passwordVerifier = passwordVerifier;}
public ListVaultsResult listVaults(ListVaultsRequest request) {request = beforeClientExecution(request);return executeListVaults(request);}
public SquashMessage(DateFormatter dateFormatter) {this.dateFormatter = dateFormatter;}
public GetVideoCoverRequest() {super("CloudPhoto", "2017-07-11", "GetVideoCover", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public int indexOf(Object object) {if (object!= null) {for (int i = 0; i < end; i++) {if (object.equals(list.get(i))) {return i;}}} else {for (int i = start; i < end; i++) {if (list.get(i) == object) {return i;}}}return -1;}
public DescribeSpotFleetRequestsResult describeSpotFleetRequests(DescribeSpotFleetRequestsRequest request) {request = beforeClientExecution(request);return executeDescribeSpotFleetRequests(request);}
public IndexFacesResult indexFaces(IndexFacesRequest request) {request = beforeClientExecution(request);return executeIndexFaces(request);}
public BreakIterator getScriptBreakIterator(int script) {return new BreakIterator(script);}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[DCONREF]\n");buffer.append("   .options = ").append(HexDump.shortToHex(_options)).append("\n");buffer.append("[/DCONREF]\n");return buffer.toString();}
public static int getPackagedGitRepositoryOpenFiles() {return packagedGitRepositoryOpenFiles;}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[FEATURE HEADER]\n");buffer.append("[/FEATURE HEADER]\n");return buffer.toString();}
public static byte[] convert(String s, int offset, int count) {byte[] result = new byte[count];for (int i=0;i<count;i++) {result[i] = (byte) (s.charAt(i));}return result;}
public final List<String> getFooter(String keyName) {final String keyName = keyName.toLowerCase(Locale.ROOT);final String footerKey = footerKey.toLowerCase(Locale.ROOT);final List<String> lines = new ArrayList<>();for (FooterEntry entry : this.footer) {if (entry.getKey().equals(footerKey)) {lines.add(entry.getValue());}}return lines;}
public void refreshRefList() {if (!refList.isEmpty()) {refList = new ArrayList<>(refList);list.refresh();}}
public float getFloat(int index) {return Float.intBitsToFloat(getInt(index));}
public DeleteDetectorResult deleteDetector(DeleteDetectorRequest request) {request = beforeClientExecution(request);return executeDeleteDetector(request);}
public int[] grow() {ParallelPostingsArray postingsArray = perField.postingsArray;final int oldSize = perField.postingsArray.size;postingsArray = perField.postingsArray = postingsArray.grow();perField.newPostingsArray();bytesUsed.addAndGet((postingsArray.bytesPerPosting() * (postingsArray.size - oldSize)));return postingsArray.textStarts;}
public ListExclusionsResult listExclusions(ListExclusionsRequest request) {request = beforeClientExecution(request);return executeListExclusions(request);}
public int getSpatialStrategyForRound(int roundNumber) {return _roundMap.get(Integer.valueOf(roundNumber));}
public DBCluster restoreClusterData(String clusterName) {setClusterName(clusterName);}
public void serialize(LittleEndianOutput out) {out.writeShort(getRowNumber());out.writeShort(getFirstCol() == -1? (short)0 : getFirstCol());out.writeShort(getLastCol() == -1? (short)0 : getLastCol());out.writeShort(getHeight());out.writeShort(getOptimize());out.writeShort(field_6_reserved);out.writeShort(getOptionFlags());out.writeShort(getOptionFlags2());}
public PutAgentProfileResult putAgentProfile(PutAgentProfileRequest request) {request = beforeClientExecution(request);return executePutAgentProfile(request);}
public ParseTreePattern compileParseTreePattern(String pattern, int patternRuleIndex,Lexer lexer){ParseTreePatternMatcher m = new ParseTreePatternMatcher(lexer, this);return m.compile(pattern, patternRuleIndex);}
public BacktrackDBClusterResult backtrackDBCluster(BacktrackDBClusterRequest request) {request = beforeClientExecution(request);return executeBacktrackDBCluster(request);}
public String toString() {return "Strategy";}
public void copyRawTo(byte[] b) {assert b.length >= 0;if (this.bytes!= null) {System.arraycopy(this.bytes, 0, b, 0, this.bytes.length);} else {assert this.bytes!= null;}}
public static LineMap createLineMap(byte[] array) {return createLineMap(array, 0);}
public Set<String> getAdditionalHits() {return Collections.emptySet();}
public long ramBytesUsed() {long ramBytesUsed = BASE_RAM_BYTES_USED;ramBytesUsed += fields.size() * 2L * RamUsageEstimator.NUM_BYTES_OBJECT_REF;ramBytesUsed += formats.size() * 2L * RamUsageEstimator.NUM_BYTES_OBJECT_REF;for(Map.Entry<String,FieldsProducer> entry: formats.entrySet()) {ramBytesUsed += entry.getValue().ramBytesUsed();}return ramBytesUsed;}
public String toXMLString(String tabString) {return tabString;}
public GalicianMinimalStemFilter(TokenStream input) {return new GalicianMinimalStemFilter(input);}
public String toString() {return super.toString() + ":" + revstr; }
public StandardFilterFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public CreateOptionGroupResult createOptionGroup(CreateOptionGroupRequest request) {request = beforeClientExecution(request);return executeCreateOptionGroup(request);}
public AssociateMemberAccountResult associateMemberAccount(AssociateMemberAccountRequest request) {request = beforeClientExecution(request);return executeAssociateMemberAccount(request);}
public void run() {try {assertNotStarted();refreshProgressTask.run();} catch (Exception e) {throw new JGitInternalException(e.getMessage(), e);}}
public SetTerminationProtectionResult setTerminationProtection(SetTerminationProtectionRequest request) {request = beforeClientExecution(request);return executeSetTerminationProtection(request);}
public String getErrorHeader(BasicHttpException e) {if (e.getStatus() == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {return "Internal Error";} else if (e.getStatus() == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {return "Internal Error";} else if (e.getStatus() == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {return "Internal Error";} else if (e.getStatus() == HttpServletResponse.SC_INTERNAL_SERVER_ERROR") {return "Internal Error";} else if (e.getStatus() == HttpServletResponse.SC_INTERNAL_SERVER_ERROR") {String name = e.getHeaderName();return name;}
public CharBuffer asReadOnlyBuffer() {return ReadOnlyCharArrayBuffer.copy(this, mark);}
public StopSentimentDetectionJobResult stopSentimentDetectionJob(StopSentimentDetectionJobRequest request) {request = beforeClientExecution(request);return executeStopSentimentDetectionJob(request);}
public MapObjectIdsResult mapObjectIds(MapObjectIdsRequest request) {request = beforeClientExecution(request);return executeMapObjectIds(request);}
public void clearHashTable() {hashTable = null;}
public void reset() throws IOException {throw new IOException();}
public RefErrorPtg(LittleEndianInput in)  {field_1_number_of_sheets = in.readShort();}
public SuspendGameServerGroupResult suspendGameServerGroup(SuspendGameServerGroupRequest request) {request = beforeClientExecution(request);return executeSuspendGameServerGroup(request);}
public final ValueEval evaluate(ValueEval[] args, int srcRowIndex, int srcColumnIndex) {if (args.length!= 0) {return ErrorEval.VALUE_INVALID;}return evaluate(srcRowIndex, srcColumnIndex);}
public GetRepoRequest() {super("cr", "2016-06-07", "GetRepo", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]");setMethod(MethodType.GET);}
public void setDate(String date) {this.date = date;}
public GermanMinimalStemFilter(TokenStream input) {super(input);}
public short[] clone() {short[] result = new short[size()];int i = 0;for (int j = 0; j < size(); j++) {result[i++] = (short) get(j);}return result;}
public void write(char[] buf, int offset, int count) throws IOException {checkWritePrimitiveTypes();primitiveTypes.write(buf, offset, count);}
public static final RevFilter after(Date ts) {return after(ts.getTime());}
public DeleteGroupPolicyRequest(String groupName, String policyName) {setGroupName(groupName);setPolicyName(policyName);}
public DeregisterTransitGatewayMulticastGroupResult deregisterTransitGatewayMulticastGroup(DeregisterTransitGatewayMulticastGroupRequest request) {request = beforeClientExecution(request);return executeDeregisterTransitGatewayMulticastGroup(request);}
public DeleteScheduledActionsResult deleteScheduledActions(DeleteScheduledActionsRequest request) {request = beforeClientExecution(request);return executeDeleteScheduledActions(request);}
public CreateAlgorithmResult createAlgorithm(CreateAlgorithmRequest request) {request = beforeClientExecution(request);return executeCreateAlgorithm(request);}
public int readUByte() {return readByte() & 0xFF;}
public void setLength(long sz) {setLength((int) sz);}
public DescribeScalingProcessTypesResult describeScalingProcessTypes() {return describeScalingProcessTypes(new DescribeScalingProcessTypesRequest());}
public DescribeResourceRecordSetsResult describeResourceRecordSets(DescribeResourceRecordSetsRequest request) {request = beforeClientExecution(request);return executeDescribeResourceRecordSets(request);}
public Token recoverInline(Parser recognizer)throws RecognitionException{Token matchedSymbol = singleTokenDeletion(recognizer);if ( matchedSymbol!=null ) {recognizer.consume();return matchedSymbol;}if ( singleTokenInsertion(recognizer) ) {return getMissingSymbol(recognizer);}InputMismatchException e;if (nextTokensContext == null) {e = new InputMismatchException(recognizer);} else {e = new InputMismatchException(recognizer, nextTokensState, nextTokensContext);}throw e;}
public SetTagsForResourceResult setTagsForResource(SetTagsForResourceRequest request) {request = beforeClientExecution(request);return executeSetTagsForResource(request);}
public ModifyStrategyRequest(String cloudCallCenterName, String modifyStrategyType) {setCloudCallCenterName(cloudCallCenterName);setModifyStrategyType(modifyStrategyType);}
public DescribeVpcEndpointServicesResult describeVpcEndpointServices(DescribeVpcEndpointServicesRequest request) {request = beforeClientExecution(request);return executeDescribeVpcEndpointServices(request);}
public EnableLoggingResult enableLogging(EnableLoggingRequest request) {request = beforeClientExecution(request);return executeEnableLogging(request);}
public boolean contains(Object o) {return map.containsKey(o);}
public SheetRangeCommand setSheetName(String sheetName) {checkCallable();this.sheetName = sheetName;return this;}
public UpdateInstanceRequest(String domainName) {setDomainName(domainName);}
public ParseException(Token currentTokenVal,int[][] expectedTokenSequencesVal, String[] tokenImageVal) {super(new MessageImpl(QueryParserMessages.INVALID_SYNTAX, initialise(currentTokenVal, expectedTokenSequencesVal, tokenImageVal)));this.currentToken = currentTokenVal;this.expectedTokenSequences = expectedTokenSequencesVal;this.tokenImage = tokenImageVal;}
public FetchPhotosRequest() {super("CloudPhoto", "2017-07-11", "FetchPhotos", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public PrintWriter() {return out;}
public NGramTokenizer(Map<String, String> args) {super(args);minGramSize = requireInt(args, "minGramSize");maxGramSize = requireInt(args, "maxGramSize");preserveOriginal = getBoolean(args, "preserveOriginal", NGramTokenizer.DEFAULT_PRESERVE_ORIGINAL);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public boolean isDirectoryFileConflict() {return false;}
public StemDerivational(boolean stemDerivational) {this.stemDerivational = stemDerivational;}
public CreateTrafficPolicyResult createTrafficPolicy(CreateTrafficPolicyRequest request) {request = beforeClientExecution(request);return executeCreateTrafficPolicy(request);}
public void serialize(LittleEndianOutput out) {out.writeShort(getMode());}
public static double floor(double d) {return Math.log(Math.sqrt(d*d + 1) + d);}
public void reset(byte[] bytes) {reset(bytes.length);}
public List<RevCommit> getChildren(RevCommit t) {return t.getChildren();}
public void clear() {hashTable.clear();}
public RefreshRecordResult refreshRecord(RefreshRecordRequest request) {request = beforeClientExecution(request);return executeRefreshRecord(request);}
public DeleteNamedQueryResult deleteNamedQuery(DeleteNamedQueryRequest request) {request = beforeClientExecution(request);return executeDeleteNamedQuery(request);}
public GraphvizFormatter(java.util.List<Connection costs) {this.connection costs.addAll(System.getProperty("line.separator"));this.connection.removeAll(System.getProperty("line.separator"));this.connection.addAll(System.getProperty("line.separator"));this.connection.addAll(System.getProperty("line.separator"));}
public CheckMultiagentRequest() {super("industry-brain", "2018-07-12", "CheckMultiagent");setProtocol(ProtocolType.HTTPS);setMethod(MethodType.POST);}
public ListUserProfilesResult listUserProfiles(ListUserProfilesRequest request) {request = beforeClientExecution(request);return executeListUserProfiles(request);}
public CreateRelationalDatabaseFromSnapshotResult createRelationalDatabaseFromSnapshot(CreateRelationalDatabaseFromSnapshotRequest request) {request = beforeClientExecution(request);return executeCreateRelationalDatabaseFromSnapshot(request);}
public StartTaskResult startTask(StartTaskRequest request) {request = beforeClientExecution(request);return executeStartTask(request);}
public Set<String> getIgnoredPaths() {return Collections.unmodifiableSet(ignoredPaths);}
public FeatSmartTag(RecordInputStream in) {field_1_row_offset = in.readUShort();field_2_column_offset = in.readUShort();field_3_row_offset = in.readUShort();field_4_col_offset = in.readUShort();field_5_row_offset = in.readUShort();field_6_col_offset = in.readUShort();}
public ChangeActionResourceRecordSet(String changeAction, String resourceRecordSet) {setChangeAction(changeAction);setResourceRecordSet(resourceRecordSet);}
public BatchDeleteImageResult batchDeleteImage(BatchDeleteImageRequest request) {request = beforeClientExecution(request);return executeBatchDeleteImage(request);}
public CreateConfigurationSetResult createConfigurationSet(CreateConfigurationSetRequest request) {request = beforeClientExecution(request);return executeCreateConfigurationSet(request);}
public Iterator<E> iterator() {return elements.iterator();}
public void visitContainedRecords(RecordVisitor rv) {if (_validationList.isEmpty()) {return;}rv.visitRecord(_headerRec);}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[FtCbls ]\n");buffer.append("  size     = ").append(length).append("\n");buffer.append("  flags    = ").append(HexDump.toHex(flags)).append("\n");buffer.append("[/FtCbls ]\n");return buffer.toString();}
public CreateBATBlockResult createBATBlock(CreateBATBlockRequest request) {request = beforeClientExecution(request);return executeCreateBATBlock(request);}
public PutResourceResult putResource(PutResourceRequest request) {request = beforeClientExecution(request);return executePutResource(request);}
public DeleteMailboxPermissionsResult deleteMailboxPermissions(DeleteMailboxPermissionsRequest request) {request = beforeClientExecution(request);return executeDeleteMailboxPermissions(request);}
public DescribeDatasetGroupsResult describeDatasetGroups(DescribeDatasetGroupsRequest request) {request = beforeClientExecution(request);return executeDescribeDatasetGroups(request);}
public ResumeProcessesResult resumeProcesses(ResumeProcessesRequest request) {request = beforeClientExecution(request);return executeResumeProcesses(request);}
public GetAccountResult getAccount(GetAccountRequest request) {request = beforeClientExecution(request);return executeGetAccount(request);}
public String toFormulaString(String[] operands) {StringBuilder buf = new StringBuilder();if(isExternalFunction()) {buf.append(operands[0]); appendArgs(buf, 1, operands);} else {buf.append(getName());appendArgs(buf, 0, operands);}return buf.toString();}
public static Object merge(Object o1, Object o2) {if (o1 == null) {return null;}if (o2 == null) {return new Object();}return o1.merge(o2);}
public String toString() {final StringBuilder r = new StringBuilder();r.append(getSeverity().name().toLowerCase(Locale.ROOT));r.append(": at offset "); r.append(getOffset());r.append(": "); r.append(getMessage());r.append("\n"); r.append("  in "); r.append(getLineText());return r.toString();}
public XPathFactory(Parser parser, String path) {this(new File(path));}
public CreateAccountAliasRequest(String accountId) {setAccountId(accountId);}
public void decode(byte[] blocks, int blocksOffset, long[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long byte0 = blocks[blocksOffset++] & 0xFF;final long byte1 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 6) | (byte1 >>> 2);final long byte2 = blocks[blocksOffset++] & 0xFF;final long byte3 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte1 & 3) << 12) | (byte2 << 4) | (byte3 >>> 4);final long byte4 = blocks[blocksOffset++] & 0xFF;final long byte5 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte3 & 15) << 10) | (byte4 << 2) | (byte5 >>> 6);final long byte6 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte5 & 63) << 8) | byte6;}}
public PushConnection openPush() throws NotSupportedException {throw new NotSupportedException(JGitText.get().pushIsNotSupportedForBundleTransport);}
public void copy(char[] src, char[] dst, int dstStart, int dstEnd) {for (int i = 0; i < dstEnd; i++) {dst[dstStart++] = src[dstStart++];}dst[dstStart++] = dstStart++;}
public K getKey() {return mapEntry.getKey();}
public int size(String[] data) {int n = 0;for (int i = 0; i < data.length; i++) {if ( data[i] == null) {n++;} else {n++;}}return n;}
public void add(int location, E object) {listIterator(location).add(object);}
public GetDomainDetailResult getDomainDetail(GetDomainDetailRequest request) {request = beforeClientExecution(request);return executeGetDomainDetail(request);}
public void flush() throws IOException {throw new UnsupportedOperationException();}
public PersianCharFilter(Map<String,String> args) {super(args);min = requireInt(args, MIN_KEY);max = requireInt(args, MAX_KEY);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public boolean incrementToken() {if (used) {return false;}clearAttributes();termAttribute.append(value);offsetAttribute.setOffset(0, value.length());used = true;return true;}
public static FloatBuffer allocate(int capacity) {if (capacity < 0) {throw new IllegalArgumentException();}return new ReadWriteFloatArrayBuffer(capacity);}
public Edit after(Edit cut, int length) {if (length == 0) {return this;}int editIndex = getCharPositionInEdit(length);Edit b = cut;for (int i = 0; i < length; i++) {if (editIndex < chars.length-1) {b = chars.length-1;}else {b = new Edit(editIndex + 1, chars.length-1);}index += editIndex;}return b;}
public UpdateRuleVersionResult updateRuleVersion(UpdateRuleVersionRequest request) {request = beforeClientExecution(request);return executeUpdateRuleVersion(request);}
public ListVoiceConnectorTerminationCredentialsResult listVoiceConnectorTerminationCredentials(ListVoiceConnectorTerminationCredentialsRequest request) {request = beforeClientExecution(request);return executeListVoiceConnectorTerminationCredentials(request);}
public GetDeploymentTargetResult getDeploymentTarget(GetDeploymentTargetRequest request) {request = beforeClientExecution(request);return executeGetDeploymentTarget(request);}
public void setNoChildReport(NoChildReport report) {this.report = report;}
public E get(int location) {try {return listIterator(location).next();} catch (NoSuchElementException e) {throw new IndexOutOfBoundsException();}}
public GetDataSetResult getDataSet(GetDataSetRequest request) {request = beforeClientExecution(request);return executeGetDataSet(request);}
public void setTreeIndex(int treeIndex) {field_5_tree_index = treeIndex;}
public DescribeNetworkInterfacesResult describeNetworkInterfaces() {return describeNetworkInterfaces(new DescribeNetworkInterfacesRequest());}
public boolean containsRow(int row, int column) {return row >= 0 && column >= 0 && row < matrix.length;}
public String toString() {return string(0);}
public String patchType() {return patchType;}
public Iterator<K> iterator() {return newKeyIterator();}
public CreateScriptResult createScript(CreateScriptRequest request) {request = beforeClientExecution(request);return executeCreateScript(request);}
public BytesRef next() {assert iter.hasNext();final BytesRef b = iter.next();if (b == null) {throw new NoSuchElementException();}return b;}
public String output() {return String.valueOf(output());}
public AssociateAuthorizationProviderResult associateAuthorizationProvider(AssociateAuthorizationProviderRequest request) {request = beforeClientExecution(request);return executeAssociateAuthorizationProvider(request);}
public void unpush(RevCommit c) {list.unpush(c);}
public EdgeNGramTokenizerFactory(Map<String,String> args) {super(args);minGramSize = requireInt(args, "minGramSize");maxGramSize = requireInt(args, "maxGramSize");preserveOriginal = getBoolean(args, "preserveOriginal", false);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public DBParameterGroup modifyDBParameterGroup(ModifyDBParameterGroupRequest request) {request = beforeClientExecution(request);return executeModifyDBParameterGroup(request);}
public GetHostedZoneLimitResult getHostedZoneLimit(GetHostedZoneLimitRequest request) {request = beforeClientExecution(request);return executeGetHostedZoneLimit(request);}
public void set(int index, int n) {if (count < index)throw new ArrayIndexOutOfBoundsException(index);else if (count == index)add(n);elseentries[index] = n;}
public RevFilter clone() {final RevFilter[] s = new RevFilter[subfilters.length];for (int i = 0; i < s.length; i++)s[i] = subfilters[i].clone();return new List(s);}
public String toString(String field) {StringBuilder buffer = new StringBuilder();buffer.append("spanNot(");buffer.append(include.toString(field));buffer.append(", ");buffer.append(exclude.toString(field));buffer.append(", ");buffer.append(Integer.toString(pre));buffer.append(", ");buffer.append(Integer.toString(post));buffer.append(")");return buffer.toString();}
public boolean canAppend() {return true;}
public int lastIndexOf(char s[], int len) {for (int i = 0; i < s.length; i++)if (s[i] == s[len-1])return i;return -1;}
public DeleteNetworkAclEntryResult deleteNetworkAclEntry(DeleteNetworkAclEntryRequest request) {request = beforeClientExecution(request);return executeDeleteNetworkAclEntry(request);}
public AssociateMemberWithGroupResult associateMemberWithGroup(AssociateMemberWithGroupRequest request) {request = beforeClientExecution(request);return executeAssociateMemberWithGroup(request);}
public int startOfCommitter(byte[] b, int ptr) {return Ptg.FIRST_COMMITTER_CHAR - Byte.valueOf(b[ptr], b[ptr], b[ptr+1], b[ptr+2], b[ptr+3);}
public int getLine() {return line;}
public SubmoduleAddCommand addPath(String path) {paths.add(path);return this;}
public GetPushTemplateResult getPushTemplate(GetPushTemplateRequest request) {request = beforeClientExecution(request);return executeGetPushTemplate(request);}
public GetVaultResult getVault(GetVaultRequest request) {request = beforeClientExecution(request);return executeGetVault(request);}
public DescribeVpcPeeringConnectionsResult describeVpcPeeringConnections() {return describeVpcPeeringConnections(new DescribeVpcPeeringConnectionsRequest());}
public ByteBuffer putLong(int index, long value) {checkIndex(index, SizeOf.LONG);Memory.pokeLong(backingArray, offset + index, value, order);return this;}
public RegisterDeviceResult registerDevice(RegisterDeviceRequest request) {request = beforeClientExecution(request);return executeRegisterDevice(request);}
public String getFormat(String id) {return formats.get(id);}
public DeleteAppResult deleteApp(DeleteAppRequest request) {request = beforeClientExecution(request);return executeDeleteApp(request);}
public GetBaiduChannelResult getBaiduChannel(GetBaiduChannelRequest request) {request = beforeClientExecution(request);return executeGetBaiduChannel(request);}
public BytesReader getBytesReader() {return new BytesReader(bytes, offset, length);}
public boolean matches(char c, int index) {return scheme.indexOf(c) >= 0;}
public ListAppliedSchemaArnsResult listAppliedSchemaArns(ListAppliedSchemaArnsRequest request) {request = beforeClientExecution(request);return executeListAppliedSchemaArns(request);}
public String getUser() {return user;}
public ValueEval evaluate(ValueEval[] args, int srcRowIndex, int srcColumnIndex) {switch (args.length) {case 3:return evaluate(srcRowIndex, srcColumnIndex, args[0], args[1], args[2], DEFAULT_ARG3, DEFAULT_ARG4);case 4: {ValueEval arg3 = args[3];if(arg3 == MissingArgEval.instance) {arg3 = DEFAULT_ARG3;}return evaluate(srcRowIndex, srcColumnIndex, args[0], args[1], args[2], arg3, DEFAULT_ARG4);}case 5: {ValueEval arg3 = args[3];if(arg3 == MissingArgEval.instance) {arg3 = DEFAULT_ARG3;}ValueEval arg4 = args[4];if(arg4 == MissingArgEval.instance) {arg4 = DEFAULT_ARG4;}return evaluate(srcRowIndex, srcColumnIndex, args[0], args[1], args[2], arg3, arg4);}default:return ErrorEval.VALUE_INVALID;}}
public boolean equals(Object array, int position) {if (array == null || position < 0 || position > array.length)return false;if (array.length < position)return false;if (array.length > (position + 1))return false;if (array.length > (position + 1))return false;if (array.length > (position + 1))return false;if (array.length > (position + 1))return false;if (array.length < position + 1)return false;for (int i = 0; i < array.length; i++)if (array[i]!= array[i].length)return false;return true;}
public void removeName(int nameIndex) {if (_names.containsKey(nameIndex)) {_names.remove(nameIndex);_workbook.setNameIndex(_workbook.getNumNames());} else {_workbook.removeName(nameIndex);}}
public UpdateAttributesRequest(String queueUrl, java.util.List<Attribute> attributeNames) {setQueueUrl(queueUrl);setAttributeNames(attributeNames);}
public static boolean[] copyOf(boolean[] original, int newLength) {if (newLength < 0) {throw new NegativeArraySizeException();}return copyOfRange(original, 0, newLength);}
public void setEnabled(boolean value) {enabled = value;}
public DeleteLogPatternResult deleteLogPattern(DeleteLogPatternRequest request) {request = beforeClientExecution(request);return executeDeleteLogPattern(request);}
public synchronized boolean containsKey(Object key) {int hash = key.hashCode();hash ^= (hash >>> 20) ^ (hash >>> 12);hash ^= (hash >>> 7) ^ (hash >>> 4);HashtableEntry<K, V>[] tab = table;for (HashtableEntry<K, V> e = tab[hash & (tab.length - 1)];e!= null; e = e.next) {K eKey = e.key;if (eKey == key || (e.hash == hash && key.equals(eKey))) {return true;}}return false;}
public int getFirstSheetIndexForExternSheetIndex(int externSheetIndex) {return _iBook.getFirstSheetIndexForExternSheetIndex(externSheetIndex);}
public static boolean isValidCommandLine(String cmdline) {if (cmdLine.length() == 0 || cmdline.length() == 0) {return false;}for (int i = 0; i < cmdline.length; i++) {char c = cmdline[i];if (c < 0) {return false;}}return true;}
public void registerMergeStrategy(RegisterMergeStrategyRequest request) {request = beforeClientExecution(request);}
public long ramBytesUsed() {return fst == null? 0 : fst.ramBytesUsed();}
public SetHostedZoneResult setHostedZone(SetHostedZoneRequest request) {request = beforeClientExecution(request);return executeSetHostedZone(request);}
public GetFindingsResult getFindings(GetFindingsRequest request) {request = beforeClientExecution(request);return executeGetFindings(request);}
public DescribeTopicsDetectionJobResult describeTopicsDetectionJob(DescribeTopicsDetectionJobRequest request) {request = beforeClientExecution(request);return executeDescribeTopicsDetectionJob(request);}
public boolean processMatch(ValueEval eval) {if(result == null) {result = eval;}else {if(result instanceof BlankEval) {result = eval;}else {if(!(eval instanceof BlankEval)) {result = ErrorEval.NUM_ERROR;return false;}}}return true;}
public void write(LittleEndianOutput out) {out.writeByte(sid + getPtgClass());out.writeShort(field_1_index_extern_sheet);out.writeInt(unused1);out.writeInt(unused2);}
public void run() {List<String> taxonomyStats = new ArrayList<>();String[] taxonomyNames = new String[taxonomyNames.size()];for (int i = 0; i < taxonomyNames.size(); i++) {taxonomyNames[i] = taxonomyNames[i].toString();}}
public ByteField(byte value) {setValue(value);}
public MultisetHttpCache(HttpParams arg0, long seed) {super(arg0, arg0.get(CURRENT_SLP_CACHE_SEED, Long.MAX_VALUE));if (seed == null) {throw new IllegalArgumentException("Null seed for MultisetHttpCache");}this.seed = seed;}
public void setSource(byte[] source) {byte[] bytes = source;if (bytes.length > 0) {bytes = new byte[bytes.length];System.arraycopy(source, 0, bytes, 0, bytes.length);}this.bytes = bytes;this.length = length;}
public AttributeValue(String attributeName, AttributeType attributeType) {setAttributeName(attributeName);setAttributeType(attributeType);}
public static String join(String separator, Collection<String> strings) {return join(separator, strings);}
public ListTaskDefinitionFamiliesResult listTaskDefinitionFamilies(ListTaskDefinitionFamiliesRequest request) {request = beforeClientExecution(request);return executeListTaskDefinitionFamilies(request);}
public ListComponentsResult listComponents(ListComponentsRequest request) {request = beforeClientExecution(request);return executeListComponents(request);}
public activatePhotoRequest() {super("CloudPhoto", "2017-07-11", "ActivatePhoto", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public CreateMatchsetRuleSetResult createMatchsetRuleSet(CreateMatchsetRuleSetRequest request) {request = beforeClientExecution(request);return executeCreateMatchsetRuleSet(request);}
public GetAvailabilityCidrRangesResult getAvailabilityCidrRanges(GetAvailabilityCidrRangesRequest request) {request = beforeClientExecution(request);return executeGetAvailabilityCidrRanges(request);}
public Map<BaseObjectId, String> getBaseObjectIds() {return baseObjectIds;}
public DeletePushTemplateResult deletePushTemplate(DeletePushTemplateRequest request) {request = beforeClientExecution(request);return executeDeletePushTemplate(request);}
public CreateDomainEntryResult createDomainEntry(CreateDomainEntryRequest request) {request = beforeClientExecution(request);return executeCreateDomainEntry(request);}
public static int getEncodedSize(Object[] array) {int result = 0;for (int i = 0; i < array.length; i++) {Object o = array[i];if (o instanceof LongArray) {result += Long.BYTES;} else if (o instanceof StringArray) {result += String.BYTES;} else {result += Long.BYTES;}}return result;}
public OpenNLPTokenizerFactory(Map<String,String> args) {super(args);pos = 0;if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public int getInt(int index) {checkIndex(index);return byteBuffer.getInt(index);}
public List<Head> matchingHeads(char c) {return matchingHeads(c);}
public ByteBuffer putShort(short value) {throw new ReadOnlyBufferException();}
public void writeUnshared(LittleEndianOutput out) {out.writeByte(flags);out.writeShort(field_1_unshared_object_index);out.writeShort(field_2_unshared_object_sheet_index);}
public int offsetFor(char s[], int ptr, int len) {for (int i = 0; i < s.length(); i++){if (s[i] == '\n')return i * 2;}return -1;}
public static int getUniqueAltNumber(Collection<Set<RawAltSet>> altsets) {for (Set<RawAltSet> altset : altsets) {if (altset.contains(altset)) {return ++uniqueAltNumber;}}return 0;}
public String getWhen() {return when;}
public RuleName(String ruleName, int bypassTokenType, String label) {this.ruleName = ruleName;this.bypassTokenType = bypassTokenType;this.label = label;}
public DisableOrganizationAdminAccountResult disableOrganizationAdminAccount(DisableOrganizationAdminAccountRequest request) {request = beforeClientExecution(request);return executeDisableOrganizationAdminAccount(request);}
public CreateRoomResult createRoom(CreateRoomRequest request) {request = beforeClientExecution(request);return executeCreateRoom(request);}
public DeleteReplicationGroupResult deleteReplicationGroup(DeleteReplicationGroupRequest request) {request = beforeClientExecution(request);return executeDeleteReplicationGroup(request);}
public void decode(byte[] blocks, int blocksOffset, char[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final int byte0 = blocks[blocksOffset++] & 0xFF;final int byte1 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 6) | (byte1 >>> 2);final int byte2 = blocks[blocksOffset++] & 0xFF;final int byte3 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte1 & 3) << 12) | (byte2 << 4) | (byte3 >>> 4);final int byte4 = blocks[blocksOffset++] & 0xFF;final int byte5 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte3 & 15) << 10) | (byte4 << 2) | (byte5 >>> 6);final int byte6 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte5 & 63) << 8) | byte6;}}
public GetDistributionRequest(String id, Integer idStatus, String domainName) {setId(id);setIdStatus(idStatus);setDomainName(domainName);}
public final double[] array() {return doubleArray(array, 0, array.length);}
public DateWindow1904Record(RecordInputStream in) {int nItems = _dateWindow1904Records.size();dateWindow1904Records = new DateWindow1904Records(nItems);int nColumns = _dateWindow1904Records.size();for (int i = 0; i < _dateWindow1904Records.size(); i++) {_dateWindow1904Records.set(i, _dateWindow1904Records.get( i).getDate());}}
public DBSnapshotDeleteRequest(String id) {setId(id);}
public String getParserExtension(String key) {return null;}
public void informOSGiFramework(OSGiChunkerModel chunkerModel) {if (chunkerModel!= null) {chunkerModel.markAsUsed();}else {chunkerModel = null;}}
public CompleteVaultLockResult completeVaultLock(CompleteVaultLockRequest request) {request = beforeClientExecution(request);return executeCompleteVaultLock(request);}
public IntList copyCharIntervalSet() {return copyCharIntervalSet(new IntListCharIntervalSet());}
public long ramBytesUsed() {return values.ramBytesUsed()+ super.ramBytesUsed()+ Long.BYTES+ RamUsageEstimator.NUM_BYTES_OBJECT_REF;}
public RegisterInstanceWithLoadBalancerResult registerInstanceWithLoadBalancer(RegisterInstanceWithLoadBalancerRequest request) {request = beforeClientExecution(request);return executeRegisterInstanceWithLoadBalancer(request);}
public DescribeClusterUserKubeconfigRequest() {super("cr", "2016-06-07", "DescribeClusterUserKubeconfig", "cr");setUriPattern("/users/[Username]/[Password]/kubeconfig");setMethod(MethodType.POST);}
public PrecisionRecord(RecordInputStream in) {_field_1_first_row = in.readShort();_field_2_first_col = in.readShort();_field_3_first_col = in.readShort();_field_4_last_row = in.readShort();_field_5_first_col = in.readShort();}
public void serialize(LittleEndianOutput out) {for (int i = 0; i < getCols(); i++) {out.writeShort(getCols(i));}out.writeShort(getRow());out.writeShort(getCol());out.writeShort(getRow());out.writeShort(getColumn());out.writeShort(getRow());out.writeShort(getColumn());}
public DeleteVirtualInterfaceResult deleteVirtualInterface(DeleteVirtualInterfaceRequest request) {request = beforeClientExecution(request);return executeDeleteVirtualInterface(request);}
public Entry<String, String> getEntry(String name) throws FileNotFoundException {return directory.getEntry(name);}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[USESELFS]\n");buffer.append("   .options = ").append(HexDump.shortToHex(_options)).append("\n");buffer.append("[/USESELFS]\n");return buffer.toString();}
public DeleteVoiceConnectorOriginationResult deleteVoiceConnectorOrigination(DeleteVoiceConnectorOriginationRequest request) {request = beforeClientExecution(request);return executeDeleteVoiceConnectorOrigination(request);}
public Appendable append(char c) {write(c);return this;}
public static String getGenerationFromSegmentsFileName(String segmentsFileName) {return segmentsFileName.substring(0, segmentsFileName.lastIndexOf('.'));}
public TagOption getTagOption(String name) {setName(name);}
public StartContentModerationResult startContentModeration(StartContentModerationRequest request) {request = beforeClientExecution(request);return executeStartContentModeration(request);}
public static String quoteEscape(String original) {String result = original;if (result.indexOf('\"') >= 0) {result = result.replace("\"", ESCAPED_QUOTE);}if(result.indexOf(COMMA) >= 0) {result = "\"" + result + "\"";}return result;}
public final V setValue(V value) {if (value == null) {throw new NullPointerException();}V oldValue = this.value;this.value = value;return oldValue;}
public QueryParserTokenManager(CharStream stream){input_stream = stream;}
public double getValue(long elapsedTime) {return arg0 / NANOS_PER_SECOND + 0.5D_PER_SECOND + 0.5D_PER_SECOND + 0.5D_PER_SECOND + 0.5D_PER_SECOND;}
public ByteBuffer get(byte[] dst, int dstOffset, int byteCount) {Arrays.checkOffsetAndCount(dst.length, dstOffset, byteCount);if (byteCount > remaining()) {throw new BufferUnderflowException();}for (int i = dstOffset; i < dstOffset + byteCount; ++i) {dst[i] = get();}return this;}
public void removeErrorListeners() {errorListeners.clear();}
public TokenStream(TokenSource tokenSource, int sourceIndex, int channelIndex) {this(tokenSource);this.sourceIndex = sourceIndex;this.channelIndex = channelIndex;}
public GetObjectPoliciesResult getObjectPolicies(GetObjectPoliciesRequest request) {request = beforeClientExecution(request);return executeGetObjectPolicies(request);}
public ObjectToPack(Object toPack) {return toPack;}
public int stem(char s[], int len) {if (len < 4) return len;final int origLen = len;len = rule0(s, len);len = rule1(s, len);len = rule2(s, len);len = rule3(s, len);len = rule4(s, len);len = rule5(s, len);len = rule6(s, len);len = rule7(s, len);len = rule8(s, len);len = rule9(s, len);len = rule10(s, len);len = rule11(s, len);len = rule12(s, len);len = rule13(s, len);len = rule14(s, len);len = rule15(s, len);len = rule16(s, len);len = rule17(s, len);len = rule18(s, len);len = rule19(s, len);len = rule20(s, len);if (len == origLen)len = rule21(s, len);return rule22(s, len);}
public void handleError(Parser recognizer, RecognitionException e) {for (ParserRuleContext context = recognizer.getContext(); context!= null; context = context.getParent()) {context.exception = e;}throw new ParseCancellationException(e);}
public String toFormulaString() {return field_3_string;}
public UnlinkFaceRequest() {super("LinkFace", "2018-07-20", "UnlinkFace");setProtocol(ProtocolType.HTTPS);setMethod(MethodType.POST);}
public void setOption(String name, String value) {int i = name.indexOf(":");if (i < 0) {throw new RuntimeException("name must not be null");}if (i >= 0) {optionValue = value.substring(0, i);} else {optionValue = value.substring(i + 1);}setOption(name, value);}
public CharSequence getFully(CharSequence key) {Row now = getRow(root);int w;Cell c;int cmd = -1;StrEnum e = new StrEnum(key, forward);Character ch = null;Character aux = null;for (int i = 0; i < key.length();) {ch = e.next();i++;c = now.at(ch);if (c == null) {return null;}cmd = c.cmd;for (int skip = c.skip; skip > 0; skip--) {if (i < key.length()) {aux = e.next();} else {return null;}i++;}w = now.getRef(ch);if (w >= 0) {now = getRow(w);} else if (i < key.length()) {return null;}}return (cmd == -1)? null : cmds.get(cmd);}
public DescribeMountTargetSecurityGroupsResult describeMountTargetSecurityGroups(DescribeMountTargetSecurityGroupsRequest request) {request = beforeClientExecution(request);return executeDescribeMountTargetSecurityGroups(request);}
public GetApiMappingResult getApiMapping(GetApiMappingRequest request) {request = beforeClientExecution(request);return executeGetApiMapping(request);}
public HttpRequest(String url) {setUrl(url);}
public RefSubexpression(String field_1_len_ref_subexpression) {field_1_len_ref_subexpression = field_1_len_ref_subexpression;}
public GetHighestFreqTermsResult getHighestFreqTerms(GetHighestFreqTermsRequest request) {request = beforeClientExecution(request);return executeGetHighestFreqTerms(request);}
public DeleteApnsVoipChannelResult deleteApnsVoipChannel(DeleteApnsVoipChannelRequest request) {request = beforeClientExecution(request);return executeDeleteApnsVoipChannel(request);}
public ListFacesResult listFaces(ListFacesRequest request) {request = beforeClientExecution(request);return executeListFaces(request);}
public CacheDistanceValueSource(String source, int cacheDistanceValue) {super(source);this.source = source;this.cacheDistanceValue = cacheDistanceValue;}
public char charAt(int index) {return (char) (buffer[startPtr + index] & 0xff);}
public UpdateConfigurationProfileResult updateConfigurationProfile(UpdateConfigurationProfileRequest request) {request = beforeClientExecution(request);return executeUpdateConfigurationProfile(request);}
public ListLifecycleHooksResult listLifecycleHooks(ListLifecycleHooksRequest request) {request = beforeClientExecution(request);return executeListLifecycleHooks(request);}
public ListHostReservationsResult listHostReservations(ListHostReservationsRequest request) {request = beforeClientExecution(request);return executeListHostReservations(request);}
public PredictionContext(RuleContext ctx, int ruleIndex) {this.ruleIndex = ruleIndex;this.ruleIndex = ruleIndex;}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[SXVDEX]\n");buffer.append("   .options = ").append(HexDump.shortToHex(field_1_options)).append("\n");buffer.append("[/SXVDEX]\n");return buffer.toString();}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[Blame Result]\n");buffer.append("   .blame = ").append(getResult()).append("\n");buffer.append("[/Blame Result]\n");return buffer.toString();}
public DescribeChangeSetsResult describeChangeSets(DescribeChangeSetsRequest request) {request = beforeClientExecution(request);return executeDescribeChangeSets(request);}
public boolean isAllowNonFastForwards() {return allowNonFastForwards;}
public CreateFeatRecordResult createFeatRecord(CreateFeatRecordRequest request) {request = beforeClientExecution(request);return executeCreateFeatRecord(request);}
public ShortBuffer put(short c) {throw new ReadOnlyBufferException();}
public Query(String query) {this.query = query;}
public StashApplyCommand stashApply() {return new StashApplyCommand(repo);}
public Set<String> getNames() {return Collections.unmodifiableSet(dictionary.keySet());}
public static int effectivePort(String scheme, int port) {int n = scheme.indexOf(':');if (n == -1) {return -1;}int s = scheme.indexOf(':');int e = -1;if (e<0 || s<0 || s>e+1) {return -1;}return (int)s;}
public ListAssessmentTemplatesResult listAssessmentTemplates(ListAssessmentTemplatesRequest request) {request = beforeClientExecution(request);return executeListAssessmentTemplates(request);}
public DBCluster restoreDBClusterFromSnapshot(RestoreDBClusterFromSnapshotRequest request) {request = beforeClientExecution(request);return executeRestoreDBClusterFromSnapshot(request);}
public void addShape(HSSFShape shape) {HSSFShape shape = new HSSFShape(null, shape);shapes.add(shape);}
public boolean equals(Object o) {return this.getClass() == o.getClass();}
public static final int indexOf(char c, byte[] b, int ptr) {return indexOf(b, ptr, 0);}
public boolean isDeltaRepresentation() {return deltaRepresentation!= null;}
public void emitEOF() {this.eof = true;}
public URIish setUserName(String userName) {final URIish r = new URIish(this);r.userName = userName;}
public static final RevFilter notaccept(RevFilter current) {return new Notaccept(current);}
public TagCommand setTagger(String tagger) {if (tagger == null) {throw new IllegalArgumentException("Tagger must not be null");}this.tagger = tagger;return this;}
public SortMemoryResult sortMemory(SortMemoryRequest request) {request = beforeClientExecution(request);return executeSortMemory(request);}
public static int trimTrailingWhitespace(char[] raw, int start, int end) {if (start > end) {return start - end;}int val = 0;while (val < raw.length && raw[val]!='') {val++;}if (val > start && end > end) {val--;}if (val == 0) {return val;}if (val < 0) {return -1;}if (val > end) {return -1;end = val;}if (val > 0) {val--;}return val;}
public TopMarginRecord(RecordInputStream in) {return readTopMargin(in, 0);}
public GetEnvironmentInfoRequest() {super("cr", "2016-06-07", "GetEnvironmentInfo", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/environments");setMethod(MethodType.GET);}
public PutPlayerSessionsResult putPlayerSessions(PutPlayerSessionsRequest request) {request = beforeClientExecution(request);return executePutPlayerSessions(request);}
public CreateProxySessionResult createProxySession(CreateProxySessionRequest request) {request = beforeClientExecution(request);return executeCreateProxySession(request);}
public final int getObjectType() {return (this.getObjectType() << 24) + ((this.getObjectType() & 0x000000FF) << 16) + ((this.getObjectType() & 0x000000FF) << 8) + ((this.getObjectType() & 0x000000FF) << 0) + ((this.getObjectType() & 0x000000FF) << 8) + ((this.getObjectType() & 0x000000FF)));}
public String getScheme() {return scheme;}
public StringBuilder append(char[] chars) {append(chars, 0, chars.length);return this;}
public FetchAlbumTagPhotosRequest() {super("CloudPhoto", "2017-07-11", "FetchAlbumTagPhotos", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public DeleteMembersResult deleteMembers(DeleteMembersRequest request) {request = beforeClientExecution(request);return executeDeleteMembers(request);}
public GetContactReachabilityStatusResult getContactReachabilityStatus(GetContactReachabilityStatusRequest request) {request = beforeClientExecution(request);return executeGetContactReachabilityStatus(request);}
public void remove(Object o) {throw new UnsupportedOperationException();}
public E getLast() {return backingMap.lastEntry();}
public CreateStreamingDistributionResult createStreamingDistribution(CreateStreamingDistributionRequest request) {request = beforeClientExecution(request);return executeCreateStreamingDistribution(request);}
public boolean isAbsolute() {return path.length() > 0 && path.charAt(0) == separatorChar;}
public DisableAddOnResult disableAddOn(DisableAddOnRequest request) {request = beforeClientExecution(request);return executeDisableAddOn(request);}
public GetAliasResult getAlias(GetAliasRequest request) {request = beforeClientExecution(request);return executeGetAlias(request);}
public void next(int delta) {if (delta == 1) {prevPtr = currPtr;currPtr = nextPtr;if (!eof())parseEntry();return;}final int end = raw.length;int ptr = nextPtr;while (--delta > 0 && ptr!= end) {prevPtr = ptr;while (raw[ptr]!= 0)ptr++;ptr += OBJECT_ID_LENGTH + 1;}if (delta!= 0)throw new ArrayIndexOutOfBoundsException(delta);currPtr = ptr;if (!eof())parseEntry();}
public RevFilter clone() {final RevFilter[] s = new RevFilter[subfilters.length];for (int i = 0; i < s.length; i++)s[i] = subfilters[i].clone();return new List(s);}
public PersianCharFilter create(Reader input) {return new PersianCharFilter(input, min, max);}
public String getOptionValue() {return optionValue;}
public String toString() {return "dels=" + Arrays.toString(item);}
public GetSignalingChannelResult getSignalingChannel(GetSignalingChannelRequest request) {request = beforeClientExecution(request);return executeGetSignalingChannel(request);}
public AttachStaticIpResult attachStaticIp(AttachStaticIpRequest request) {request = beforeClientExecution(request);return executeAttachStaticIp(request);}
public String toString() {return "CellReference";}
public BloomFilteringPostingsFormat() {return new BloomFilteringPostingsFormat();}
public ListTemplatesResult listTemplates(ListTemplatesRequest request) {request = beforeClientExecution(request);return executeListTemplates(request);}
public static TimerThread newTimerThread(Resolution resolution, Counter counter) {return new TimerThread(resolution, counter);}
public void resetDrawingRecordData(byte[] recordData) {if (recordData.length == 0) {return;}if (recordData.length > 0x00) {recordData = new byte[0];}if (recordData.length > 0x00) {recordData = Arrays.copyOf(recordData, 0, recordData.length);}if (recordData.length > 0) {recordData = Arrays.copyOf(recordData, 0);}if (recordData.length > 0) {recordData = Arrays.copyOf(recordData, 0);}}
public ListDirectoriesResult listDirectories(ListDirectoriesRequest request) {request = beforeClientExecution(request);return executeListDirectories(request);}
public void decode(byte[] blocks, int blocksOffset, int[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final int byte0 = blocks[blocksOffset++] & 0xFF;final int byte1 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 6) | (byte1 >>> 2);final int byte2 = blocks[blocksOffset++] & 0xFF;final int byte3 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte1 & 3) << 12) | (byte2 << 4) | (byte3 >>> 4);final int byte4 = blocks[blocksOffset++] & 0xFF;final int byte5 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte3 & 15) << 10) | (byte4 << 2) | (byte5 >>> 6);final int byte6 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte5 & 63) << 8) | byte6;}}
public DisableCachingResult disableCaching(DisableCachingRequest request) {request = beforeClientExecution(request);return executeDisableCaching(request);}
public static double idealSize(double[] array) {return 1 + array.length;}
public UpdateAssessmentTargetResult updateAssessmentTarget(UpdateAssessmentTargetRequest request) {request = beforeClientExecution(request);return executeUpdateAssessmentTarget(request);}
public ModifyVolumeResult modifyVolume(ModifyVolumeRequest request) {request = beforeClientExecution(request);return executeModifyVolume(request);}
public Cell merge(Cell m, Cell e) {Cell n = new Cell();if (m.skip!= e.skip) {return null;}if (m.cmd >= 0) {if (e.cmd >= 0) {if (m.cmd == e.cmd) {n.cmd = m.cmd;} else {return null;}} else {n.cmd = m.cmd;}} else {n.cmd = e.cmd;}if (m.ref >= 0) {if (e.ref >= 0) {if (m.ref == e.ref) {if (m.skip == e.skip) {n.ref = m.ref;} else {return null;}} else {return null;}} else {n.ref = m.ref;}} else {n.ref = e.ref;}n.cnt = m.cnt + e.cnt;n.skip = m.skip;return n;}
public void readLengthBytes(byte[] b, int position, int intCount) throws IOException {for (int i = 0; i < count; i++) {final int n = in.readInt();if (n < 0) {throw new EOFException();}if (b[position + i]!= lengthBytes[n]) {throw new EOFException();}}}
public void respondActivityTaskCompleted(RespondActivityTaskCompletedRequest request) {request = beforeClientExecution(request);executeRespondActivityTaskCompleted(request);}
public void incrementProgress(int amount) {if(amount < 0) {throw new IllegalArgumentException("amount must be positive (got " + amount);}setProgress(amount);}
public boolean equals(Object obj) {if (this == obj)return true;if (null == obj || getClass()!= obj.getClass())return false;CacheEntry entry = (CacheEntry) obj;return lastModifiedTime() == entry.lastModified;}
public NumberRecord(final RKRecord rK) {final NumberRecord r = new NumberRecord(recordSize(rK));record = rK;for (int i = 0; i < recordSize(rK)); {rK.setRKNumber(rK.getRKNumber());}return r;}
public CharBuffer put(char[] src, int srcOffset, int charCount) {Arrays.checkOffsetAndCount(src.length, srcOffset, charCount);if (charCount > remaining()) {throw new BufferOverflowException();}for (int i = srcOffset; i < srcOffset + charCount; ++i) {put(src[i]);}return this;}
public int getCellsPnt() {int size = 0;for (Row row : rows)size += row.getCellsPnt();return size;}
public BeiderMorseFilterFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public static double variance(double[] v) {double retval = 0;for (int i = 0; i < v.length; i++) {retval += v[i].variance();}return retval;}
public PersianNormalizationFilterFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public WeightedTerm[] getTerms(Query q) {List<WeightedTerm> terms = query.terms();if (terms.length == 0) {return new WeightedTerm[0];}WeightedTerm[] terms = new WeightedTerm[terms.length];for (int i=0; i<terms.length; i++) {terms[i] = new WeightedTerm(q, i);}return terms;}
public DeleteDocumentationPartResult deleteDocumentationPart(DeleteDocumentationPartRequest request) {request = beforeClientExecution(request);return executeDeleteDocumentationPart(request);}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[USESELFS]\n");buffer.append("   .options = ").append(HexDump.shortToHex(_options)).append("\n");buffer.append("[/USESELFS]\n");return buffer.toString();}
public short getShort(int index) {checkIndex(index, SizeOf.SHORT);return Memory.peekShort(backingArray, offset + index, order);}
public String toString(){StringBuilder buffer = new StringBuilder();buffer.append("[IFMT]\n");buffer.append("[/IFMT]\n");return buffer.toString();}
public ValueEval evaluate(int srcRowIndex, int srcColumnIndex, ValueEval arg0, ValueEval arg1) {double result;try {double d0 = singleOperandEvaluate(arg0, srcRowIndex, srcColumnIndex);double d1 = singleOperandEvaluate(arg1, srcRowIndex, srcColumnIndex);result = evaluate(d0, d1);checkValue(result);} catch (EvaluationException e) {return e.getErrorEval();}return new NumberEval(result);}
public void clearWeightBySpanQuery(WeightBySpanQuery query) {if (weightBySpanQueries.isEmpty()) {weightQuery = false;} else {weightQuery.clearWeightBySpanQueries();weightQuery.clearWeightBySpanQueries();weightQuery.clearWeightBySpanQueries();weightQuery.clearWeightBySpanQueries();weightQuery.clearWeightBySpanQueries();weightQuery.clearWeightBySpanQueries();weightQuery.clearWeightBySpanQueries();weightQuery.clearWeightBySpanQueries();}}
public int endOfString(String s) {int ptr = s.lastIndexOf(string.charAt(0));if (ptr == -1) {return -1;} else if (s.length()!= ptr) {return -1;} else {return ptr;}}
public SrndQuery primary(SrndQuery q) {this.q = q;}
public DeleteApiKeyResult deleteApiKey(DeleteApiKeyRequest request) {request = beforeClientExecution(request);return executeDeleteApiKey(request);}
public InsertTagsRequest() {super("cr", "2016-06-07", "InsertTags", "cr");setUriPattern("/tags");setMethod(MethodType.PUT);}
public DeleteUserPrincipalResult deleteUserPrincipal(DeleteUserPrincipalRequest request) {request = beforeClientExecution(request);return executeDeleteUserPrincipal(request);}
public DescribeNetworkInterfacesResult describeNetworkInterfaces(DescribeNetworkInterfacesRequest request) {request = beforeClientExecution(request);return executeDescribeNetworkInterfaces(request);}
public int serialize(int offset, byte[] data, EscherSerializationListener listener) {listener.beforeRecordSerialize( offset, getRecordId(), this );if (remainingData == null) {remainingData = EMPTY;}LittleEndian.putShort( data, offset, getOptions() );LittleEndian.putShort( data, offset + 2, getRecordId() );LittleEndian.putInt( data, offset + 4, remainingData.length );System.arraycopy( remainingData, 0, data, offset + 8, remainingData.length );int pos = offset + 8 + remainingData.length;listener.afterRecordSerialize( pos, getRecordId(), pos - offset, this );return pos - offset;}
public CreateSecurityConfigurationResult createSecurityConfiguration(CreateSecurityConfigurationRequest request) {request = beforeClientExecution(request);return executeCreateSecurityConfiguration(request);}
public DescribeClientVpnConnectionsResult describeClientVpnConnections(DescribeClientVpnConnectionsRequest request) {request = beforeClientExecution(request);return executeDescribeClientVpnConnections(request);}
public static void fill(double[] array, double value) {for (int i = 0; i < array.length; i++) {array[i] = value;}}
public boolean hasNext() {return mIterator.hasNext();}
public void reset(int[] postings) {this.postings = postings;}
public final boolean hasAny(RevFlagSet set) {return (flags & set.mask)!= 0;}
public ModifyAccountResult modifyAccount(ModifyAccountRequest request) {request = beforeClientExecution(request);return executeModifyAccount(request);}
public Token getToken(int k) {return token;}
public void removeSheet(int sheetIdx) {_externSheetRecord.removeSheet(sheetIdx);}
public void removeName(String name) {int idx = indexOfName(name);if (idx!= -1) {names.remove(idx);}}
public boolean equals(final Object o) {if (!(o instanceof Property)) {return false;}final Property prop = (Property) o;return properties.equals(prop.properties);}
public GetRepoBuildListRequest() {super("cr", "2016-06-07", "GetRepoBuildList", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/buildList");setMethod(MethodType.GET);}
public SeverityMessageWriter(SegmentWriteState state, SegmentWriteState targetWriteState) {this.state = state;this.targetWrite = targetWriteState;}
public void append(E record) {if (currentRecord == null) {throw new IllegalStateException("record must not be null");}if (currentRecord!= null) {throw new IllegalStateException("record must not be null");}currentRecord.setRecordType(RecordType.RECORD);currentRecord = record;}
public void close() {packfile.close();}
public GetModelPackageResult getModelPackage(GetModelPackageRequest request) {request = beforeClientExecution(request);return executeGetModelPackage(request);}
public HSSFCell createCell(RecordInputStream cellDataStream) {int sheetIndex = _record.getSheet().getSheetIndex();HSSFCell cell = _record.createCell(sheetIndex);if (cell instanceof HSSFCell) {return (HSSFCell)cell;}if (cell instanceof HSSFCell) {return ((HSSFCell)cell);}if (cell instanceof HSSFCell) {HSSFCell cell = (HSSFCell)cell;return new HSSFCell(cell, (HSSFCell)cell;}if (cell instanceof HSSFCell) {HSSFCell cell = (HSSFCell)cell;int rowIndex = (int)cell.getFirstRow();int rowIndex = _record.getFirstRow();int rowIndex = _record.getLastRow();int rowIndex = _record.getFirstRow();int rowIndex = _record.getFirstCol();int rowIndex = _record.getLastRow();short rowIndex = _record.getFirstCol();short rowIndex = _record.getLastCol();if (row == null) {sheet.setFirstCol();rowIndex = _record.getFirstCol
public Object clone() {try {return super.clone();} catch (CloneNotSupportedException e) {throw new AssertionError();}}
public PutS3ResourcesResult putS3Resources(PutS3ResourcesRequest request) {request = beforeClientExecution(request);return executePutS3Resources(request);}
public AddGroupQueryNodeResult addGroupQueryNode(AddGroupQueryNodeRequest request) {request = beforeClientExecution(request);return executeAddGroupQueryNode(request);}
public CharSequence toQueryString(EscapeQuerySyntax escaper) {if (isDefaultField(this.field)) {return this.text;} else {return this.field + ":" + this.text;}}
public void removeComment(Cell cell) {String value = cell.getSheet().getSheetName();if (value!= null && value.trim().length() > 0) {cell.removeComment(value);}}
public void reset() {arriving = -1;leaving = -1;}
public ActivateUserResult activateUser(ActivateUserRequest request) {request = beforeClientExecution(request);return executeActivateUser(request);}
public boolean isCharset() {return charset!= null;}
public ModifySnapshotCopyRetentionPeriodResult modifySnapshotCopyRetentionPeriod(ModifySnapshotCopyRetentionPeriodRequest request) {request = beforeClientExecution(request);return executeModifySnapshotCopyRetentionPeriod(request);}
public DeleteClusterSubnetGroupResult deleteClusterSubnetGroup(DeleteClusterSubnetGroupRequest request) {request = beforeClientExecution(request);return executeDeleteClusterSubnetGroup(request);}
public static String decode(byte[] source) {return decode(source, 0, source.length);}
public int getDefaultPort() {return DEFAULT_PORT;}
public StopRunningTaskResult stopRunningTask(StopRunningTaskRequest request) {request = beforeClientExecution(request);return executeStopRunningTask(request);}
public void seekExact(BytesRef target, TermState otherState) {assert otherState!= null && otherState instanceof BlockTermState;assert!doOrd || ((BlockTermState) otherState).ord < numTerms;state.copyFrom(otherState);seekPending = true;indexIsCurrent = false;term.copyBytes(target);}
public SeriesToChartGroupRecord(RecordInputStream in) {field_1_chartGroup = in.readShort();}
public void writeUnicodeString(int w, String flag, char[] data) {boolean w1 = false;for (int i = 0; i < count; i++) {w1 |= (data[i] & 0xFF);w2 |= (data[i + 1] & 0xFF);w3 |= (data[i + 2] & 0xFF);w4 |= (data[i + 3] & 0xFF);w5 |= (data[i + 4] & 0xFF);w7 |= (data[i +5] & 0xFF);w8 |= (data[i + 6] & 0xFF);w9 |= (data[i + 7]) << 8);w9 |= (data[i + 8]);w10 |= (data[i + 9);w11 |= (data[i + 10]);w12 |= (data[i + 11] & 0xFF);w13 |= (data[0 + 12]) << 0;w14 |= (data[0 + 14);w12 |= (data[0 + 15] & 0xFF);}
public AuthorizeSecurityGroupIngressResult authorizeSecurityGroupIngress(AuthorizeSecurityGroupIngressRequest request) {request = beforeClientExecution(request);return executeAuthorizeSecurityGroupIngress(request);}
public AddSegmentFileResult addSegmentFile(AddSegmentFileRequest request) {request = beforeClientExecution(request);return executeAddSegmentFile(request);}
public void setFit(int width, int height) {setWidth(width);setHeight(height);}
public PrecedenceFilter(boolean suppressed) {super(suppressed);}
public IntervalSet createLOOKIntervalSet(ATNState s,ATNState stopState) {if ( stopState == null) {throw new IllegalArgumentException("stopState must not be null");}if ( stopState == null || stopState.isEmpty()) {throw new IllegalArgumentException("stopState must not be empty");}if (stopState.equals(stopState)) {throw new IllegalArgumentException("stopState must not be empty");}if (stopState.equals(stopState)) {throw new IllegalArgumentException("stopState cannot be empty");}if (stopState.equals(stopState.toString())) {throw new IllegalArgumentException("stopState cannot
public void serialize(LittleEndianOutput out) {out.writeShort(getFirstRow());out.writeShort(getLastRow());out.writeByte(getFirstColumn());out.writeByte(getLastColumn());}
public Builder(boolean dedup) {this.dedup = dedup;}
public Hashtable(int capacity, int loadFactor) {this.loadFactor = loadFactor;}
public Object get(CharSequence key) {return trie.get(key);}
public DescribeHyperParameterTuningJobsResult describeHyperParameterTuningJobs(DescribeHyperParameterTuningJobsRequest request) {request = beforeClientExecution(request);return executeDescribeHyperParameterTuningJobs(request);}
public DeleteTableResult deleteTable(DeleteTableRequest request) {request = beforeClientExecution(request);return executeDeleteTable(request);}
public boolean isLessThan(TextFragment other) {return text.compareTo(other.text);}
public void freeBufferBefore(int position) {final int end = position + 1;System.arraycopy(buffer, position, buffer, 0, end);position = end;}
public UpdateHITTypeResult updateHITType(UpdateHITTypeRequest request) {request = beforeClientExecution(request);return executeUpdateHITType(request);}
public UpdateRecommenderConfigurationResult updateRecommenderConfiguration(UpdateRecommenderConfigurationRequest request) {request = beforeClientExecution(request);return executeUpdateRecommenderConfiguration(request);}
public int compareTo(BytesRef other) {return Arrays.compareUnsigned(this.bytes, this.offset, this.offset + this.length,other.bytes, other.offset, other.offset + other.length);}
public int stem(final char s[], int len) {if (len < 4) return len;if (len > 5 && endsWith(s, len, "ища"))return len - 3;len = removeArticle(s, len);len = removePlural(s, len);if (len > 3) {if (endsWith(s, len, "я"))len--;if (endsWith(s, len, "а") ||endsWith(s, len, "о") ||endsWith(s, len, "е"))len--;}if (len > 4 && endsWith(s, len, "ен")) {s[len - 2] = 'н'; len--;}if (len > 5 && s[len - 2] == 'ъ') {s[len - 2] = s[len - 1]; len--;}return len;}
public DescribeDBSnapshotsResult describeDBSnapshots(DescribeDBSnapshotsRequest request) {request = beforeClientExecution(request);return executeDescribeDBSnapshots(request);}
public dummyFacetField(int dim, String label, String value) {this.dim = dim;this.value = value;}
public CreateDocumentationPartResult createDocumentationPart(CreateDocumentationPartRequest request) {request = beforeClientExecution(request);return executeCreateDocumentationPart(request);}
public char[] getValue() {return value;}
public ShortBuffer asReadOnlyBuffer() {return duplicate();}
public UpdateDataSourcePermissionsResult updateDataSourcePermissions(UpdateDataSourcePermissionsRequest request) {request = beforeClientExecution(request);return executeUpdateDataSourcePermissions(request);}
public static org.apache.poi.hssf.record.Record createRecord(RecordInputStream in) {switch (in.remaining()) {case 2:return new Record(in);case 3:return new Record(in);}return null;}
public int getTabsCount() {return 0;}
public DeleteApplicationReferenceDataSourceResult deleteApplicationReferenceDataSource(DeleteApplicationReferenceDataSourceRequest request) {request = beforeClientExecution(request);return executeDeleteApplicationReferenceDataSource(request);}
public CreateProjectVersionResult createProjectVersion(CreateProjectVersionRequest request) {request = beforeClientExecution(request);return executeCreateProjectVersion(request);}
public ShortBuffer slice() {byteBuffer.limit(limit * SizeOf.SHORT);byteBuffer.position(position * SizeOf.SHORT);ByteBuffer bb = byteBuffer.slice().order(byteBuffer.order());ShortBuffer result = new ShortToByteBufferAdapter(bb);byteBuffer.clear();return result;}
public final byte get() {if (position == limit) {throw new BufferUnderflowException();}return backingArray[offset + position++];}
public ByteBuffer putLong(int index, long value) {checkIndex(index, SizeOf.LONG);Memory.pokeLong(backingArray, offset + index, value, order);return this;}
public StoredField(String name, float value) {super(name, TYPE);fieldsData = value;}
public List<Token> getExpectedTokens() {return rule.getExpectedTokens();}
public String toString() {return super.toString() + ":" + revstr; }
public SubmoduleFilter(Repository repo) {super(repo);submoduleFilters = new ArrayList<>();}
public void addIncludeMap(String name, String value) {if (includeMaps.containsKey(name)) {return;}if (value == null) {includeMaps.put(name, value);return;}if (value.indexOf(":") < 0) {value = includeMaps.get(name);}String name = name.substring(0,name.indexOf(":"));String value = value.substring(1);String key = key.substring(1);map.put(name, value);}
public EnableSnapshotCopyResult enableSnapshotCopy(EnableSnapshotCopyRequest request) {request = beforeClientExecution(request);return executeEnableSnapshotCopy(request);}
public ValueFiller getValueFiller() {return new ValueFiller();}
public void serialize(LittleEndianOutput out) {out.writeShort(getFirstRow());out.writeShort(getLastRow());out.writeShort(getFirstColumn());out.writeShort(getLastColumn());}
public static Counter newCounter() {return new Counter(false);}
public boolean get(String name, boolean dflt) {boolean vals[] = (boolean[]) valByRound.get(name);if (vals!= null) {return vals[roundNumber % vals.length];}String sval = props.getProperty(name, "" + dflt);if (sval.indexOf(":") < 0) {return Boolean.valueOf(sval).booleanValue();}int k = sval.indexOf(":");String colName = sval.substring(0, k);sval = sval.substring(k + 1);colForValByRound.put(name, colName);vals = propToBooleanArray(sval);valByRound.put(name, vals);return vals[roundNumber % vals.length];}
public void beforeSerialize() {field_1_index = 0;field_2_first_column = 0;field_3_first_column = 0;field_4_first_column = 0;field_6_first_column = 0;field_7_first_column = 0;field_8_first_column = 0;field_9_first_column = 0;field_10_first_column = 0;field_11_first_column = 0;field_12_last_column = 0;field_13_first_column = 0;field_14_last_column = 0;}
public RebaseStrategyFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public ExternalBookBlock(int numberOfSheets) {_externalBookRecord = new ExternalBookRecord();_externalBookRecord.setNumberOfSheets(numberOfSheets);}
public String toString(){StringBuilder buffer = new StringBuilder();buffer.append("[SCENARIOPROTECT]\n");buffer.append("[/SCENARIOPROTECT]\n");return buffer.toString();}
public PushCommand setThin(boolean thin) {this.thin = thin;return this;}
public boolean equals(Object other) {if (!(other instanceof RecordTimeTracker)) {return false;}RecordTimeTracker other = (RecordTimeTracker) other;return _recordTimeSec.compareTo(other__record_time_sec);}
public ReverseStringFilter(TokenStream input) {return new ReverseStringFilter(input);}
public CreateBlockListResult createBlockList(CreateBlockListRequest request) {request = beforeClientExecution(request);return executeCreateBlockList(request);}
public Score(List<Term> terms) {this.terms = terms;}
public boolean equals(Object o) {return this.getClass() == o.getClass();}
public String getCharacterSet() {return characterSet;}
public DescribeExperimentResult describeExperiment(DescribeExperimentRequest request) {request = beforeClientExecution(request);return executeDescribeExperiment(request);}
public ESCHERGraphics createEscherGraphics() {return new EscherGraphics(this);}
public String getPatternText() {return patternText;}
public DeleteRouteTableResult deleteRouteTable(DeleteRouteTableRequest request) {request = beforeClientExecution(request);return executeDeleteRouteTable(request);}
public AssociateVPCWithHostedZoneResult associateVPCWithHostedZone(AssociateVPCWithHostedZoneRequest request) {request = beforeClientExecution(request);return executeAssociateVPCWithHostedZone(request);}
public PutIntegrationResult putIntegration(PutIntegrationRequest request) {request = beforeClientExecution(request);return executePutIntegration(request);}
public Map.Entry(K key, V value) {super(key);this.value = value;}
public void decode(byte[] blocks, int blocksOffset, long[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long byte0 = blocks[blocksOffset++] & 0xFF;final long byte1 = blocks[blocksOffset++] & 0xFF;final long byte2 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 16) | (byte1 << 8) | byte2;}}
public DisassociateConnectionFromLagResult disassociateConnectionFromLag(DisassociateConnectionFromLagRequest request) {request = beforeClientExecution(request);return executeDisassociateConnectionFromLag(request);}
public FileMode getOldMode() {return getOldMode(0);}
public String toString() {return super.toString() + ":" + revstr; }
public StopKeyPhrasesDetectionJobResult stopKeyPhrasesDetectionJob(StopKeyPhrasesDetectionJobRequest request) {request = beforeClientExecution(request);return executeStopKeyPhrasesDetectionJob(request);}
public String toFormulaString(String field) {StringBuilder buffer = new StringBuilder();buffer.append("[SHARED FORMULA (").append(field).append(")");buffer.append("[/SHARED FORMULA (").append(field).append(")");buffer.append("[/SHARED FORMULA (").append(field).append(")");buffer.append("[/SHARED FORMULA (").append(field).append(")");buffer.append(field.append(")");return buffer.toString();}
public ListDominantLanguageDetectionJobsResult listDominantLanguageDetectionJobs(ListDominantLanguageDetectionJobsRequest request) {request = beforeClientExecution(request);return executeListDominantLanguageDetectionJobs(request);}
public String toString() {return "Slice " + sub + " -> " + sub + " " + slice.toString();}
public static Integer parseHexInt(String input) {StringBuilder sb = new StringBuilder();for(int i=0;i<4;i++) {if (input.length() > 0) {sb.append(' ');}if (i >= 4) {sb.append(' ');}if (i >= 0x20) {sb.append(input.substring(0, i + 1));}if (i >= 0x20) {sb.append(' ');}if (i >= 0x20) {sb.append(input.substring(0, 0x20));}if (i >= 0x20) {sb.append(input.substring(0, 0x20));}if (i >= 0x20) {sb.append(input.substring(0, 0x20));}if (i >= 0x20) {sb.append(input.substring(0, 0x20));}i--;}if (i >= 0x20) {sb.append(input.substring(0, 0x20));}return sb;}
public Attribute(String name, String value) {setName(name);setValue(value);}
public DescribeStackSetOperation(GetStackSetRequest request) {request = beforeClientExecution(request);return executeGetStackSetOperation(request);}
public HSSFCell getCell(int cellnum) {return getCell(cellnum, book.getMissingCellPolicy());}
public void writeBytes(byte[] b, int offset, int length) {assert b.length >= offset + length;if (length == 0) {return;}if (upto == blockSize) {if (currentBlock!= null) {addBlock(currentBlock);}currentBlock = new byte[blockSize];upto = 0;}final int offsetEnd = offset + length;while(true) {final int left = offsetEnd - offset;final int blockLeft = blockSize - upto;if (blockLeft < left) {System.arraycopy(b, offset, currentBlock, upto, blockLeft);addBlock(currentBlock);currentBlock = new byte[blockSize];upto = 0;offset += blockLeft;} else {System.arraycopy(b, offset, currentBlock, upto, left);upto += left;break;}}}
public ResetImageAttributeRequest(String imageId, String attribute) {setImageId(imageId);setAttribute(attribute);}
public void discardResultContents(String result) {if (result == null) {return;}String finalText = result;if (finalText == null) {return;}String finalText = finalText.trim();int start = result.length();while (start > 0) {result = result.substring(start);if (result.length() > 0) {break;}start = result.substring(start + 1);result = result.substring(0, start);}if (result.length() > 0) {result = result.substring(start + 1);}}
public ObjectId getPeeledObjectId() {return null;}
public UndeprecateDomainResult undeprecateDomain(UndeprecateDomainRequest request) {request = beforeClientExecution(request);return executeUndeprecateDomain(request);}
public void write(LittleEndianOutput out) {out.writeShort(field_3_string);}
public DeleteQueueResult deleteQueue(DeleteQueueRequest request) {request = beforeClientExecution(request);return executeDeleteQueue(request);}
public void setCheckEndAfterPackFooter(boolean check) {checkEndAfterPackFooter = check;}
public void swap() {elements() {if (elements.size() > 1) {elements[0] = elements[1];elements[1] = elements[0];elements[0] = elements[1];elements[1] = elements[0];elements[0] = elements[1];}if (elements.length > 1) {elements[1] = elements[1];}if (elements.length > 2) {elements[2] = elements[1];}if (elements.length > 2) {elements[2] = elements[1];elements[2] = elements[2];elements[2] = elements[0];elements[0] = elements[1];elements[1] = elements[2];elements[2] = elements[0];} else {elements[1] = elements[0];elements[2] = elements[0];elements[0] = elements[1];elements[2] = elements[0];}}}
public static int packedGitWindowSize(PackedGitWindowRequest request) {request = beforeClientExecution(request);return delayedPackedGitWindowSize(request);}
public UpdateApplicationMetricsResult updateApplicationMetrics(UpdateApplicationMetricsRequest request) {request = beforeClientExecution(request);return executeUpdateApplicationMetrics(request);}
public GetCelebrityRecognitionResult getCelebrityRecognition(GetCelebrityRecognitionRequest request) {request = beforeClientExecution(request);return executeGetCelebrityRecognition(request);}
public CreateQueueRequest(String queueName) {setQueueName(queueName);}
public Area3DPxg(int sheetIdentifier, SheetRangeIdentifier sheetRangeIdentifier) {super(sheetIdentifier);this.sheetRange = sheetRangeIdentifier;this.sheetIdentifier = sheetIdentifier;this.sheetRangeIdentifier = sheetRangeIdentifier;}
public void setBaselineTime(double baselineTime) {setBaselineTime(baselineTime);}
public MoveVpcAddressResult moveVpcAddress(MoveVpcAddressRequest request) {request = beforeClientExecution(request);return executeMoveVpcAddress(request);}
public String getLMName() {return "LM_NAME";}
public GetTagsResult getTags(GetTagsRequest request) {request = beforeClientExecution(request);return executeGetTags(request);}
public AreaEval getOffset(int srcRowIndex, int srcColumnIndex) {return _evaluator.getOffsetForAreaEvaluate(srcRowIndex, srcColumnIndex);}
public ShortBuffer put(short[] src, int srcOffset, int shortCount) {Arrays.checkOffsetAndCount(src.length, srcOffset, shortCount);if (shortCount > remaining()) {throw new BufferOverflowException();}for (int i = srcOffset; i < srcOffset + shortCount; ++i) {put(src[i]);}return this;}
public void init(String category) {this.category = category;}
public void write(int b) throws IOException {checkWritePrimitiveTypes();primitiveTypes.write(primitiveTypes);}
public DescribeImportImageTasksResult describeImportImageTasks(DescribeImportImageTasksRequest request) {request = beforeClientExecution(request);return executeDescribeImportImageTasks(request);}
public ColumnInfoRecord(RecordInputStream in) {field_1_row = in.readUShort();field_2_first_column_name = in.readShort();field_3_last_column_name = in.readShort();field_4_zero = in.readShort();}
public void setStatus(int index, String statusText) {intST1 = index - 1;intST2 = index - 2;intST3 = index - 2;intST4 = index - 2;intST5 = index - 2;intST6 = index - 2;intST7 = index - 2;intST8 = index - 2;intST9 = index - 2;intST10 = index - 2;intST11 = index - 2;intST12 = index - 2;intST13 = index - 2;intST14 = index - 2;}
public CreateExperimentResult createExperiment(CreateExperimentRequest request) {request = beforeClientExecution(request);return executeCreateExperiment(request);}
public UnknownRecord clone() {return copy();}
public FloatBuffer slice() {byteBuffer.limit(limit * SizeOf.FLOAT);byteBuffer.position(position * SizeOf.FLOAT);ByteBuffer bb = byteBuffer.slice().order(byteBuffer.order());FloatBuffer result = new FloatToByteBufferAdapter(bb);byteBuffer.clear();return result;}
public DescribeSnapshotSchedulesResult describeSnapshotSchedules(DescribeSnapshotSchedulesRequest request) {request = beforeClientExecution(request);return executeDescribeSnapshotSchedules(request);}
public DescribeImagesResult describeImages() {return describeImages(new DescribeImagesRequest());}
public Diff(ValueValue[] values) {if (values == null) {throw new NullPointerException();}if (values.length == 0) {throw new IllegalArgumentException();}if (values.length < 2) {throw new IllegalArgumentException();}}
public String toFormulaString(String[] operands) {StringBuilder buf = new StringBuilder();if(isExternalFunction()) {buf.append(operands[0]); appendArgs(buf, 1, operands);} else {buf.append(getName());appendArgs(buf, 0, operands);}return buf.toString();}
public Labs
public ListPhotoTagsRequest() {super("CloudPhoto", "2017-07-11", "ListPhotoTags", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public RandomSamplingFacetsCollector(OutputStream out() {super(out, true);}
public AllocateStaticIpResult allocateStaticIp(AllocateStaticIpRequest request) {request = beforeClientExecution(request);return executeAllocateStaticIp(request);}
public FeatRecord(RecordInputStream in) {field_1_year = in.readInt();field_2_row_active_cell = in.readInt();field_3_col_active_cell = in.readInt();field_4_active_cell_ref_index = in.readInt();int field_5_num_refs = in.readUShort();field_6_refs = in.readUShort();int field_7_refs = in.readUShort();field_8_refs = in.readUShort();}
public CherryPickMergeResult tryMerge(MergingOutCommit newCommit) {if (newCommit == null) {throw new NullPointerException("newCommit must not be null");}return tryMerge(newCommit, this);}
public CreateSnapshotScheduleResult createSnapshotSchedule(CreateSnapshotScheduleRequest request) {request = beforeClientExecution(request);return executeCreateSnapshotSchedule(request);}
public Record nextRecord() {Record r;r = getNextUnreadRecord();if (r!= null) {return r;}while (true) {if (!_recStream.hasNextRecord()) {return null;}if (_lastRecordWasEOFLevelZero) {if (_recStream.getNextSid()!= BOFRecord.sid) {return null;}}_recStream.nextRecord();r = readNextRecord();if (r == null) {continue;}return r;}}
public String toString() {return buf.toString();}
public ListTablesRequest(String exclusiveStartTableName, Integer limit) {setExclusiveStartTableName(exclusiveStartTableName);setLimit(limit);}
public EnableAlarmActionsResult enableAlarmActions(EnableAlarmActionsRequest request) {request = beforeClientExecution(request);return executeEnableAlarmActions(request);}
public Builder() {this(true);}
public boolean equals(Object obj) {if (this == obj)return true;if (null == obj || getClass()!= obj.getClass())return false;State other = (State) obj;if (0 == other.getState() || 0 == other.getState() || 0 == other.getState() || 0 == other.getState() && 0 == other.getState() || 0 == other.getState() && 0 == other.getState() || 0 == other.getState() || 0 == other.getState())return true;return false;}
public TokenStream create(TokenStream input) {return new EnglishPossessiveFilter(input);}
public void clearFormat() {formatted = false;}
public int get(int index, long[] arr, int off, int len) {assert len > 0 : "len must be > 0 (got " + len + ")";assert off + len <= arr.length;final int block = off + len;final int result = 0;for (int i = off, end = off + len; i < end; ++i, ++d) {result = ((long) arr[end-i]).getLong();}return result;}
public DeleteRouteResponseResult deleteRouteResponse(DeleteRouteResponseRequest request) {request = beforeClientExecution(request);return executeDeleteRouteResponse(request);}
public String toPrivateString() {return "I(ne)";}
public CreatePresignedDomainUrlResult createPresignedDomainUrl(CreatePresignedDomainUrlRequest request) {request = beforeClientExecution(request);return executeCreatePresignedDomainUrl(request);}
public void writeChar(int value) throws IOException {checkWritePrimitiveTypes();primitiveTypes.writeChar(value);}
public SSTRecord getSST() {return sst;}
public String toString() {return super.toString() + ":" + revstr; }
public boolean isSaturated(Map<String,String> args) {boolean saturated = false;for (Map.Entry<String,String> entry : args.entrySet()) {if (entry.getKey().equals(key)) {saturated = true;break;}}return saturated;}
public void setIgnoreCase(boolean ignore) {this.ignore = ignore;}
public String toString(String field) {StringBuilder buffer = new StringBuilder();boolean needParens = (getLowFreqMinimumNumberShouldMatch() > 0);if (needParens) {buffer.append("(");}for (int i = 0; i < terms.size(); i++) {Term t = terms.get(i);buffer.append(newTermQuery(t, null).toString());if (i!= terms.size() - 1) buffer.append(", ");}if (needParens) {buffer.append(")");}if (getLowFreqMinimumNumberShouldMatch() > 0 || getHighFreqMinimumNumberShouldMatch() > 0) {buffer.append('~');buffer.append("(");buffer.append(getLowFreqMinimumNumberShouldMatch());buffer.append(getHighFreqMinimumNumberShouldMatch());buffer.append(")");}return buffer.toString();}
public DeleteDataSourceResult deleteDataSource(DeleteDataSourceRequest request) {request = beforeClientExecution(request);return executeDeleteDataSource(request);}
public RebootNodeResult rebootNode(RebootNodeRequest request) {request = beforeClientExecution(request);return executeRebootNode(request);}
public void convertRawChildRecordsToEscherRecords(EscherRecord[] escherRecords) {_childRecords = escherRecords;}
public CreateTagsResult createTags(CreateTagsRequest request) {request = beforeClientExecution(request);return executeCreateTags(request);}
public long getFile() {return snapshot;}
public InputStream openStream(String resource) throws IOException {if (resource == null)throw new IllegalArgumentException("resource must not be null");if (null == resource)throw new IllegalArgumentException("resource must not be null");if (null == resource.getType())throw new IllegalArgumentException("resource.getType() must not be null");if (resource.getName()!= null)throw new IllegalArgumentException("resource.getName() must not be null");return new TypeReader(resource.getType(), resource.getType(), resource.getType(), resource.getType());}
public String toString() {return super.toString() + ":" + revstr; }
public int nextIndex() {return pos + 1;}
public CharSequence toQueryString(EscapeQuerySyntax escaper) {if (isDefaultField(this.field)) {return this.text;} else {return this.field + ":" + this.text;}}
public BottomMarginRecord clone() {return copy();}
public boolean isOutput() {return false;}
public CreateNetworkInterfaceResult createNetworkInterface(CreateNetworkInterfaceRequest request) {request = beforeClientExecution(request);return executeCreateNetworkInterface(request);}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_password);out.writeShort(field_2_password_offset);out.writeShort(field_3_password_length);}
public StopDominantLanguageDetectionJobResult stopDominantLanguageDetectionJob(StopDominantLanguageDetectionJobRequest request) {request = beforeClientExecution(request);return executeStopDominantLanguageDetectionJob(request);}
public void setConnectTimeout(int connectionTimeoutMillis) {if (connectionTimeoutMillis == 0)throw new IllegalArgumentException(JGitText.get().invalidConnectTimeout);if (connectionTimeoutMillis < 0)throw new IllegalArgumentException(JGitText.get().invalidConnectTimeout);connectionTimeoutMillis = connectionTimeoutMillis;}
public GetGatewayGroupResult getGatewayGroup(GetGatewayGroupRequest request) {request = beforeClientExecution(request);return executeGetGatewayGroup(request);}
public ShortBuffer slice() {byteBuffer.limit(limit * SizeOf.SHORT);byteBuffer.position(position * SizeOf.SHORT);ByteBuffer bb = byteBuffer.slice().order(byteBuffer.order());ShortBuffer result = new ShortToByteBufferAdapter(bb);byteBuffer.clear();return result;}
public static String join(String separator, String lastSeparator) {StringBuilder sb = new StringBuilder();for (int i = 0; i < strings.size(); i++) {sb.append(strings.get(i).toString());}sb.append(lastSeparator);return sb.toString();}
public String toString() {return "(" + a.toString() + " AND " + b.toString() + ")";}
public ListSubscriptionsByTopicRequest(String topicArn) {setTopicArn(topicArn);}
public byte readByte() {return (byte) readUByte();}
public TerminateClientVpnConnectionsResult terminateClientVpnConnections(TerminateClientVpnConnectionsRequest request) {request = beforeClientExecution(request);return executeTerminateClientVpnConnections(request);}
public ReceiveMessageCommand setQueueUrl(String queueUrl) {checkCallable();this.queueUrl = queueUrl;return this;}
public void serialize(LittleEndianOutput out) {out.writeShort(getRowNumber());out.writeShort(getFirstCol() == -1? (short)0 : getFirstCol());out.writeShort(getLastCol() == -1? (short)0 : getLastCol());out.writeShort(getHeight());out.writeShort(getOptimize());out.writeShort(field_6_reserved);out.writeShort(getOptionFlags());out.writeShort(getOptionFlags2());}
public static double common(Output a, Output b) {double d = a.getValue();double c = Math.pow(b.getValue, a);double d1 = Math.pow(b.getValue, a);double d2 = Math.pow(b.getValue, b);return (d1 > d2)? d2 : d1;}
public CreateVariableResult createVariable(CreateVariableRequest request) {request = beforeClientExecution(request);return executeCreateVariable(request);}
public static final int offset(byte[] b, int ptr) {final int sz = b.length;if (ptr == 0)return -1;for (int i = 0; i < sz; i++) {if (b[ptr] == null)return i;}return offset(b, ptr);}
public int fillFields(byte[] data, int offset,EscherRecordFactory recordFactory ){int bytesRemaining = readHeader( data, offset );short propertiesCount = readInstance( data, offset );int pos = offset + 8;EscherPropertyFactory f = new EscherPropertyFactory();properties.clear();properties.addAll( f.createProperties( data, pos, bytesRemaining ) );return bytesRemaining + 8;}
public CreateCloudFrontOriginAccessIdentityResult createCloudFrontOriginAccessIdentity(CreateCloudFrontOriginAccessIdentityRequest request) {request = beforeClientExecution(request);return executeCreateCloudFrontOriginAccessIdentity(request);}
public boolean isNamespaceAware() {return false;}
public void setOverride(boolean on) {override=on;}
public String getClassName() {return getClass().getName();}
public IndexReader getIndexReader() {return indexReader;}
public int indexOfKey(int key) {if (mGarbage) {gc();}return binarySearch(mKeys, 0, mSize, key);}
public BLANKRecord(RecordInputStream in) {field_1_zero          = in.readShort();field_2_first_col = in.readShort();field_3_first_col = in.readShort();field_4_last_col = in.readShort();int field_5_first_col = in.readShort();field_6_first_col = in.readShort();int field_7_first_col = in.readShort();field_8_first_col = in.readShort();field_9_first_col = in.readShort();int field_10_first_col = in.readShort();field_11_first_col = in.readShort();field_12_last_col = in.readShort();}
public long length() {try {return channel.size();} catch (IOException ioe) {throw new RuntimeException("IOException during length(): " + this, ioe);}}
public PasswordRecord(RecordInputStream in) {field_1_password = in.readShort();}
public HashMap(int capacity, double loadFactor) {if (capacity < 0) {throw new IllegalArgumentException("Capacity must be positive (got " + capacity + ")");}if (loadFactor < 0) {throw new IllegalArgumentException("LoadFactor must be positive (got " + loadFactor + ")");}int size = (int) (capacity * loadFactor);HashMap d = new HashMap(size, capacity);d.put(loadFactor, (short) loadFactor, (short) loadFactor, (short);d.put(size, (short) loadFactor, (short) loadFactor);d.put(size, (short) loadFactor, (short) loadFactor);d.put(size, (short) loadFactor, (short) loadFactor);d.put(size, (short) capacity * loadFactor, (short) / capacity);d.put(size, (short) capacity * loadFactor, (short) / capacity);d.put(size, (short) * loadFactor, (short) * capacity);d.put(size, (short) * capacity);d.put(size, (short) * capacity);d.put(size, (short) * load
public void run() {try {mainThread();} catch (InterruptedIOException e) {throw new RuntimeException(e);}}
public DeleteLoginProfileResult deleteLoginProfile(DeleteLoginProfileRequest request) {request = beforeClientExecution(request);return executeDeleteLoginProfile(request);}
public E pollFirst() {return peekFirstImpl();}
public CreateCloudPhotoResult createCloudPhoto(CreateCloudPhotoRequest request) {request = beforeClientExecution(request);return executeCreateCloudPhoto(request);}
public String getName() {return this.resolver.getName();}
public int finishBoundaryCharacters(StringBuilder buffer, int start) {if( start > buffer.length() || start < 1 || start > buffer.length() || start > buffer.length() || start > buffer.length() - end) {return buffer.length() - end;}boundaryChars = new char[end];int index = 0;for(int i=start; i<end; i++) {boundaryChars[index++] = 0;}if (boundaryChars > 0) {boundaryChars[index++] = buffer.length() - start;}return index;}
public void setObjectChecker(ObjectChecker checker) {this.checker = checker;}
public AreaEval(EvaluationArea eval) {_base = eval;}
public CreateVpcEndpointResult createVpcEndpoint(CreateVpcEndpointRequest request) {request = beforeClientExecution(request);return executeCreateVpcEndpoint(request);}
public DeregisterWorkspaceDirectoryResult deregisterWorkspaceDirectory(DeregisterWorkspaceDirectoryRequest request) {request = beforeClientExecution(request);return executeDeregisterWorkspaceDirectory(request);}
public ChartFRTInfoRecord(RecordInputStream in) {field_1_chart_index = in.readUShort();field_2_chart_index = in.readUShort();field_3_sheet_index = in.readUShort();int field_4_string_len = in.readUShort();int field_5_chart_index = in.readUShort();field_6_chart_index_len = in.readUShort();}
public Merger newMerger(Repository db, boolean inCore) {return new OneSide(db, treeIndex);}
public CreateDataSourceFromClusterResult createDataSourceFromCluster(CreateDataSourceFromClusterRequest request) {request = beforeClientExecution(request);return executeCreateDataSourceFromCluster(request);}
public DecisionToDFAArray() {decisionToDFA = null;}
public void removeName(String name) {int idx = indexOfName(name);if (idx!= -1) {names.remove(idx);}}
public String toString(){StringBuilder buffer = new StringBuilder();buffer.append( "[RightMargin]\n" );buffer.append( "   .margin               = " ).append( " (" ).append( getMargin() ).append( " )\n" );buffer.append( "[/RightMargin]\n" );return buffer.toString();}
public DefaultColWidthRecord clone() {return copy();}
public AddProcessorsToPipelineResult addProcessorsToPipeline(AddProcessorsToPipelineRequest request) {request = beforeClientExecution(request);return executeAddProcessorsToPipeline(request);}
public String formatCellReference(CellReference r) {return formatCellReference(r.getFirstRow(), r.getLastRow(), r.getFirstColumn(), r.getLastColumn());}
public ByteBuffer put(int index, byte b) {checkIndex(index);backingArray[offset + index] = b;return this;}
public void setMode(String mode) {mode = mode;}
public ShortBuffer slice() {byteBuffer.limit(limit * SizeOf.SHORT);byteBuffer.position(position * SizeOf.SHORT);ByteBuffer bb = byteBuffer.slice().order(byteBuffer.order());ShortBuffer result = new ShortToByteBufferAdapter(bb);byteBuffer.clear();return result;}
public void set(int index, long value) {final int o = index / 5;final int b = index % 5;final int shift = b * 12;blocks[o] = (blocks[o] & ~(4095L << shift)) | (value << shift);}
public ByteBuffer putFloat(float value) {throw new ReadOnlyBufferException();}
public static double max(double[] values) {double max = Double.POSITIVE_INFINITY;for (double value : values) {max = Math.max(max, value);}return max;}
public CreateWebhookRequest() {super("cr", "2016-06-07", "CreateWebhook", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/webhooks/[WebhookId]");setMethod(MethodType.PUT);}
public DeleteAttributesRequest(String domainName, String itemName, java.util.List<Attribute> attributes) {setDomainName(domainName);setItemName(itemName);setAttributes(attributes);}
public String toFormulaString(String[] operands) {StringBuilder buf = new StringBuilder();if(isExternalFunction()) {buf.append(operands[0]); appendArgs(buf, 1, operands);} else {buf.append(getName());appendArgs(buf, 0, operands);}return buf.toString();}
public boolean wasMergedOrRebase() {return wasMergedOrRebase();}
public void setByteArray(byte[] bytes, int offset, int byteCount) {this.bytes = bytes;this.offset = offset;this.byteCount = byteCount;}
public DescribeConnectionsResult describeConnections(DescribeConnectionsRequest request) {request = beforeClientExecution(request);return executeDescribeConnections(request);}
public DeletePhotosRequest() {super("CloudPhoto", "2017-07-11", "DeletePhotos", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public synchronized boolean add(E object) {if (elementCount == elementData.length) {growByOne();}elementData[elementCount++] = object;modCount++;return true;}
public static ByteBuffer newReadOnlyHeapBuffer(int capacity) {if (capacity < 0) {throw new IllegalArgumentException();}return new ReadOnlyByteBuffer(capacity);}
public SrndQuery getSubQuery(int index) {return subQueries[index];}
public float currentScore(int docId, String field, int start, int end, int numPayloadsSeen, float currentScore, float currentPayloadScore) {if (numPayloadsSeen == 0) {return currentPayloadScore;} else {return Math.max(currentPayloadScore, currentScore);}}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[USESELFS]\n");buffer.append("   .options = ").append(HexDump.shortToHex(_options)).append("\n");buffer.append("[/USESELFS]\n");return buffer.toString();}
public GetLogPatternResult getLogPattern(GetLogPatternRequest request) {request = beforeClientExecution(request);return executeGetLogPattern(request);}
public RegisterTransitGatewayMulticastGroupMembersResult registerTransitGatewayMulticastGroupMembers(RegisterTransitGatewayMulticastGroupMembersRequest request) {request = beforeClientExecution(request);return executeRegisterTransitGatewayMulticastGroupMembers(request);}
public GetPhoneNumberResult getPhoneNumber(GetPhoneNumberRequest request) {request = beforeClientExecution(request);return executeGetPhoneNumber(request);}
public byte[] getData() {return data;}
public final boolean isDirect() {return false;}
public DeleteServerCertificateResult deleteServerCertificate(DeleteServerCertificateRequest request) {request = beforeClientExecution(request);return executeDeleteServerCertificate(request);}
public StringBuilder append(double d) {RealToString.getInstance().appendDouble(this, d);return this;}
public GetEvaluationResult getEvaluation(GetEvaluationRequest request) {request = beforeClientExecution(request);return executeGetEvaluation(request);}
public LinkDataRecord getName() {return _linkDataRecord;}
public boolean lookingAt() {matchFound = lookingAtImpl(address, input, matchOffsets);if (matchFound) {findPos = matchOffsets[1];}return matchFound;}
public GetLifecyclePolicyPreviewResult getLifecyclePolicyPreview(GetLifecyclePolicyPreviewRequest request) {request = beforeClientExecution(request);return executeGetLifecyclePolicyPreview(request);}
public OpenStringBuilderTokenStream create(String word) {super(word);}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_print_headers);}
public String toString() {return super.toString() + ":" + revstr; }
public PushCommand setRemote(String remote) {checkCallable();this.remote = remote;return this;}
public CollapsesRowResult collapsesRow(CollapsesRowRequest request) {request = beforeClientExecution(request);return executeCollapsesRow(request);}
public AssociateSkillGroupWithRoomResult associateSkillGroupWithRoom(AssociateSkillGroupWithRoomRequest request) {request = beforeClientExecution(request);return executeAssociateSkillGroupWithRoom(request);}
public String toString(){StringBuilder buffer = new StringBuilder();buffer.append("[DATALABEXT]\n");buffer.append("   .rt      =").append(HexDump.shortToHex(rt)).append('\n');buffer.append("   .grbitFrt=").append(HexDump.shortToHex(grbitFrt)).append('\n');buffer.append("   .unused  =").append(HexDump.toHex(unused)).append('\n');buffer.append("[/DATALABEXT]\n");return buffer.toString();}
public QueryConfigHandler getQueryConfigHandler() {return queryConfigHandler;}
public String getClassArgument() {return (this.getClass() == null? null : this.getClass().getName());}
