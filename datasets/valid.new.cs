public override void Delete(){this._enclosing.Delete(this.GetPath()));}
public virtual string GetConsoleOutput(){return Console.Error.ToString();}
public virtual DescribeClusterSubnetGroupsResponse DescribeClusterSubnetGroups(DescribeClusterSubnetGroupsResponse describeClusterSubnetGroups){return describeClusterSubnetGroups(describeClusterSubnets);}
public virtual void DeleteLoadBalancerPolicy(string policyId){if (policyId == null){throw new System.ArgumentException("policyId cannot be null");}if (policyId.GetType()!= typeof(LoadBalancerPolicy)){throw new System.ArgumentException("policyType cannot be null");}delete(policyId.GetType());}
public virtual void Checkout(){checkout();}
public array of Indexable fields { get; set; }
public virtual List<ElasticCloud> GetElasticClouds(){return elasticClouds;}
public virtual void put(int v){if (m_len >= m_buf.Length){throw new System.IO.IOException();}m_len += 1;}
public Group next(){return this.nextEntry();}
public override bool contains(object o){return this._enclosing.containsValue(o);}
public override void ReadFormula(Formula formula){if (formula == null) {throw new RecordFormatException("Missing required field (formula is null)"); }else{if (formula.IsFormula){throw new RecordFormatException("Missing required field (formula is a string but got: " + formula.GetName() + ")");}}
public virtual Stemmer GetStem(string @string){return null;}
public virtual void SetInputReader(TextReader input){this.m_input = input;}
public virtual DeploymentDetail GetDeploymentDetail(){return deploymentDetail;}
public virtual void SetDirectory(FilePath dir){this.workingDirectory = dir;}
public virtual void copy(){object[] contents = new object[_size];output.write(to_s);}
public virtual void Skip(IToken token){Skip(token);}}
public virtual ListTagsResponse ListTags(string domain){return ListTags(domain, true);}
public virtual void clear(){list.clear();}
public virtual ISequence<long?> Decode(long? values, int values.Length){return Decode(values, 0, values.Length);}
public Type GetType(){return type;}
public override void Serialize(byte[] buffer, int offset){buffer[offset] = (byte)('\x07');buffer[offset + 4] = (byte)('\x07');buffer[offset + 8] = (byte)('\x07');}
public java.util.MapClass.CharacterFilter mappingFilterFrom(object mapClass){return new java.util.Hashtable<object, object>(mapClass);}
public string GetFederationToken(string appName){return GetFederationToken(appName);}
public virtual FieldTermMap GetFieldTermMap(){return fieldTermMap;}
public void Register(User user){if (user == null){throw new ArgumentNullException("user cannot be null");}if (user.GetRoles().Count == 0){throw new ArgumentException("user with roles cannot be null");}if (user.GetRoles().Count == 0){throw new ArgumentException("user with roles cannot be null");}
public virtual GroupInfo GetGroupInfo(){return Groups[GetGroupId()];}
public virtual StashListCommand StashList(){return new StashListCommand(this);}
public string representation(s) {return string.Format("^{0}
public virtual Object Clone(){return new Object(this._enclosing.Clone());}
public virtual Score GetScore(int index){return scores[index];}
public HindiNormalizationFilter(TokenStream input){return new HindiNormalizationFilter(input);}
public AnalyzerInstance(Type analyzerType){return new AnalyzerInstance(analyzerType);}
public virtual VersioningInfo GetSegmentVersions(){return Segment.GetSegmentVersions();}
public virtual void Update(){api = this.GetApi();}
public virtual TagPhoto TagPhoto(){return TagPhoto(this);}
public Fleet(string name, Fleet options){return new Fleet(name, options);}
public virtual void Fill(int field_1_count, int value1){if (field_1_count < 0){throw new Exception("Invalid field number (0x" + value1 + ")");}if (field_1_count < 0){throw new Exception("Invalid field number (0x" + field_1_count + ")");}
public void SetReserved(int v){reserved = v;}
public virtual void write(string @string){write(m_littleEndian, 0, @string.Length);}
public virtual void sort(int start, int end){for (int i = start; i < end; i++){if (range[i] < start){start = start;break;}if (range[i] > end){break;}}if (start < end){return;}int start = end - 1;int end = start + 1;int cmp = range[i] - start;if (cmp < 0){throw new Exception("Invalid start or end: " + str[start] + " and " + end + " are not supported.");}int cmp_1 = (int)range[i] - start;if (cmp_1 < 0){throw new Exception("Invalid start or end range: " + start + " and "
public virtual GetLibraryRequest Create(LibraryRequest request){var options = new InvokeOptions();options.RequestMarshaller = GetLibraryRequestMarshaller.Instance;options.ResponseUnmarshaller = GetLibraryResponseUnmarshaller.Instance;return Create(request, options);}
public ViewDefinitionRecord(RecordInputStream in1){return ReadViewDefinitionRecord(in1);}
public virtual void Recycle(){bytesUsed = 0;}
public virtual void resize(int size_1){resize(size_1);}
public void SetUserData(IDataUser data){this.userData = data;}
public override void Serialize(FieldOutput out1){out1.WriteByte(field_1_precision);}
public void SetCookieStickinessPolicyName(string cookieStickinessPolicyName){this.SetCookieName(cookieStickinessPolicyName);}
public virtual void putChar(char ch){buffer.putChar(ch);}
public int indexOf(char c){if (c == 0){return 0;}return indexof(c, 1);}
public RecordSID(byte b) {return ReadRecord(b, 0); }
public override bool IsPortugueseStemmer(){return o is a PortugueseStemmer;}
public virtual void DeleteKnownHostKeys(string appID){DeleteKnownHostKeys(appID, appID);}
public int compareRemaining(int remaining){return compare(remaining, this.remaining);}
public virtual void setValueAt(int index, V value){if (index < 0 || index >= values.Length){throw new IndexOutOfRangeException(index.ToString());}else if (index < 0){throw new IndexOutOfRangeException(index.ToString());}this._value = value;}
public virtual void Update(){endpoint = this.CreateEndpoint();}
public void DisassociateIAMProfile(string profileId){this.hm.Disassociate(profileId);}
public pictureQuery(PictureQueryRequest request){this.request = request;}
public TimeConstraint(){return new TimeConstraintAnonymousInnerClassHelper(this);}
public virtual int read(byte[] buffer, int offset, int length){throw new System.NotImplementedException();}
public java.util.NavigableSet<E>(){return this._enclosing.descendingOrder(this.elements);}
public ConcurrentRefUpdateException(string message, int returnCode){throw new System.NotImplementedException();}
public Trial(int trial){this.trial = trial;}
public virtual int available(){return _buf.remaining();}
public void Write(byte[] b){Write(b, 0, b.Length);}
public virtual void remove(){lastChild = null;}
public CloudFrontOriginAccessIdentityRequest(OriginAccessIdentityConfig config){return new CloudFrontOriginAccessIdentityRequest(config);}
public NameXPtg for the given name and sheetRefIndex. eturn NameXPtg for the given name and sheetRefIndex.
public virtual IRuleInvocationStack GetRuleInvocationStack(){return this._ruleInvocationStack;}
public HindiNormalizationFilterFactory(INormalizationParameterCollection parameters){return new HindiNormalizationFilterFactory(parameters);}
public virtual void Start(){startFleet(null, 0, 0);}
public virtual GatewayInformation GetGatewayInformation(){return gatewayInformation;}
public virtual NGit.BatchStepResponse JobFlowStep(string request){var options = new InvokeOptions();options.RequestMarshaller = JobFlowStepRequestMarshaller.Instance;options.ResponseUnmarshaller = JobFlowStepResponseUnmarshaller.Instance;return Invoke<NGit.BatchStepResponse>(request, options);}
public override void checkCodecCompatibility(Codec codec){if (codec.hasMagic){throw new System.IO.IOException("Codec has no magic");}if (codec.level <= ErrorConstants.LUCENE_CURRENT){throw new java.io.IOException("Codec has no level of "+ ErrorConstants.LUCENE_CURRENT);}if (codec.flags & FLAG_FINALIZE_CODEC){throw new java.io.IOException("Codec has been invalidated.");}
public int Read(BytesRef buffer, int offset, length) {return _buf.ReadInt(offset, length);}
public virtual void SetExpireTime(int expire){this.expire = expire;}
public static double[] GetIrrFunction(double[] doubles){return doubles.GetIrrFunction(double[])}
public Format GetAcceptFormat(){return format;}
public virtual string listNext(){return java.util.TreeMap<string, object>.EMPTY_STRING;}
public override void write(float value){throw new System.NotImplementedException();}
public DhcpOptionsRequest(List<DhcpConfiguration> dhcpConfigurations){return new DhcpOptionsRequest(dhcpConfigurations);}
public virtual AmazonGuardDutyAssessmentReport GetGuardDutyAssessmentReport(){return null;}
public override string ToString(){return this.ToString(CultureInfo.InvariantCulture);}
public virtual int size(){return elements.Length;}
public virtual void Delete(){this.lag = null;}
public override void Decompress(BytesRef input){this.Decompress(input);}
public ASCIIFoldingFilterFactory(){return new ASCIIFoldingFilterFactory(this);}
public virtual void SetMaxChange(int max){max = max;}
public virtual void insert(char c){if (m_len >= m_buf.Length){throw new java.lang.StringIndexOutOfBoundsException("too many characters in StringBuffer");}int pos = m_pos + 1;m_buf[pos] = c;m_pos++;m_len++;}
public virtual void Destroy(){this._enclosing.Destroy();}
public HunspellRecord(){return this;}
public HashSet<T>(){return new HashSet<T>(this._enclosing);}
public virtual void setText(string text){this.m_text = text;}
public void write(bool val){throw new System.NotImplementedException();}
public virtual int floor(int x){return Math.Floor(x / 100);}
public virtual IList<MailboxPermission> GetMailboxPermissions(){return mailboxPermissions;}
public E get(int index){E e = list[index];if (e == null){return e;}return e[index];}
public virtual void Split(int shardIndex){if (shardIndex < 0){throw new ArgumentException("Invalid shard index (" + shardIndex + ").");}if (shardIndex >= shards.Count){throw new ArgumentException("Invalid shard index (" + shardIndex + ").");}
public virtual List<PolicyAttachment> GetPolicyAttachments(){return PolicyAttachment.GetList(this);}
public override long Length(){return data.Length;}
public virtual void incRef(V value){count++;}
public NGit.Revwalk.Filter.NotRevFilter clone(){return this;}
public virtual void close(){parseListener = null;}
public virtual void Delete(){this._enclosing.DeleteRule(this.ruleId);}
public virtual IList<Link> GetLinks(){return links;}
public String ResolveName(){return ResolveName(null);}
public int AddDrawingGroupId(int gid){return gid;}
public virtual ResetInstanceAttributeResponse ResetInstanceAttribute(ResetInstanceAttributeRequest request){var options = new InvokeOptions();options.RequestMarshaller = ResetInstanceAttributeRequestMarshaller.Instance;options.ResponseUnmarshaller = ResetInstanceAttributeResponseUnmarshaller.Instance;return Invoke<ResetInstanceAttributeResponse>(request, options);}
public virtual DescribeObjectResponse DescribeObject(DescribeObjectResponse describeObject){return DescribeObject(describeObject);}
public virtual SignatureComposer GetSignatureComposer(){return composer;}
public IdentityProviderConfiguration Information(string identityProviderName){return null;}
public void setCoords(long[] coordinates){for (int i = 0; i < coordinates.Length; i++){coordinates[i] = coordinates[i];}}
public virtual RoomInfo GetRoomInfo(){return information;}
public CatLabRecord(RecordInputStream in1){return new CatLabRecord(in1);}
public override void putShort(short value){throw new System.NotImplementedException();}
public virtual void Add(string name){this.m_name = name;}
public virtual bool IsWhiteSpace(char ch){return false;}
public override IList AvailablePresets(){return presets;}
public virtual string ToString(){return this.GetName().Intern();}
public virtual string ToString(){return "this@" + this;}
public virtual void SetJoinDocFreqValueSource(float q){m_joinDocFreq = q;}
public virtual AccountResponse Account(AccountRequest request){var options = new InvokeOptions();options.RequestMarshaller = AccountRequestMarshaller.Instance;options.ResponseUnmarshaller = AccountResponseUnmarshaller.Instance;return Invoke<AccountResponse>(request, options);}
public short? GetShortValue(IValueHolder holder){return value;}
public int size(){return map.size();}
public string ToString(bool brief = false){return ToString(brief? "true" : "false");}
public DeleteCloudFrontOriginAccessIdentityRequest(string id, string ifMatch, CloudFrontOriginAccessIdentityRequest parameters){var request = new DeleteCloudFrontOriginAccessIdentityRequest(id, ifMatch, parameters);request.params.id = id;request.ifMatch = ifMatch;return request;}
public AuthorizationRequest(string namespace){var request = new AuthorizationRequest(namespace);request.Namespace = namespace;return request;}
public virtual IndexSearcher GetIndexSearcher(){return _indexSearcher;}
public override SeekTo(Object target){return target.SeekTo(this.origin);}
public virtual int getIndexVersion(){return version;}
public int ReadByte(){return _buf[_ReadIndex++];}
public List<RkRec> Parse(RecordInputStream in1){try{return Parse(in1, null);}catch (IOException e){throw new RuntimeException(e);}}
public virtual void SendSelfNotifications(){}
public virtual void Delete(){_trafficPolicy = null;}
public virtual void Invite(InviteMember[] members){foreach (InviteMember member in members){invite(member);}}
public override string ToString(){return "Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter Four - letter" + "
public int GetSheetIndex(int sheetIndex){return _externSheetIndex;}
public virtual int GetResultTreeId(){return treeId;}
public virtual DescribeJobOutputResponse DescribeJobOutput(DescribeJobOutputResponse result){return result.DescribeJobOutput(this);}
public string representation(suggestions) of the text.
public virtual bool DetectStackSetDrift(int stackFrame){return false;}
public void ReadForHideArrayItem(){if (HIDE_ARRAY_ITEM_IN_FOCUSED_BLOCK){CheckForDisposed(true);}if (HIDE_ARRAY_ITEM_IN_FOCUSED_BLOCK){CheckForDisposed(false);}}
public ModifyJumpserverPasswordRequest(string password){return ModifyJumpserverPasswordRequest(password);}
public override void Initialize(){base.Initialize();}}
public virtual void channelChanged(){lexer.channelChanged();}}
public override int get(int index){if (index < 0){throw new IndexOutOfRangeException(index);}return -1;}
public virtual RefMap Get(){return this._enclosing.GetMap();}
public void setValue(E value){lock (this){object[] a = elements;if (a!= null){object[] b = a.Clone();a.CopyTo(b, 0, b);b = a[i];}}}
public int Get(byte[] array, int pointer){return Get(array, pointer, 3);}
public virtual void close(){if (this.lastException == null){throw new System.InvalidOperationException("Last error has occurred.");}lastException = this.lastException.GetHashCode();}
public ICollection<E> GetSortedElements(int startUpTo, int startDownTo){return new List<E>(startUpTo, startDownTo, startUpTo);}
public virtual void cacheResultErrorCode(){lastError = -1;}
public int size(){return this._enclosing._size;}
public void DeleteTerminationCredentials(){_terminationCredentials = null;}
public virtual void Skip(int count){Skip(count * libcore.io.SizeOf.BYTES);}
public virtual void AttachVPNGateway(VPNGateway gateway){for (int i = 0; i < _vpnGatewayTable.Length; i++){_vpnGatewayTable[i] = gateway;}}
public virtual void Update(string name){this.name = name;}
public Hunspell stem filter(TokenStream st1){return new Hunspellstemfilter(st1);}
public virtual PromoteReadReplicaToReadReplica(PromoteReadReplicaToReadReplica other){return PromoteReadReplicaToReadReplica(other);}
public void close(){_rc4.dispose ();_rc4 = null;}
public override string ToString(){return ruleName;}
public virtual void SetFieldsData(object field){this._fieldsData = field;}
public virtual bool IsDiffEntry(EntryCt mode){return mode!= rawMode;}
public virtual void putShortArray(short[] array){if (shortArrays == null){throw new System.NotImplementedException();}shortArrays[offset] = array;offset++;}
public void Update(GCMChannel channel){Update(channel);}
public CollectionRequest(CollectionRequest request){this._request = request;}
public virtual Object Clone(){return this;}
public void EnablePropagation(bool enable){disable = enable;}
public virtual void diff(){lock (mutex){return;}}
public SettingsUpdate(App app){return settings;}
public virtual void rm(){if (this.exitValue == 0){throw new InvalidOperationException("Exit code is not valid.");}this.exitValue = this.exec(this.exitValue);}
public virtual string ToString(){return "this@" + this;}
public virtual IInvocationStack GetInvocationStack(RuleContext ruleContext){return null;}
public virtual ICollection<License> GetLinkFacets(LinkFaceLinkFaceType type){return licensesForLinkFaceType.Values;}
public virtual OutputStream Create(StreamOutput out1){try{@out1.SetEncoding(m_enclosing.GetEncoding()) || (@out1.SetEncoding()) || (@out1.SetEncoding()) || (@out1.SetEncoding()) || (@out1.SetEncoding()) || (@out1.SetEncoding()) || (@out1.SetRaw(false)));return out1;}catch (IOException e){throw new RuntimeException(e);}
public ModifySnapshotScheduleForCluster(){return ModifySnapshotScheduleForClusterInternal(null, true);}
public virtual TreewalkTrie Reduce(TreewalkFunction reduceFunction){return this;}
public override void Decode(int n){if (n < 1){throw new System.ArgumentException("Bad number of bytes: " + n);}if (n > 1){throw new System.ArgumentException("Bad number of bytes: " + n);}
public override void CopyValue(MutableValueDouble value){if (this.value == POIFS_MUTABLE_SINGLE_VALUE_DOUBLE){this.value = value.CopyValue(this);}else{this.value = value;}this.isMandatory = true;}
public void Disassociate(string fleetId){fleetId = disassociate(fleetId);}
public override int Read(int fieldIndex){return _complexLexer.ReadInt32(fieldIndex);}
public override FuzzySet GetMaxDoc(){return new FuzzySet(maxDoc);}
public void DeleteDeviceGroup(){deleteDeviceGroup(null);}
public virtual void push(object o){push(o);}}
public virtual bool isEmpty(){return countMap.isEmpty();}
public void Reset(bool b){reset(b? "true" : "false");}
public override bool ToBoolean(){return false;}
public virtual E pop(){if (this.e == null){throw new ArgumentException("this element cannot be null");}return this.e = this.popInternal(this.e.getType().GetProperty("default"));}
public virtual IList<Healthcheck> GetHealthchecks(){return null;}
public new capacity provider(byte capacity){return new _capacityProvider(this, capacity);}
public java.io.PrintArea getPrintArea(ISheet sheet){return printArea;}
public virtual TerminationHealth GetTerminationHealth(){return TerminationHealth.UNKNOWN;}
public E remove(E @object){lock (this._enclosing){int oldSize = this._enclosing._size;this._enclosing.remove(@object);return this._enclosing._first[oldSize];}}
public virtual List<string> ListAll(){return List<string>(template.Versions).ToList();}
public virtual ListClusterSecurityGroupsResponse ListClusterSecurityGroups(string clusterName){return ListClusterSecurityGroups(clusterName, clusterName);}
public void SetPrintArea(IPrintArea area){this.m_printArea = area;}
public virtual IList<CreditSpecification> GetCreditSpecifications(){return creditSpecifications;}
public virtual IList<Job> GetJobsForWorkteam(Workteam job){return null;}
public virtual void Reinitialize(){this._enclosing.Reinitialize();}
public virtual int next(){return this.type;}
public override void Cancel(){base.Cancel();}}
public virtual void append(char ch){append0(ch);}
public string ProductName { get; set; }
public override object Encode(byte b){throw new System.NotImplementedException();}
public virtual void DeleteLaunchTemplates(){string launchTemplateName = GetType().FullName;string deleteSource = GetType().FullName;if (deleteSource!= null){try{deleteSource.GetString(launchTemplateName);}finally{if (deleteSource!= null){return;}}}
public virtual TokenizerFactory GetTokenizerFactory(){return null;}
public virtual IList<reservedCacheNodeOffer>(){return reservedCacheNodeOfferings.Values;}
public virtual string ToString(){return "Vocabulary(" + m_vocabulary + ")";}
public PropertyTable(HeaderBlock headerBlock){return new PropertyTable(headerBlock);}
public StreamSummary GetSummary(){return StreamSummary.Create(this.StreamName);}
public string Format(double value){return value.ToString(CultureInfo.InvariantCulture);}
public SeriesRecord GetSeriesRecord(){return series;}
public virtual void AddMerge(Merge merge){this.Merges.Add(merge);}
public virtual void write(){out1.write(m_attributeName);out1.write(m_value);}
public virtual void Start(){Start(null);}
public virtual void Delete(){Delete (null, repo.Resolve());}
public virtual void put(double[] doubles){if (doubleBuffer.isBuffer(backingArray)){java.util.Arrays.putDouble(backingArray, 0, backingArray.Length);}java.util.Arrays.checkOffsetAndCount(backingArray, 0, backingArray.Length);}
public virtual int GetLength(FilePath file){throw new NotSupportedException(MessageFormat.Format(JGitText.Get().isAFileHasNoBasename, file));}
public void Initialize(E value){this.value = value;}
public virtual void ClearTrackingOptions(){this._enclosing.ClearTrackingOptions();}
public static SpotDataFeedSubscription Info(string subscriptionName){return new SpotDataFeedSubscriptionInfo(subscriptionName);}
public override void SetDirectory(DirectoryInfo dir){this._directory = dir;}
public virtual bool Equals(PrefixQuery query){return query.Equals(this);}
public void WriteUShort(int v){WriteUShort(v);}
public virtual InformationalGateway List<InformationalGateway>(IEnumerable<InformationalGateway> customHeaders = null, CancellationToken cancellationToken = default(CancellationToken)){return customHeaders;}
public virtual DiffCommand CreateDiffCommand(){return new DiffCommand(this);}
public virtual GetAlbumsByNameResponse GetAlbumsByName(GetAlbumsByNameRequest request){var options = new InvokeOptions();options.RequestMarshaller = GetAlbumsByNameRequestMarshaller.Instance;options.ResponseUnmarshaller = GetAlbumsByNameResponseUnmarshaller.Instance;return Invoke<GetAlbumsByNameResponse>(request, options);}
public Rule Rule(string suffix, string replacement){return new Rule(this, suffix, replacement);}
public virtual int read(int len){throw new System.NotImplementedException();}
public virtual Information about a document. public virtual Information about(DocumentClassifier doc){return null;}
public virtual void A(int n){if (_isV){_isV = true;}else{_isV = false;}
public virtual bool isEmpty(){lock (mutex){return c.isEmpty();}}
public override void Read(Record record){if (record.BackupId!= backupId){throw new InvalidOperationException("Invalid backup id (" + backupId + ").");}this.ReadEntry(record);}
public override bool contains(object o){return this._enclosing.containsKey(o);}
public UserInfo Create(string username){return new UserInfo(username,'')}
public virtual void remove(E e){throw new System.NotSupportedException("filter.remove() cannot be called with a null object" );}
public virtual int GetOverhead(){return 0;}
public override CherryPickResult GetCherryPickStatus(){return this._enclosing.GetCherryPickStatus();}
public string Sign(string @string, string key, string secret){return Sign(@string, key, secret);}
public virtual void setResult(V result){this._enclosing.result = result;}
public AnalyzerProcessing(){return new AnalyzerProcessing(this);}
public GermanStemFilter Create(TokenStream tokenStream){return new GermanStemFilter(tokenStream);}
public java.util.regex.MatcherOptions set(string value){this.value = value;this.operator = operator;}
public void setValue(V value){_value = value;}
public sending options { get; internal set; }
public string domain and item names. eturn domain and item names.
public virtual char[] read(char[] chars, int offset, int length){throw new System.NotImplementedException();}
public virtual bool print(bool b){return b? "true" : "false";}
public override void SeekTo(int term){term = 1 << (term & 0x07);}
public Token Create(Type type, string text){return new Token(type, text);}
public override double Value(){return EvaluateMin(this, double.NaN);}
public java.nio.ByteBuffer slice(){return byteBuffer.slice();}
public override void SetReadlimit(int readlimit){throw new System.NotImplementedException();}
public PatternReplaceCharFilterFactory(IList<string> args){return new PatternReplaceCharFilterFactory(args);}
public bool shouldCreateCells(){return false;}
public void Remove(){list.Remove(this);}
public override void Serialize(byte[] buffer, int offset, int length){buffer[offset] = (byte)0;}
public override void putDouble(double value){if (double.IsNaN(value)){throw new System.ArgumentException("value is a NaN value in the buffer");}if (double.IsInfinity(value)){throw new java.util.IndexOutOfBoundsException("value is a NaN value in the buffer");}double d = double.Parse(value);buffer.position = d;buffer.notifyDataSetChanged();}
public VoiceConnectorGroup Information(VoiceConnectorGroup group){var options = new InvokeOptions();options.RequestMarshaller = VoiceConnectorGroupRequestMarshaller.Instance;options.ResponseUnmarshaller = VoiceConnectorGroupResponseUnmarshaller.Instance;return Invoke<VoiceConnectorGroupResponse>(group, options);}
public override bool ShouldExpire(){return session.ShouldExpire();}
public virtual void setKeyProgressIncrement(){keyProgress = 1;}
public int ReadByte(){return _buf[_ReadIndex++];}
public virtual string ToString(){return "this " + this.phase + " " + this.m_initial;}
public virtual void DeleteVolume(string volumeId){DeleteVolumeRequest(volumeId, null);}
public Analyzer SetAnalyzer(Analyzer analyzer){this.analyzer = analyzer;}
public virtual IList<string> GetGitHubAccountTokenNames(){return GitHubAccountTokenNames.ToList();}
public override void Serialize(byte[] b, int offset){byte[] s = b;int n = this._enclosing.Serialize(b, offset + _le.Length);for (int i = offset; i < n; i++){s[i] = (byte)_enclosing[i];}}
public SelectionRecord Create(int recordIndex){return new SelectionRecord(recordIndex);}
public virtual void Update(XSchema schema){this.xSchema = schema;}
public java.nio.ShortBuffer get(byte order){return new java.nio.ReadWriteShortBuffer(this);}
public virtual void CreateAlias(){this.n = 1;}
public override void SeekTo(int term){term = 1 << (term & 0x07);}
public virtual void Delete(){_ipSet = null;}
public void Reset(bool b){reset(b? "true" : "false");}
public virtual java.nio.charset.CodingErrorAction getErrorAction(){return new java.nio.charset.CodingErrorAction(this);}
public override long GetBytesUsed(){return delegate1.GetBytesUsed()? delegate1.GetBytesUsed() : 0;}
public virtual void Start(){startTranslationJob();}
public Expression Evaluate(int row, int column){return Evaluate(row, column);}
public Format[] GetAvailableFormats(){return new Format[]{formatName, formatVersion}
public virtual void FV(){if (this.r!= null){try{System.Diagnostics.Debug.Assert(this.r.InnerFunctions!= null);this.r.AddHelper(this.h);}}catch (Exception e){throw new Exception(e.ToString(), e);}}
public virtual int eval(int arg){if (arg < 0){throw new ArgumentException("arg must be a positive integer");}int r = int.Parse(arg, CultureInfo.InvariantCulture);if (r < 0){throw new EvaluationException(arg.ToString());}int b = int.Parse(r, CultureInfo.InvariantCulture);if (b < 0){throw new EvaluationException(arg.ToString());}int d = arg / 100;int b_weight = b * b;if (c < d){throw new EvaluationException(arg.ToString());}if (c > d){throw new EvaluationException(arg.ToString(), CultureInfo.InvariantCulture, d);}int arg_1 = (int)c;if (b < 0){throw new EvaluationException(arg.ToString(), CultureInfo.InvariantCulture, d);}
public virtual IList<Tag> GetTags(){return new List<Tag>(tag_2_value) { };}
public override TokenFilter Lookup(string name){return LookupFactory(name);}
public override void Serialize(Stream out1){out1.Write(GetRange(out1).Range);out1.Write(GetExtraData(out1).ExtraData);}
public HyphenationCompoundWordTokenFilter(){return new HyphenationCompoundWordTokenFilter(this);}
public Builder<T> Add(T value){return this;}
public virtual void IncrementToken(){this.IncrementToken();}}
public void ModifyClusterIamRoles(string role){_role = role;}
public virtual void resize(int size_1){resize(size_1);}
public Credentials GetCredentials(){return credentials;}
public UnicodeString AddUnicodeString(UnicodeString str){chars.Add(str);return str;}
public void Update(){this._enclosing.RefreshExpGroupMetadata(this.ConfigurationInfo.PartitionKey, this._enclosing.ReadEntryAndObjectId);}
public virtual void Delete(){this._enclosing.DeleteChildNode(this.nodeGroup);}
public string GetSessionUrl(string sessionId){return sessionUrl;}
public virtual IToken consume(){return this.next_token;}
public TokenCollector(int tokensPerToken){Initialize(tokensPerToken, tokensPerToken * _numberOfTokensPerToken);}
public void SetHorizBorder(int border){horizontalBorder = border;}
public override TermInfo GetTermInfo(string term){return TermInfo.GetTermInfo(term);}
public virtual VoiceConnectorLoggingConfiguration GetLoggingConfig(){return VoiceConnectorLoggingConfiguration.Default;}
public java.nio.DoubleBuffer get(byte order){return new java.nio.ReadWriteDoubleBuffer(byte order);}
public virtual string ToString(){return "Vocabulary(" + m_vocabulary + ")";}
public void set_width_height(){set_height(_w[0]);}
public virtual short[] getShort(){return get(0, _limit);}
public VoiceConnectorGroup Update(ConnectorGroup group){var options = new InvokeOptions();options.ConnectorGroup = group;options.ResponseUnmarshaller = Update;return Invoke<VoiceConnectorGroup>(group, options);}
public virtual void SetBinaryFileThreshold(double threshold){binaryFileThreshold = threshold;}
public virtual void SetData(byte[] data){_data = data;}
public TreeFilter(ICollection<string> strings){return new OrTreeFilter(strings);}
public string ToString(bool strict){return "true";}
public virtual void putChar(char ch){if (ch < 0){throw new System.ArgumentException("ch < 0> is not a valid char code point");}buffer[index] = ch;}
public override String ToString(){return "SeriesToChartGroup(" + m_seriesToChartGroup + ")";}
public virtual NGit.Text.RichTextString create(IDictionary<string, string> args){return new NGit.Text.RichTextString(args);}
public virtual void status(){status = true;}
public string ToDFA(DFA input){return DFA.ToDFA(input);}
public virtual void DetachTypedLink(TypedLink t){if (t == null){throw new ArgumentException("t is a required property and must not be null");}this.t = t;this.e = t.GetProperty<ITypedPath>();}
public virtual void Reboot(){Sharpen.Reboot(this.m_ctx);}
public virtual void setResult(V result){this._enclosing.result = result;}
public java.nio.CharBuffer getCharBuffer(){return new java.nio.ReadWriteCharArrayCharBuffer(this);}
public virtual void detect(){for (int i = 0; i < _entities.Length; i++){detect(i);}}
public FacetsConfig SetFacets(FacetsConfig config){this._facetsConfig = config;}
public override void Delete(){this.enclosing.DeleteInterface(this);}
public virtual void UpdatePhoneNumbers(){phoneNumbers = GetPhoneNumbersFromPhoneNumber() ;}
public virtual void UnarchivesAmazonGuardDutyFindings(string detectorId){UnarchivesAmazonGuardDutyFindings(detectorId);}
public virtual int size(){return elements.Length;}
public override string ToHexString(){return ToHexString(this);}
public static GalicianStemFilter Create(TokenStream input){return new GalicianStemFilter(input);}
public virtual int GetDepth(){return this.depth;}
public Identity Principal(IIdentity identity){return identity;}
public virtual string ToString(){return "this@" + this.luceneIndex;}
public Rule Rule { get; set; }
public override string ToString(){return this.operator.ToString();}
public OrQuery(SreaQuery query){this.q = query;}
public virtual void SetTags(string[] tags){for (int i = 0; i < tags.Length; i++){tags[i] = Tags[i];}}
public virtual bool SetFollowFileNames(bool follow){follow = follow;}
public static BulkOperationPackedSingleBlock Construct(int bitsPerValue, BulkOperation packedSingleBlock){return new BulkOperationPackedSingleBlock(bitsPerValue, packedSingleBlock);}
public void Write(byte b){try{BeginWrite();dst.Write(b) ;}finally{EndWrite();}}
public virtual bool contains(char[] range){return false;}
public virtual int GetOldId(){return oldId;}
public virtual void Update(){this._enclosing.UpdateRuleMetadata(this.ruleMetadata);}
public virtual K getKey(K key){return this.enclosing.get(key);}
public virtual void write(){throw new System.NotImplementedException();}
public virtual ConferencePreference GetConferencePreference(){return conferencePreference;}
public FieldsQueryList(){return new FieldsQueryList(this);}
public SheetName FormatSheetName(){return sheetName;}
public virtual void Stop(){halt();}
public virtual void RemoveBreak(){m_breakTable.Remove(m_breakIndex);}
public int size(){return map.size();}
public SendMessageRequest(object message){var request = new SendMessageRequest();request.SetDefault(defaultParameters);return request;}
public virtual string ToString(){return "this@" + this;}
public override object GetBatchPrediction(){return batchPrediction;}
public DeleteGroupRequest(GroupRequest request){var options = new InvokeOptions();options.RequestMarshaller = DeleteGroupRequestMarshaller.Instance;options.ResponseUnmarshaller = DeleteGroupRequestUnmarshaller.Instance;return Invoke<DeleteGroupRequest>(request, options);}
public static NGit.Util.FileSystem Create(FileSystem fileSystem){return new NGit.Util.FileSystem(fileSystem);}
public virtual void Update(){this._enclosing.Update(null);}
public java.util.MapClass.Entry<K, V>(){return this;}
public virtual DescribeApplicationsResponse DescribeApplications(){return DescribeApplications(null);}
public void Reset(int point){java.util.Iterator<E> it = this.reset(point);}
public String ToString(){return "this";}
public virtual object GetDeltaBase(){return delta;}
public virtual IList<OptionGroup> GetOptionGroups(){return optionGroups;}
public byte[] ToByteArray(){return array.ToArray();}
public VoiceConnectorProxy CreateOrUpdateVoiceConnectorProxy(Connector proxy){var options = new InvokeOptions();options.RequestMarshaller = CreateVoiceConnectorProxyMarshaller.Instance;options.ResponseUnmarshaller = UpdateVoiceConnectorProxyResponseUnmarshaller.Instance;return Invoke<ConnectorProxy>(proxy, options);}
public Permissions Permissions(data source){return Permissions(data.GetPermission(dataSource));}
public override bool contains(object o){return this._enclosing.containsValue(o);}
public override long Seek(long position){throw new System.NotImplementedException();}
public virtual void SetWriter(TextWriter writer){this.writer = writer;}
public override SpanTermQuery With(Term query){return new SpanTermQuery(query);}
public GalicianMinimalStemFilter(){return new GalicianMinimalStemFilter(this);}
public override bool Equals(object @object){lock (this){return @object == null;}}
public override object GetPasswordData(string password){return base.GetPasswordData(password);}
public bool SubTotal(int row, int column){return true if (row == column){return true;}return false;}
public void DisassociateGroup(Group group){if (group.IsGroup){return;}group.IsGroup = false;}
public virtual DescribeDBSnapshotsResponse DescribeDBSnapshots(DescribeDBSnapshotsRequest request){var options = new InvokeOptions();options.RequestMarshaller = DescribeDBSnapshotsRequestMarshaller.Instance;options.ResponseUnmarshaller = DescribeDBSnapshotsResponseUnmarshaller.Instance;return Invoke<DescribeDBSnapshotsResponse>(request, options);}
public virtual DescribeJobFlowsResponse DescribeJobFlows(){return DescribeJobFlows(null, null, null);}
public virtual void DecrementRefCount(){this.decRefCount();}
public virtual NGit.Book.BookDeclareName SetName(){return this;}
public virtual void Modifies(LaunchTemplate launchTemplate){this.launchTemplate = launchTemplate;}
public virtual List<Device> listdevices(){return listdevices(new DeviceFilter(this));}
public virtual int nextToken(){return lex.next();}
public Search(int start, int end){return index >= 0 && index < end? -1 : 0;}
public virtual void Destroy(){this._enclosing.Destroy();this.m_root.Destroy();this.m_fileSystem = null;}
public QueryBook(QueryBook query){this.query = query;}
public Orientation GetOrientation(int value){return orientation;}
public virtual IEventStream GetEventStream(int stream){return stream;}
public virtual IList<IParseTree> FindAll(IParseTree tree){return FindAll(tree.StartAt, tree.EndAt);}
public virtual bool ContainsChart(){return true;}
public virtual void setValueAt(int index, V value){if (index < 0 || index >= values.Length){throw new IndexOutOfRangeException(index.ToString());}else if (index < 0){throw new IndexOutOfRangeException(index.ToString());}this._value = value;}
public void Set(V value){if (value == null){throw new ArgumentNullException("value is a required property and must be set before making this call.");}this._value = value;}
public virtual void SetWorkingDirectoryMessage(string message){this.workingDirectoryMessage = message;}
public virtual void Clear(){countMap.Clear();}
public int absolute(int row, int column){return row * column;}
public virtual NGit.Api.LsPathFilter SetPathFilter(NGit.Api.LsPathFilter pathFilter){this.pathFilter = pathFilter;}
public virtual void remove(string name){throw new System.NotImplementedException();}
public double Evaluate(double value){return value;}
public string ReplaceSuffix(string replacement){return replacement;}
public override bool IsSidAComponentRecord(SID sid){return false;}
public virtual void open(Stream stream){throw new System.NotImplementedException();}
public int get(long offset){return backingArray[offset];}
public virtual int ReadMore(){return _lei.ReadMore();}
public virtual StepDetail GetStepDetail(int step){return StepDetail(step);}
public virtual void DeleteBatch(string queueName){var request = GetQueueRequest(queueName);request.Delete(requestId, true);}
public virtual bool wasEscaped(char ch){return ch == '\\';}
public override String GetFontName(){return fontName;}
public UnicodeString PutCompressedUnicodeString(Output out1){return putCompressedUnicodeString(out1);}
public virtual void DetectModerationLabels(){Detects moderation labels.
public bool Get(int index){return _bitMap[index];}
public override BytesRef Convert(Int32sRef intsRef){return new BytesRef(intsRef.Value);}
public virtual string ToString(){return "this@" + this;}
public void VisitAll(Record[] records){VisitAll(records, 0, records.Length);}
public override List<AdjustmentType> GetAdjustments(){return adjustmentTypes;}
public override void Open(){this.m_writer = null;}
public void CopyImage(File image){this.CopyFile(image.GetPath());}
public virtual BlendedTermQuery Build(){return new BlendedTermQuery(this.FieldValues);}
public virtual void Register(List<Instance> instances){foreach (Instance instance in instances){Register(instance);}}
public virtual void SetAppResolver(AppResolver resolver){this.resolver = resolver;}
public override bool IsPrecompiled(RuleContext ruleContext){return true if ruleContext is PrecompiledRule;}
public LexerAtnDecisionConfiguration Set(LexerAtnDecisionConfiguration config){this.configuration = config;}
public AvailabilityZone(AvailabilityZone zone){_availabilityZone = zone;}
public virtual IList<LevelEncryptionConfig> GetAppLevelEncryptionConfigs(){return null;}
public virtual void remove(){lock (this._enclosing){int oldSize = this._enclosing._size;this._enclosing.remove(this.m_object);this._enclosing.notifyDataSetRemoved();}}
public override void ReadRawData(RecordInputStream in1){in1.ReadByte();}
public string representation(Automaton automaton){return automaton.ToString();}
public Model { get; set; }
public WorkGroup(string name, List<string> args){throw new ArgumentException("name cannot be null or empty.");}
public IChartSubstreamRecordAggregate Create(RecordStream stream){return Create(stream, null);}
public virtual IList<TrafficMirrorSession> GetTrafficMirrorSessions(){return new List<TrafficMirrorSession>(this._enclosing.GetTrafficMirrorSessions());}
public virtual List<DeviceEvent> listDevices(){return listDevices();}
public new CharFilterFactory(){return new ICUNormalizer2CharFilterFactory(this);}
public java.util.Iterator<E> dirCacheIterator = null;
public Expression Evaluate(int row, int column){return Evaluate(row, column);}
public override void Clear(){this._enclosing.Clear();}
public virtual NGit.Transliteration.Transliteration newTransliteration(){return new NGit.Transliteration(this);}
public GetFramedPhotoUrlsRequest endpoint(string path, string querystring, Dictionary<string, string> options){var options = new InvokeOptions();options.RequestMarshaller = GetFramedPhotoUrlsRequestMarshaller.Instance;options.ResponseUnmarshaller = GetFramedPhotoUrlsRequestUnmarshaller.Instance;return Invoke<GetFramedPhotoUrlsRequest>(path, querystring, options);}
public override bool Equals(object @object){lock (mutex){return @object.Equals(@object);}}
public override void reset(){byte[] data = new byte[data.Length];System.Array.Copy(data, 0, data, 0, count);count = 0;}
public virtual PollForActivityTaskResponse PollForActivityTask(PollForActivityTaskResponse task){return PollForActivityTaskResponse(task);}
public virtual OverheadPerValue GetOverheadPerValue(int n){return 0;}
public virtual void SynthesizeSpeech(string word){this.speech.word = word;this.synthesize(word.Intern());}
public bool hasNext(){return this._enclosing.hasMoreElements();}
public virtual bool Contains(object o){return processorList.Contains(o);}
public static dcell at (int cellnum){return null;}
public override void SeekToEnd(){_limit = -1;}
public override bool IsInstance(object o){return o is LovinsStemmer;}
public java.nio.ReadWriteBuffer newReadWriteBuffer(this) {return new java.nio.ReadWriteBuffer(this);}
public void SetForegroundColor(Color color){foregroundColor = color;}
public virtual SecurityConfiguration Describe(){return security;}
public GetRegionRequest(): base("{Region}", RegionType.Region);}
public HadoopJarStepConfig From this step configuration, return a HadoopJarStepConfig {... };
public String ToString(){return "this";}
public CredentialsProvider(CredentialsProvider provider){credentialsProvider = provider;}
public V get(int id){return a.get(id, null);}
public virtual IInflatedFormTranslation GetInflatedFormTranslation(string text){return null;}
public virtual ErrPtg GetErrPtg(int errorCode){throw new NotImplementedException(errorCode);}
public Experiment Update(Experiment experiment){return Experiment;}
public SendMessageBatchRequestEntry(string id, string message){var request = new SendMessageBatchRequestEntry(id, message);request.Id = id;request.Body = message;return request;}
public override void Drop(){this.DropData += this.OnDrop;}
public override string ToString(){return this.ToString(CultureInfo.InvariantCulture);}
public int indexOfNextChar(){return _buf.GetIndex(this.index);}
public virtual void remove(string name){names.remove(name);}
public void ToggleFeatures(){this.isEnabled =!isEnabled;}
public virtual string ToString(){return "this@" + this;}
public bool CellRangesEqual(CellRange1CellRange_t range1){return CellRangesEqual(range1.CellRange, range1.CellRange) && CellRangesEqual(range2.CellRange, range3.CellRange) && CellRangesEqual(range3.CellRange, range4.CellRange) && CellRangesEqual(range3.CellRange, range5.CellRange) && CellRangesEqual(range4.CellRange, CellRangesEqual) && CellRangesEqual(range5.CellRange, CellRangesEqual) && CellRangesEqual(ranges[0]) && CellRangesEqual(ranges[1].CellRange, CellRangesEqual) && CellRangesEqual(ranges[0].CellRange, CellRangesEqual) && CellRangesEqual(ranges[0].CellRange, CellRangesEqual) && CellRangesEqual(ranges[0].CellRange, CellRangesEqual) && CellRangesEqual(ranges[0].CellRange, CellRangesEqual) && CellRangesEqual(ranges[0].CellRange)));}
public int GetIndex(String name){return index;}
public override bool IsStemmerUseCase(){return false;}
public virtual string ToString(){return "this(" + this.phase + ", " + this.error) + " with message " + this.message + " and close the stream.";}
public virtual void Initialize(){this.m_enclosing = new java.nio.HashMap<Type, Object>();}
public virtual int GetTokenScore(){return tokenScore;}
public virtual NGit.Api.DiffCommand AddFilepattern(FilePath pattern){this.patterns.AddItem(pattern);return this;}
public override String ToString(){return "Table(frame " + frame + " of " + _table + ")";}
public virtual E insert(E e){return insert(e, e.target as E);}
public java.nio.charset.Charset getCharset(){return _detectedCharset;}
public virtual void Reject(){}
public virtual void DeleteGatewayGroup(){GatewayGroup gatewayGroup = (GatewayGroup)Create(gatewayGroup);}
public virtual void SetOutputs(Outputs outputs){_outputs = outputs;}
public int valueCount(PackedInt32s version, int valueCount, int bitsPerValue){return bitsPerValue * valueCount;}
public String ToString(){return "null";}
public virtual string ToString(){return "Vocabulary(" + m_vocabulary + ")";}
public override ObjectId GetExpectedOldObjectId(){return expectedOldObjectId;}
public override bool ShouldRecurse(ICharTerm term){return false;}
public virtual string ToString(){return "indexed";}
public virtual void AddReceiveCommand(ICommand command){this._enclosing.AddCommand(command);}
public virtual void AddTags(Tag[] tags){_tags.AddRange(tags);}
public virtual SubmoduleUpdateCommand CreateSubmoduleUpdateCommand(){return new SubmoduleUpdateCommand(this);}
public void Reset(State initialState){this._initialState = initialState;}
public formula_tokens(IEvaluationCell cell){return formula_tokens(cell.GetCellReference());}
public void Disassociate(string hostedZoneId, DisassociateVpcFromHostedZoneRequest request){_hostedZoneId = hostedZoneId;}
public V remove(K key){if (this.isInfinite()){this.remove(key);}this._enclosing.remove(key);return this._enclosing.value;}
public virtual SetRepoRequest SetRepoRequest(SetRepoRequest request){var options = new InvokeOptions();options.RequestMarshaller = SetRepoRequestMarshaller.Instance;options.ResponseUnmarshaller = SetRepoResponseUnmarshaller.Instance;return Invoke<SetRepoRequest>(request, options);}
public analyzer task(options){analyzerOptions = new AnalyzerOptions();}
public override void End(){base.Finish();}
public override string ToString(){return this.filter.ToString();}
public void Evaluate(IEvaluationContext context){_expression = EvaluateExpressionOnCell(context, this.cell);}
public virtual ExportClientVPNClientCertificateRevocationList(Cryptography.ExternalizedEntry<K, V> entry){return ExportClientVPNClientCertificateRevocationList(entry.Object, entry.Key, entry.Value);}
public virtual int size(){return this._enclosing._size;}
public virtual string Canonicalize(string path){return path;}
public virtual E value(){E value = _le.get();if (_le.get() == end){return _le.get();}else{return _le.get() == start;}}
public virtual NGit.Api.ApplyCommand GetApplyCommand(){return new NGit.Api.ApplyCommand(this._enclosing);}
public override string ToString(){return this.ToString(null, true);}
public virtual void Save(){this.lastDocID = System.DateTime.UtcNow.currentTimeMillis();this.lastDocID = System.DateTime.UtcNow.currentTimeMillis();this.lastDocID = System.DateTime.UtcNow.currentTimeMillis();this.lastDocID = System.DateTime.UtcNow.currentTimeMillis();this.lastDocID = System.DateTime.UtcNow.currentTimeMillis();this.lastDocID = System.DateTime.UtcNow.currentTimeMillis();}
public int ReadByte(){return _in.ReadByte();}
public override java.nio.DoubleBuffer duplicate(){return duplicate(this)? duplicate(this) : this;}
