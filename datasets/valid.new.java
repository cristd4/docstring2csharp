public DVRecord(RecordInputStream in) {rt = in.readShort();grbitFrt = in.readShort();wOffset = in.readShort();at = in.readShort();grbit = in.readShort();if(in.available() == 0) {unused = null;} else {unused = in.readShort();}}
public String toString() {return pattern;}
public InsertInstanceRequest() {super("cr", "2016-06-07", "InsertInstance", "cr");setUriPattern("/clusters/[SubnetId]");setMethod(MethodType.POST);}
public boolean contains(Object o) {synchronized (mutex) {return delegate().contains(o);}}
public static ByteBuffer encodeBase64String(String s) {return encodeBase64String(s, 0, s.length());}
public boolean requiresCommitBody() {return false;}
public String getKey() {return key;}
public ValueEval evaluate(int srcRowIndex, int srcColumnIndex, ValueEval arg0) {double result;try {double d0 = NumericFunction.singleOperandEvaluate(arg0, srcRowIndex, srcColumnIndex);result = NumericFunction.singleOperandEvaluate(arg0, srcRowIndex, srcColumnIndex);} catch (EvaluationException e) {return e.getErrorEval();}return new NumberEval(result);}
public DeleteClientVpnEndpointResult deleteClientVpnEndpoint(DeleteClientVpnEndpointRequest request) {request = beforeClientExecution(request);return executeDeleteClientVpnEndpoint(request);}
public V get(Object key) {if (key == null) {HashMapEntry<K, V> e = entryForNullKey;return e == null? null : e.value;}int hash = key.hashCode();hash ^= (hash >>> 20) ^ (hash >>> 12);hash ^= (hash >>> 7) ^ (hash >>> 4);HashMapEntry<K, V>[] tab = table;for (HashMapEntry<K, V> e = tab[hash & (tab.length - 1)];e!= null; e = e.next) {K eKey = e.key;if (eKey == key || (e.hash == hash && key.equals(eKey))) {return e.value;}}return null;}
public StartFleetActionsResult startFleetActions(StartFleetActionsRequest request) {request = beforeClientExecution(request);return executeStartFleetActions(request);}
public CellRangeAddress getAreaAt(int index) {return _regions[_startIndex + index];}
public static Document loadXML(Reader is) {DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();DocumentBuilder db = null;try {db = dbf.newDocumentBuilder();}catch (Exception se) {throw new RuntimeException("Parser configuration error", se);}org.w3c.dom.Document doc = null;try {doc = db.parse(new InputSource(is));}catch (Exception se) {throw new RuntimeException("Error parsing file:" + se, se);}return doc;}
public double get(String name, double dflt) {double vals[] = (double[]) valByRound.get(name);if (vals!= null) {return vals[roundNumber % vals.length];}String sval = props.getProperty(name, "" + dflt);if (sval.indexOf(":") < 0) {return Double.parseDouble(sval);}int k = sval.indexOf(":");String colName = sval.substring(0, k);sval = sval.substring(k + 1);colForValByRound.put(name, colName);vals = propToDoubleArray(sval);valByRound.put(name, vals);return vals[roundNumber % vals.length];}
public String getBackgroundImageId() {return getBackgroundImage;}
public TreeFilter getTreeFilter() {return (s1, s2) -> s1.getName().equals(s2);}
public GetMemberResult getMember(GetMemberRequest request) {request = beforeClientExecution(request);return executeGetMember(request);}
public boolean canEncode() {return true;}
public ReplaceRouteResult replaceRoute(ReplaceRouteRequest request) {request = beforeClientExecution(request);return executeReplaceRoute(request);}
public ObjectId getResultTreeId() {return resultTree;}
@Override public boolean equals(Object object) {synchronized (mutex) {return list.equals(object);}}
public ListReusableDelegationSetsResult listReusableDelegationSets() {return listReusableDelegationSets(new ListReusableDelegationSetsRequest());}
public String toString() {return "(" + a.toString() + " OR " + b.toString() + ")";}
public InitiateLayerUploadResult initiateLayerUpload(InitiateLayerUploadRequest request) {request = beforeClientExecution(request);return executeInitiateLayerUpload(request);}
public UpdateRepoRequest() {super("cr", "2016-06-07", "UpdateRepo", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/updates");setMethod(MethodType.POST);}
public PhoneticFilterFactory(Map<String,String> args) {super(args);min = requireInt(args, MIN_KEY);max = requireInt(args, MAX_KEY);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public FetchCommand fetch() {return new FetchCommand(repo);}
public QueryPhraseMap searchPhrase( final List<TermInfo> phraseCandidate ){QueryPhraseMap currMap = this;for( TermInfo ti : phraseCandidate ){currMap = currMap.subMap.get( ti.getText() );if( currMap == null ) return null;}return currMap.isValidTermOrPhrase( phraseCandidate )? currMap : null;}
@Override public Iterator<Multiset.Entry<E>> iterator() {return new Multiset.EntryIterator();}
public DeleteDBSnapshotResult deleteDBSnapshot(DeleteDBSnapshotRequest request) {request = beforeClientExecution(request);return executeDeleteDBSnapshot(request);}
public DiffCommand setOutput(boolean output) {this.output = output;return this;}
public LongBuffer compact() {throw new ReadOnlyBufferException();}
public XmlPullParserFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public DeleteAnalysisSchemeResult deleteAnalysisScheme(DeleteAnalysisSchemeRequest request) {request = beforeClientExecution(request);return executeDeleteAnalysisScheme(request);}
public HSSFWorkbookInit(HSSFWorkbook workbook) {super(workbook);}
public IntBuffer put(int index, int c) {checkIndex(index);byteBuffer.putInt(index * SizeOf.INT, c);return this;}
public ClassParameter<I> getParameterClass(int index) {return parameters.get(index);}
public ListEndpointsResult listEndpoints(ListEndpointsRequest request) {request = beforeClientExecution(request);return executeListEndpoints(request);}
public static CharsRef join(List<CharsRef> words) {ArrayList<CharsRef> result = new ArrayList<>();for (CharsRef word : words) {result.add(word);}return result;}
public FloatBuffer insert(int index, float c) {checkIndex(index);byteBuffer.putFloat(index * SizeOf.FLOAT, c);return this;}
public ShortBuffer put(short[] src, int srcOffset, int shortCount) {if (shortCount > remaining()) {throw new BufferOverflowException();}System.arraycopy(src, srcOffset, backingArray, offset + position, shortCount);position += shortCount;return this;}
public DisassociateResolverEndpointIpAddressResult disassociateResolverEndpointIpAddress(DisassociateResolverEndpointIpAddressRequest request) {request = beforeClientExecution(request);return executeDisassociateResolverEndpointIpAddress(request);}
public AcceptDirectConnectGatewayAssociationProposalResult acceptDirectConnectGatewayAssociationProposal(AcceptDirectConnectGatewayAssociationProposalRequest request) {request = beforeClientExecution(request);return executeAcceptDirectConnectGatewayAssociationProposal(request);}
public StopStackOperationResult stopStackOperation(StopStackOperationRequest request) {request = beforeClientExecution(request);return executeStopStackOperation(request);}
public CacheSubnetGroup createCacheSubnetGroup(CreateCacheSubnetGroupRequest request) {request = beforeClientExecution(request);return executeCreateCacheSubnetGroup(request);}
public LoadOrdinalsResult loadOrdinals(Reader in) throws IOException {try {return loadOrdinals(in, Integer.MAX_VALUE);} catch (NumberFormatException e) {throw new IllegalArgumentException(e);}}
public String getRawUserInfo() {return userInfo;}
public Object[] toArray() {return elements.clone();}
public GetCompilationJobResult getCompilationJob(GetCompilationJobRequest request) {request = beforeClientExecution(request);return executeGetCompilationJob(request);}
public CharSequence toQueryString(EscapeQuerySyntax escaper) {if (queryString == null) {return null;}StringBuilder sb = new StringBuilder();if (queryString.length() > 0) {sb.append('?');}if (queryString.length() > 0) {sb.append(queryString.substring(0, queryString.length()));} int size = queryStringSize.get(0);if (size > 0) {sb.append('?');}if (sb.length() > 0) {sb.append('#');}if (sb.length() > 0) {sb.append('#');}if(sb.length() > 0) {if (sb.length() > 0) {sb.append('#');}sb.append(sb.toString());}return sb.toString();}
public CreateEnvironmentResult createEnvironment(CreateEnvironmentRequest request) {request = beforeClientExecution(request);return executeCreateEnvironment(request);}
public ParseTreeMatch match(ParseTree tree, int patternIndex) {return match(tree, null, patternIndex);}
public boolean containsKey(CharSequence cs) {if(cs == null)throw new NullPointerException();return false;}
public QueryStringQuery(String tableName) {this.tableName = tableName;}
public boolean isParentRowHidden(int row) {return false;}
public void retryFailedLockFileCommit(Commit retryRequest) {if (currentFile!= null && currentFile.exists()) {currentFile = newFile(currentFile);} else if (currentFile!= null && currentFile.exists()) {currentFile = newFile(currentFile);} else if (currentFile!= null && currentFile.exists()) {currentFile = newFile(currentFile);} else if (currentFile == null) {currentFile = newFile(currentFile);} else if (currentFile.exists()) {currentFile = newFile;currentFile = newFile(currentFile);} else if (currentFile.exists()) {currentFile = newFile(currentFile);} else {currentFile = newFile(currentFile);}}
public ValidateMatchmakingRuleSetResult validateMatchmakingRuleSet(ValidateMatchmakingRuleSetRequest request) {request = beforeClientExecution(request);return executeValidateMatchmakingRuleSet(request);}
public boolean get(String name, boolean dflt) {boolean vals[] = (boolean[]) valByRound.get(name);if (vals!= null) {return vals[roundNumber % vals.length];}String sval = props.getProperty(name, "" + dflt);if (sval.indexOf(":") < 0) {return Boolean.valueOf(sval).booleanValue();}int k = sval.indexOf(":");String colName = sval.substring(0, k);sval = sval.substring(k + 1);colForValByRound.put(name, colName);vals = propToBooleanArray(sval);valByRound.put(name, vals);return vals[roundNumber % vals.length];}
public UpdateLinkAttributesResult updateLinkAttributes(UpdateLinkAttributesRequest request) {request = beforeClientExecution(request);return executeUpdateLinkAttributes(request);}
public NumericPayloadFilter(TokenStream input) {for (int i = 0; i < maxPayloads-1; i++) {if (input.tokenizedNumeric(i)) == false) {return new NumericPayloadFilter(input, i);}}return new NumericPayloadFilter(input, maxPayloads-1);}
public String toFormulaString(String[] operands) {StringBuilder buf = new StringBuilder();if(isExternalFunction()) {buf.append(operands[0]); appendArgs(buf, 1, operands);} else {buf.append(getName());appendArgs(buf, 0, operands);}return buf.toString();}
public void push(E object) {throw new UnsupportedOperationException();}
public LinkedHashMap(int initialCapacity, int loadFactor) {super(loadFactor);this.initialCapacity = initialCapacity;this.loadFactor = loadFactor;}
public final void setBackedMap(final Map<K, V> newBackedMap) {if (backedMap == null) {backedMap = new HashMap<>();backedMap = new HashMap<>();} else {backedMap = new HashMap<>();}}
public synchronized long skip(long charCount) {if (charCount <= 0) {return 0;}int numskipped;if (this.count - pos < charCount) {numskipped = this.count - pos;pos = this.count;} else {numskipped = (int) charCount;pos += charCount;}return numskipped;}
public ValueEval getRef3DPxg() {return new Ref3DPxg(this);}
public AnalyzerTask() {super("cr", "2016-06-07", "AnalyzerTask", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/tags/[Tag]/analyzer");setMethod(MethodType.PUT);}
public boolean equals( Object o ) {return o instanceof EnglishStemmer;}
public void decode(long[] blocks, int blocksOffset, long[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long block = blocks[blocksOffset++];for (int shift = 48; shift >= 0; shift -= 16) {values[valuesOffset++] = (block >>> shift) & 65535;}}}
public void incRef() {refCount.incrementAndGet();}
public TestFailoverReplicationGroupResult testFailoverReplicationGroup(TestFailoverReplicationGroupRequest request) {request = beforeClientExecution(request);return executeTestFailoverReplicationGroup(request);}
public static Collection<Ref> sort(Collection<Ref> refs) {final List<Ref> r = new ArrayList<>(refs);Collections.sort(r, INSTANCE);return r;}
public Vector(int capacity, int capacityIncrement) {if (capacity < 0) {throw new IllegalArgumentException("Capacity: " + capacity);}if (capacity == 0) {throw new IllegalArgumentException("Capacity: " + capacity);}if (capacityIncrement > 0) {throw new IllegalArgumentException("Capacity: " + capacityIncrement);}if (capacityIncrement < 0) {throw new IllegalArgumentException("Capacity: " + capacityIncrement);}capacityIncrement = Math.min(capacity, capacityIncrement);return this;}
public void endWorker(EndWorkerRequest request) {request = beforeClientExecution(request);executeEndWorker(request);}
public DescribeVolumeStatusResult describeVolumeStatus(DescribeVolumeStatusRequest request) {request = beforeClientExecution(request);return executeDescribeVolumeStatus(request);}
public IntMapper(int initialCapacity) {entries = new int[initialCapacity];}
public void serialize(LittleEndianOutput out) {field_1_border_type = out.readShort();field_1_border_options = in.readShort();}
public void copyTo(int[] array) {int pos = 0;int end = count - count;array[pos] = buffer[pos];}
public ValueEval evaluate(String sheetName, int rowIndex, int columnIndex) {EvaluationCell cell = _sewb.getEvaluationCell(sheetName, rowIndex, columnIndex);switch (cell.getCellType()) {case BOOLEAN:return BoolEval.valueOf(cell.getBooleanCellValue());case ERROR:return ErrorEval.valueOf(cell.getErrorCellValue());case FORMULA:return _evaluator.evaluate(cell);case NUMERIC:return new NumberEval(cell.getNumericCellValue());case STRING:return new StringEval(cell.getStringCellValue());case BLANK:return null;default:throw new IllegalStateException("Bad cell type (" + cell.getCellType() + ")");}}
public CustomViewSettingsRecord aggregate(RecordStream in, int subRecordType) {setSubRecordType(subRecordType);}
public DeleteSignalingChannelResult deleteSignalingChannel(DeleteSignalingChannelRequest request) {request = beforeClientExecution(request);return executeDeleteSignalingChannel(request);}
public boolean remove(Object o) {if (!(o instanceof Map.Entry))return false;Map.Entry<?,?> e = (Map.Entry<?,?>)o;return ConcurrentHashMap.this.remove(e.getKey(), e.getValue());}
public PersistentSnapshotDeletionPolicy(IndexDeletionPolicy primary,Directory dir, OpenMode mode) throws IOException {super(primary);this.dir = dir;if (mode == OpenMode.CREATE) {clearPriorSnapshots();}loadPriorSnapshots();if (mode == OpenMode.APPEND && nextWriteGen == 0) {throw new IllegalStateException("no snapshots stored in this directory");}}
public void throwException(Throwable e) {throw new RuntimeException(e);}
public StringLookup(String input, int pos, int len) {StringBuilder sb = new StringBuilder();sb.append(input);sb.append("[");sb.append(input);sb.append("]");for (int i = 0; i < len; i++) {char c = input.charAt(i);if (c > 0x10) {sb.append(" ");} else if (c > 0x20) {sb.append(":");} else if (c > 0) {sb.append(" ");} else if (c > 0) {sb.append(" ");} else if (c > 0) {sb.append(" ");} else {sb.append("]");}sb.append(c);}sb.append("]");return sb.toString();}
public GetPublicAccessUrlsRequest() {super("industry-brain", "2018-07-12", "GetPublicAccessUrls");setProtocol(ProtocolType.HTTPS);}
public CleanCommand clean() {return new CleanCommand(repo);}
public Collection<String> getAllPackFiles() {Collection<String> files = new TreeSet<>();for (org.apache.poi.hssf.record.Record r : backingMap.records) {if (r.getName().equals("pack")) {files.add(r.getName());} else {files.add(r.getName());}}return files;}
public DescribeStackDriftDetectionResult describeStackDriftDetection(DescribeStackDriftDetectionRequest request) {request = beforeClientExecution(request);return executeDescribeStackDriftDetection(request);}
public ListOriginAccessIdentitiesResult listOriginAccessIdentities(ListOriginAccessIdentitiesRequest request) {request = beforeClientExecution(request);return executeListOriginAccessIdentities(request);}
public static Factory get(int id) {return factories.get(id);}
public ListConferenceProvidersResult listConferenceProviders(ListConferenceProvidersRequest request) {request = beforeClientExecution(request);return executeListConferenceProviders(request);}
public UpdateReceiptRuleResult updateReceiptRule(UpdateReceiptRuleRequest request) {request = beforeClientExecution(request);return executeUpdateReceiptRule(request);}
public String toString() {final StringBuilder r = new StringBuilder();r.append("(");for (int i = 0; i < subfilters.length; i++) {if (i > 0)r.append(" AND ");r.append(subfilters[i].toString());}r.append(")");return r.toString();}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_number_of_sheets);for (int k = 0; k < field_1_count_of_sheets; k++) {out.writeShort(field_1_number_of_sheets);}}
public UpdateHealthCheckResult updateHealthCheck(UpdateHealthCheckRequest request) {request = beforeClientExecution(request);return executeUpdateHealthCheck(request);}
public synchronized long ramBytesUsed() {long size = 0;for(CachedOrds ords : ordsCache.values()) {size += ords.ramBytesUsed();}return size;}
public UpdateWorkforceResult updateWorkforce(UpdateWorkforceRequest request) {request = beforeClientExecution(request);return executeUpdateWorkforce(request);}
public void setObjectId(AnyObjectId obj, int objType) {object = obj.copy();type = objType;}
public void write(byte[] buffer, int off, int len) throws IOException {checkWritePrimitiveTypes();primitiveTypes.write(buffer, off, len);}
public GetBlockBlockResult getBlock(GetBlockRequest request) {request = beforeClientExecution(request);return executeGetBlock(request);}
public void addExportDirectory(String exportBase, String directory, Set<String> exportNames) {setExportBase(exportBase);setDirectory(directory);setExportNames(exportNames);}
public CreateReservedInstancesResult createReservedInstances(CreateReservedInstancesRequest request) {request = beforeClientExecution(request);return executeCreateReservedInstances(request);}
public ByteBuffer put(byte b) {if (position == limit) {throw new BufferOverflowException();}byteBuffer.put(b);return this;}
public ValueEval evaluate(int srcCellRow, int srcCellCol, ValueEval arg0) {ValueEval ve;try {ve = OperandResolver.getSingleValue(arg0, srcCellRow, srcCellCol);} catch (EvaluationException e) {ve = e.getErrorEval();}return BoolEval.valueOf(evaluate(ve));}
public StopJumpServerRequest() {super("industry-brain", "2018-07-12", "StopJumpServer");setProtocol(ProtocolType.HTTPS);}
public CreateDirectoryConfigResult createDirectoryConfig(CreateDirectoryConfigRequest request) {request = beforeClientExecution(request);return executeCreateDirectoryConfig(request);}
public DescribeExportTasksResult describeExportTasks() {return describeExportTasks(new DescribeExportTasksRequest());}
public ExportClientVpnClientCertificateRevocationListResult exportClientVpnClientCertificateRevocationList(ExportClientVpnClientCertificateRevocationListRequest request) {request = beforeClientExecution(request);return executeExportClientVpnClientCertificateRevocationList(request);}
public CompleteMultipartUploadResult completeMultipartUpload(CompleteMultipartUploadRequest request) {request = beforeClientExecution(request);return executeCompleteMultipartUpload(request);}
public long ramBytesUsed() {long ramBytesUsed = postingsReader.ramBytesUsed();for (TermsReader r : fields.values()) {ramBytesUsed += r.ramBytesUsed();}return ramBytesUsed;}
public static void fill(float[] array, float value) {for (int i = 0; i < array.length; i++) {array[i] = value;}}
public DoubleBuffer put(int index, double c) {checkIndex(index);byteBuffer.putDouble(index * SizeOf.DOUBLE, c);return this;}
public DescribeAdjustmentTypesResult describeAdjustmentTypes(DescribeAdjustmentTypesRequest request) {request = beforeClientExecution(request);return executeDescribeAdjustmentTypes(request);}
public PersonIdent getSourceAuthor() {return source.getAuthor();}
public Object[] toArray() {return elements.clone();}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[VERSION]\n");buffer.append("   .version = ").append(Integer.toHexString(getVersion())).append("\n");buffer.append("[/VERSION]\n");return buffer.toString();}
public PushCommand setRefSpecs(List<RefSpec> specs) {checkCallable();this.refSpecs.clear();this.refSpecs.addAll(specs);return this;}
public String toString() {return field.toString();}
public static final X[] filter(X[] input) {if (input == null) {return null;}return tokenize(input, 0, input.length);}
public int read() {if (pos < size) {return s.charAt(pos++);} else {s = null;return -1;}}
public PersonIdent getRefLogIdent() {return refLogIdent;}
public int size() {return size;}
public GetRequestValidatorsResult getRequestValidators(GetRequestValidatorsRequest request) {request = beforeClientExecution(request);return executeGetRequestValidators(request);}
public String toString() {return String.valueOf(getValue());}
public boolean equals(Object o) {return o instanceof Token;}
public int fillFields(byte[] data, int offset,EscherRecordFactory recordFactory ){int bytesRemaining = readHeader( data, offset );short propertiesCount = readInstance( data, offset );int pos = offset + 8;EscherPropertyFactory f = new EscherPropertyFactory();properties.clear();properties.addAll( f.createProperties( data, pos, bytesRemaining ) );return bytesRemaining + 8;}
public String getSignerName() {return null;}
@Override public void clear() {if (size!= 0) {Arrays.fill(table, null);entryForNullKey = null;modCount++;size = 0;}}
public CancelCapacityReservationResult cancelCapacityReservation(CancelCapacityReservationRequest request) {request = beforeClientExecution(request);return executeCancelCapacityReservation(request);}
public ImportDocumentationPartsResult importDocumentationParts(ImportDocumentationPartsRequest request) {request = beforeClientExecution(request);return executeImportDocumentationParts(request);}
public SelectResult select(SelectRequest request) {request = beforeClientExecution(request);return executeSelect(request);}
public Explanation explain(String docIdField, int numPayloadsSeen, long payloadScore, Explanation explain) {super(explanation);this.docIdField = docIdField;this.numPayloadsSeen = numPayloadsSeen;this.explanation = explain;}
public int serialize(int offset, byte[] data, EscherSerializationListener listener) {listener.beforeRecordSerialize( offset, getRecordSet());listener.afterRecordSerialize( offset, getRecordSet());return 2 + getRecordSet().size();}
public synchronized String toString() {return super.toString();}
public static long[] copyOfRange(long[] original, int start, int end) {if (start > end) {throw new IllegalArgumentException();}int originalLength = original.length;if (start < 0 || start > originalLength) {throw new ArrayIndexOutOfBoundsException();}int resultLength = end - start;int copyLength = Math.min(resultLength, originalLength - start);long[] result = new long[resultLength];System.arraycopy(original, start, result, 0, copyLength);return result;}
public static byte[] fromByteBuffer(ByteBuffer buf) {byte[] result = new byte[buf.remaining()];buf.get(result);return result;}
public void setProgress(int value) {if (mIndeterminate || (value < 0) || (value > 0) || (value == 0) {return;}if (mIndeterminate && value < 0) {return;}mProgress = value;}
public void removeCell(int rowIndex, int columnIndex) {if (rowIndex > _firstRowIndex) {return;}if (rowIndex > _lastRowIndex) {return;}if (rowIndex < _firstRowIndex) {return;}if (rowIndex > _lastRowIndex) {return;}if (rowIndex == _lastRowIndex) {return;}if (rowIndex == _firstRowIndex) {return;}if (rowIndex == _lastRowIndex) {return;}if (rowIndex == _lastRowIndex) {return;}if (rowIndex == _lastRowIndex) {return;} if (rowIndex == _lastRowIndex) {return;} else {return;}}
public static String canonicalize(String path) {int colon = path.lastIndexOf('.');if (colon < 0 || colon > 1) {return path.substring(0, colon).toLowerCase(Locale.ROOT);}int pathIx = pathIx;int pathJ = pathJ + 1;if (pathJ > 0) {pathJ = pathJ;}if (pathJ < 0) {pathJ = -pathJ;}if (pathJ > 0) {pathJ = pathJ;}int key = path.lastIndexOf('.');if (keyJ > 0) {pathJ = pathJ;}key = pathJ;if (key.length() > 0) {pathIx = pathIx.length() - 1;}if (pathIx < 0) {pathIx = pathIx;}return pathIx;}
public ApostropheFilterFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public Entry<K, V> next() {return nextEntry();}
public DeleteEnvironmentResult deleteEnvironment(DeleteEnvironmentRequest request) {request = beforeClientExecution(request);return executeDeleteEnvironment(request);}
public int stem(char s[], int len) {if (len < 4) return len;final int origLen = len;len = rule0(s, len);len = rule1(s, len);len = rule2(s, len);len = rule3(s, len);len = rule4(s, len);len = rule5(s, len);len = rule6(s, len);len = rule7(s, len);len = rule8(s, len);len = rule9(s, len);len = rule10(s, len);len = rule11(s, len);len = rule12(s, len);len = rule13(s, len);len = rule14(s, len);len = rule15(s, len);len = rule16(s, len);len = rule17(s, len);len = rule18(s, len);len = rule19(s, len);len = rule20(s, len);if (len == origLen)len = rule21(s, len);return rule22(s, len);}
public void beforeRecord(EscherRecord childRecord) {field_1_number_of_sheets = 1;field_2_num_sheets = 2;field_3_num_sheets = 2;field_4_add_mode(field_4_set_mode(field_4_set_mode_value));}
public ListAlbumsRequest() {super("CloudPhoto", "2017-07-11", "ListAlbums", "cloudphoto");setProtocol(ProtocolType.HTTPS);}
public AddRegistrationantInfoByIdentityCredentialRequest() {super("Domain-intl", "2018-01-29", "UpdateRegistrationantInfoByIdentityCredential");setProtocol(ProtocolType.HTTPS);setMethod(MethodType.POST);}
public int getPixelHeight(int srcRowIndex, int srcColumnIndex) {return 0;}
public DescribeReservedInstancesResult describeReservedInstances() {return describeReservedInstances(new DescribeReservedInstancesRequest());}
public void setPackedGitMMAP(boolean packed) {this(0);}
public POIFSDocument(String path) {setPath(path);}
public String toString() {return key + "=" + value;}
public void decode(byte[] blocks, int blocksOffset, int[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final int byte0 = blocks[blocksOffset++] & 0xFF;final int byte1 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 2) | (byte1 >>> 6);final int byte2 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte1 & 63) << 4) | (byte2 >>> 4);final int byte3 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte2 & 15) << 6) | (byte3 >>> 2);final int byte4 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte3 & 3) << 8) | byte4;}}
public void serialize(LittleEndianOutput out) {out.writeShort(field_1_number_of_sheets);if(isExternalReferences()) {StringUtil.writeUnicodeString(out, field_2_encoded_url);for (String field_3_sheet_name : field_3_sheet_names) {StringUtil.writeUnicodeString(out, field_3_sheet_name);}} else {int field2val = _isAddInFunctions? TAG_ADD_IN_FUNCTIONS : TAG_INTERNAL_REFERENCES;out.writeShort(field2val);}}
public PatternConsumer(String pattern) {this.pattern = pattern;}
public String getField(String name) {return field_names.get(name);}
public ListIdentityPoolUsageResult listIdentityPoolUsage(ListIdentityPoolUsageRequest request) {request = beforeClientExecution(request);return executeListIdentityPoolUsage(request);}
public ValueEval evaluate(int srcRowIndex, int srcColumnIndex, ValueEval arg0,ValueEval arg1) {double result;try {double d0 = NumericFunction.singleOperandEvaluate(arg0, srcRowIndex, srcColumnIndex);double d1 = NumericFunction.singleOperandEvaluate(arg1, srcRowIndex, srcColumnIndex);result = evaluate(d0, d1);} catch (EvaluationException e) {return e.getErrorEval();}return new NumberEval(result);}
public Map<Long, Long> getCountsByTime() {Map<Long, Long> m = new HashMap<>();m = new HashMap<>();m.put(Long.class, 0);m.put(Long.class, 0);m.put(Long.class, 0);m.put(Long.class, 0);return m;}
public UpdateAccountResult updateAccount(UpdateAccountRequest request) {request = beforeClientExecution(request);return executeUpdateAccount(request);}
public DescribeTrainingJobResult describeTrainingJob(DescribeTrainingJobRequest request) {request = beforeClientExecution(request);return executeDescribeTrainingJob(request);}
public DeleteGroupResult deleteGroup(DeleteGroupRequest request) {request = beforeClientExecution(request);return executeDeleteGroup(request);}
public void advance(int target) {if (target < 0 || target > 0x00) {throw new IllegalArgumentException("target < 0: " + target + " > " + target + "!= 0x00);}else if (target > 0x00FF) {throw new IllegalArgumentException("target > 0x00FF");}else if (target > 0x00FF) {throw new IllegalArgumentException("target > 0x00FF");}else if (target < 0 || target > 0x00FF) {throw new IllegalArgumentException("target < 0|| target > 0x00FF) {throw new IllegalArgumentException("target < 0x00FF: " + target + " > " + 0x00FF) + "!= " + 0x00FF) ;}else if (target == 0x00FF) {throw new IllegalArgumentException("target > 0x00FF) {throw new IllegalArgumentException("target > 0x00FF");} else if (target > 0x00FF) {throw new IllegalArgumentException("target < 0x00FF) && target!= 0x00FF) {throw new IllegalArgumentException("target < 0x00FF: " + target + "
public RegisterListenerResult registerListener(RegisterListenerRequest request) {request = beforeClientExecution(request);return executeRegisterListener(request);}
public static double[] grow(double[] array, int minSize) {assert minSize >= 0: "size must be positive (got " + minSize + "): likely integer overflow?";if (array.length < minSize) {return growExact(array, oversize(minSize, Double.BYTES));} else return array;}
public Result visitTerminal(VisitTerminalNodeRequest request) {request = beforeClientExecution(request);return executeVisitTerminal(request);}
public TokenStream create(TokenStream input) {return new LatvianStemFilter(input);}
public IncreaseReplicationGroupReplicaCountResult increaseReplicationGroupReplicaCount(IncreaseReplicationGroupReplicaCountRequest request) {request = beforeClientExecution(request);return executeIncreaseReplicationCount(request);}
public void decode(byte[] blocks, int blocksOffset, long[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long byte0 = blocks[blocksOffset++] & 0xFF;final long byte1 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 6) | (byte1 >>> 2);final long byte2 = blocks[blocksOffset++] & 0xFF;final long byte3 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte1 & 3) << 12) | (byte2 << 4) | (byte3 >>> 4);final long byte4 = blocks[blocksOffset++] & 0xFF;final long byte5 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte3 & 15) << 10) | (byte4 << 2) | (byte5 >>> 6);final long byte6 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = ((byte5 & 63) << 8) | byte6;}}
public StopHyperParameterJobResult stopHyperParameterJob(StopHyperParameterJobRequest request) {request = beforeClientExecution(request);return executeStopHyperParameterJob(request);}
public ResetNetworkInterfaceAttributesResult resetNetworkInterfaceAttributes(ResetNetworkInterfaceAttributesRequest request) {request = beforeClientExecution(request);return executeResetNetworkInterfaceAttributes(request);}
public Blob lookupBlobById(BlobId blobId) {return (Blob) blobId.clone();}
public ListGroupMembershipsResult listGroupMemberships(ListGroupMembershipsRequest request) {request = beforeClientExecution(request);return executeListGroupMemberships(request);}
public static void mkdirs(File d, boolean skipExisting)throws IOException {if (!d.mkdirs()) {if (skipExisting && d.isDirectory())return;throw new IOException(MessageFormat.format(JGitText.get().mkDirsFailed, d.getAbsolutePath()));}}
public UpdateDetectorVersionMetadataResult updateDetectorVersionMetadata(UpdateDetectorVersionMetadataRequest request) {request = beforeClientExecution(request);return executeUpdateDetectorVersionMetadata(request);}
public void write(String str, int offset, int count) throws IOException {synchronized (lock) {if (count < 0) {throw new StringIndexOutOfBoundsException(str, offset, count);}if (str == null) {throw new NullPointerException("str == null");}if ((offset | count) < 0 || offset > str.length() - count) {throw new StringIndexOutOfBoundsException(str, offset, count);}checkStatus();CharBuffer chars = CharBuffer.wrap(str, offset, count + offset);convert(chars);}}
public void ensureCapacity(int min) {if (min > value.length) {int ourMin = value.length*2 + 2;enlargeBuffer(Math.max(ourMin, min));}}
public Get recipeResult recipe(Get recipeRequest) {request = beforeClientExecution(request);return executeGet recipe(request);}
public DisassociateRouteTableResult disassociateRouteTable(DisassociateRouteTableRequest request) {request = beforeClientExecution(request);return executeDisassociateRouteTable(request);}
public SetTopicAttributesRequest(String key, SetTopicAttributesRequest attributes) {setTopicAttributeName(key);setAttributes(attributes);}
public static char[] grow(char[] array, int minSize) {assert minSize >= 0: "size must be positive (got " + minSize + "): likely integer overflow?";if (array.length < minSize) {return growExact(array, oversize(minSize, Character.BYTES));} else return array;}
public AddCommand setRef(String ref) {checkCallable();this.ref = ref;return this;}
public FormulaRecord(RecordInputStream in) {field_1_options = in.readShort();field_2_group_options = in.readShort();}
public SynonymQuery build() {Collections.sort(terms, Comparator.comparing(a -> a.term));return new SynonymQuery(terms.toArray(new TermAndBoost[0]), field);}
public PasswordRev4Record( byte[] data) {field_1_password_rev4 = new PasswordRev4();field_2_password_rev4.clear();for (int i = 0; i < data.length; i++) {field_2_password_rev4[i] = data[i];}return record;}
public boolean isReadOnly() {return true;}
public int indexOf(int pos, int len) {return indexOf(pos, Integer.valueOf(len));}
public CodePageRecord(RecordInputStream in) {rt = in.readShort();grbitFrt = in.readShort();in.readShort();grbit = in.readShort();in.readShort();}
public ApproveAssignmentResult approveAssignment(ApproveAssignmentRequest request) {request = beforeClientExecution(request);return executeApproveAssignment(request);}
public DescribeVpnConnectionsResult describeVpnConnections() {return describeVpnConnections(new DescribeVpnConnectionsRequest());}
public V next() { return nextEntry().value; }
public GetInstanceHealthResult getInstanceHealth(GetInstanceHealthRequest request) {request = beforeClientExecution(request);return executeGetInstanceHealth(request);}
public RegisterTransportProtocolResult registerTransportProtocol(RegisterTransportProtocolRequest request) {request = beforeClientExecution(request);return executeRegisterTransportProtocol(request);}
public static char[] copyOfRange(char[] original, int start, int end) {if (start > end) {throw new IllegalArgumentException();}int originalLength = original.length;if (start < 0 || start > originalLength) {throw new ArrayIndexOutOfBoundsException();}int resultLength = end - start;int copyLength = Math.min(resultLength, originalLength - start);char[] result = new char[resultLength];System.arraycopy(original, start, result, 0, copyLength);return result;}
public static void fill(int[] array, int start, int end, int value) {Arrays.checkStartAndEnd(array.length, start, end);for (int i = start; i < end; i++) {array[i] = value;}}
public int next() {if (nextRecord == null) {return NO_MORE_RECORD_CLASS;} else {return nextRecord.getClass();}}
public static char[] copyOf(char[] original, int newLength) {if (newLength < 0) {throw new NegativeArraySizeException();}return copyOfRange(original, 0, newLength);}
public DeleteRelationalDatabaseResult deleteRelationalDatabase(DeleteRelationalDatabaseRequest request) {request = beforeClientExecution(request);return executeDeleteRelationalDatabase(request);}
public boolean equals(Object obj) {if (this == obj)return true;if (obj == null)return false;if (getClass()!= obj.getClass())return false;WeightedPhraseInfo other = (WeightedPhraseInfo) obj;return true;}
public boolean hasNext() {return nextBlock!= null;}
public void write(int oneChar) {synchronized (lock) {expand(1);buf[count++] = (char) oneChar;}}
public void serialize(LittleEndianOutput out) {out.writeShort(sid);out.writeShort(_reserved);}
public ListHierarchyGroupsResult listHierarchyGroups(ListHierarchyGroupsRequest request) {request = beforeClientExecution(request);return executeListHierarchyGroups(request);}
public TopicArn(String topicArn) {setTopicArn(topicArn);}
public CreateTrafficPolicyVersionResult createTrafficPolicyVersion(CreateTrafficPolicyVersionRequest request) {request = beforeClientExecution(request);return executeCreateTrafficPolicyVersion(request);}
@Override public synchronized boolean equals(Object object) {return (object instanceof Map) &&entrySet().equals(((Map<?,?>)object).entrySet());}
public ListResourcesResult listResources(ListResourcesRequest request) {request = beforeClientExecution(request);return executeListResources(request);}
public AtomicReferenceArray(char[] array, int value) {Arrays.checkOffsetAndCount(array.length, offset, value.length);this.array = array;this.value = value;return this;}
public FeatHdrRecord(RecordInputStream in) {field_1_first_row = 1;field_2_last_col = 1;field_3_first_col = 2;field_4_first_col = 3;field_5_first_col = 4;field_6_first_col = 0;field_7_first_col = 0;field_8_first_col = 0;field_9_first_col = 0;return new FeatHdrRecord(in, field_7);}
public DisassociatePhoneNumbersFromVoiceConnectorResult disassociatePhoneNumbersFromVoiceConnector(DisassociatePhoneNumbersFromVoiceConnectorRequest request) {request = beforeClientExecution(request);return executeDisassociatePhoneNumbersFromVoiceConnector(request);}
public ObjectId idFor(int type, byte[] data) {return delegate().idFor(type, data);}
public void removeParseListener(ParseTreeListener listener) {if (listener!= null) {parseListeners.remove(listener);}}
public AxisData(RecordInputStream in) {field_1_axis_num = in.readInt();field_2_axis_num = in.readInt();field_3_major = in.readInt();field_4_minor = in.readInt();field_5_category_pos = in.readInt();field_6_options = in.readInt();int field_7_options = in.readInt();field_8_options = in.readInt();int field_9_options = in.readInt();field_10_options = in.readInt();}
public static double max(double[] v) {double r = 0;if (v!=null && v.length > 0) {int n = v.length;Arrays.sort(v);r = (n + 1) / 2;}return r;}
public void addFacet(int newValue) {_facets.put(newValue);}
public String toString() {return "fileName=" + fileName + " size=" + size;}
public String toString(){StringBuilder buffer = new StringBuilder();buffer.append("[IFMT]\n");buffer.append("   .formatIndex          = ").append("0x").append(HexDump.toHex(  getFormatIndex ())).append(" (").append( getFormatIndex() ).append(" )");buffer.append(System.getProperty("line.separator"));buffer.append("[/IFMT]\n");return buffer.toString();}
public CharVector(int capacity) {if (capacity < 0) {throw new IllegalArgumentException("capacity < 0: " + capacity);}if (capacity > 0) {charVector = new char[capacity];System.arraycopy(capacity, 0, charVector, 0, capacity);}return charVector;}
public DescribeAccountLimitsResult describeAccountLimits(DescribeAccountLimitsRequest request) {request = beforeClientExecution(request);return executeDescribeAccountLimits(request);}
public boolean removeBuiltinRecord(BuiltinRecord r) {return false;}
public CreateSecurityGroupResult createSecurityGroup(CreateSecurityGroupRequest request) {request = beforeClientExecution(request);return executeCreateSecurityGroup(request);}
public boolean equals(Object o) {return this.getClass() == o.getClass();}
public GetObjectResult getObject(GetObjectRequest request) {request = beforeClientExecution(request);return executeGetObject(request);}
public StringBuilder append(long l) {IntegralToString.appendLong(this, l);return this;}
public GetIntegrationResponsesResult getIntegrationResponses(GetIntegrationResponsesRequest request) {request = beforeClientExecution(request);return executeGetIntegrationResponses(request);}
public ListDeploymentConfigsResult listDeploymentConfigs(ListDeploymentConfigsRequest request) {request = beforeClientExecution(request);return executeListDeploymentConfigs(request);}
public void removeCellRange(int rowIndex) {removeCellValue(rowIndex);}
public DimConfig getDimConfig(String dimensionName) {if (dimensionName == null) {throw new IllegalArgumentException("dimensionName must not be null");}if (null == dimConfig) {return null;}DimensionConfig dimConfig = dimConfig.getConfig(dimensionName);if (dimConfig == null) {throw new IllegalArgumentException("dimensionName must not be null");}return dimConfig;}
public GetStackDrift(GetStackDriftRequest request) {request = beforeClientExecution(request);return executeGetStackDrift(request);}
public CreateCommandLineParametersResult createCommandLineParameters(CreateCommandLineParametersRequest request) {request = beforeClientExecution(request);return executeCreateCommandLineParameters(request);}
public GetRepositoryAssociationResult getRepositoryAssociation(GetRepositoryAssociationRequest request) {request = beforeClientExecution(request);return executeGetRepositoryAssociation(request);}
public Enumeration<E> getElements() {return new Set().elements();}
public void set(int index, int n) {if (count < index)throw new ArrayIndexOutOfBoundsException(index);else if (count == index)add(n);elseentries[index] = n;}
public HTMLStripCharFilterFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public long getEntryPathLength() {return pathLength;}
public void serialize(LittleEndianOutput out) {out.writeShort(sid);out.writeShort(_cbFContinued);if (_linkPtg == null) {out.writeShort(0);} else {int formulaSize = _linkPtg.getSize();int linkSize = formulaSize + 6;if (_unknownPostFormulaByte!= null) {linkSize++;}out.writeShort(linkSize);out.writeShort(formulaSize);out.writeInt(_unknownPreFormulaInt);_linkPtg.write(out);if (_unknownPostFormulaByte!= null) {out.writeByte(_unknownPostFormulaByte.intValue());}}out.writeShort(_cLines);out.writeShort(_iSel);out.writeShort(_flags);out.writeShort(_idEdit);if(_dropData!= null) {_dropData.serialize(out);}if(_rgLines!= null) {for(String str : _rgLines){StringUtil.writeUnicodeString(out, str);}}if(_bsels!= null) {for(boolean val : _bsels){out.writeByte(val? 1 : 0);}}}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[1904]\n");buffer.append("   .is1904          = ").append(Integer.toHexString(getWindowing())).append("\n");buffer.append("[/1904]\n");return buffer.toString();}
public ContinueDeploymentResult continueDeployment(ContinueDeploymentRequest request) {request = beforeClientExecution(request);return executeContinueDeployment(request);}
public void set(int index, int n) {if (count < index)throw new ArrayIndexOutOfBoundsException(index);else if (count == index)add(n);elseentries[index] = n;}
public final long next() {assert hasNext();long result = currentValues[pOff++];if (pOff == currentCount) {vOff += 1;pOff = 0;fillBlock();}return result;}
public static final RevFilter between(Date since, Date until) {return between(since.getTime(), until.getTime());}
public DeleteVaultResult deleteVault(DeleteVaultRequest request) {request = beforeClientExecution(request);return executeDeleteVault(request);}
public void reset() throws IOException {drain();output.writeByte(TC_RESET);resetState();}
public void setDetachFromSymbolicReference(boolean detachFromSymbolic) {this.detachedFromSymbolic = detachFromSymbolic;}
public ModifyIdentityFormatResult modifyIdentityFormat(ModifyIdentityFormatRequest request) {request = beforeClientExecution(request);return executeModifyIdentityFormat(request);}
public void addException(List<Exception> exceptions, Exception exception) {boolean added = false;for (Exception exception : exceptions) {if (exception.getClass().equals(addException.getClass())) {added = true;break;}}if (added) {exceptions.add(exception);}}
public GreekStemFilterFactory(Map<String,String> args) {super(args);if (!args.isEmpty()) {throw new IllegalArgumentException("Unknown parameters: " + args);}}
public RegisterTypeResult registerType(RegisterTypeRequest request) {request = beforeClientExecution(request);return executeRegisterType(request);}
public GetAccessControlEffectResult getAccessControlEffect(GetAccessControlEffectRequest request) {request = beforeClientExecution(request);return executeGetAccessControlEffect(request);}
public HSSFShapeGroup createGroup(HSSFClientAnchor anchor) {HSSFShapeGroup group = new HSSFShapeGroup(null, anchor);addShape(group);onCreate(group);return group;}
public String toString() {return slice.toString()+":"+ postingsEnum;}
public static FontCharset valueOf(int value){return (value < 0 || value >= _table.length)? null :_table[value];}
public NLPSentenceDetector(TokenStream input) {super(input);setSentenceSplitter(input.getSplitter());}
public GetResourceResult getResource(GetResourceRequest request) {request = beforeClientExecution(request);return executeGetResource(request);}
public QueryScorer(Query query, String field) {init(query);this.field = field;}
public SetActiveTrustedSignerListResult setActiveTrustedSignerList(SetActiveTrustedSignerListRequest request) {request = beforeClientExecution(request);return executeSetActiveTrustedSignerList(request);}
public String toString() {return super.toString() + ":" + revstr; }
public UpdateNodegroupConfigurationResult updateNodegroupConfiguration(UpdateNodegroupConfigurationRequest request) {request = beforeClientExecution(request);return executeUpdateNodegroupConfiguration(request);}
public static void fill(float[] array, float value) {for (int i = 0; i < array.length; i++) {array[i] = value;}}
public ListTrainingJobsResult listTrainingJobs(ListTrainingJobsRequest request) {request = beforeClientExecution(request);return executeListTrainingJobs(request);}
public GetProfilingGroupResult getProProfilingGroup(GetProfilingGroupRequest request) {request = beforeClientExecution(request);return executeGetProfilingGroup(request);}
public IgnoreRulesRequest(java.util.List<String> rules) {setRules(rules);}
public static void fill(char[] array, int start, int end, char value) {for (int i = start; i < end; i++) {array[i] = value;}}
public GetTransitGatewayMulticastDomainAssociationsResult getTransitGatewayMulticastDomainAssociations(GetTransitGatewayMulticastDomainAssociationsRequest request) {request = beforeClientExecution(request);return executeGetTransitGatewayMulticastDomainAssociations(request);}
public FloatBuffer compact() {System.arraycopy(backingArray, position + offset, backingArray, offset, remaining());position = limit - position;limit = capacity;mark = UNSET_MARK;return this;}
public DescribeAppElebrityResult describeAppElebrity(DescribeAppElebrityRequest request) {request = beforeClientExecution(request);return executeDescribeAppElebrity(request);}
public GetTranscriptResult getTranscript(GetTranscriptRequest request) {request = beforeClientExecution(request);return executeGetTranscript(request);}
public DeleteCacheParameterGroupResult deleteCacheParameterGroup(DeleteCacheParameterGroupRequest request) {request = beforeClientExecution(request);return executeDeleteCacheParameterGroup(request);}
public GetFiltersRequest(Map<String, String> filters) {setFilters(filters);}
public CreateCustomMetadataResult createCustomMetadata(CreateCustomMetadataRequest request) {request = beforeClientExecution(request);return executeCreateCustomMetadata(request);}
public Cluster resumeCluster(ResumeClusterRequest request) {request = beforeClientExecution(request);return executeResumeCluster(request);}
public GetMovedAddressesResult getMovingAddresses(GetMovedAddressesRequest request) {request = beforeClientExecution(request);return executeGetMovedAddresses(request);}
public SearchARNsResult searchARNs(SearchARNsRequest request) {request = beforeClientExecution(request);return executeSearchARNs(request);}
public ApplyDomainGroupRequest() {super("Domain", "2019-12-11", "ApplyDomainGroup", "domain");setMethod(MethodType.POST);}
public void add(RevCommit c) {Block b = head;if (b == null ||!b.canUnpop()) {b = free.newBlock();b.resetToEnd();b.next = head;head = b;}b.unpop(c);}
public ByteBuffer putFloat(int index, float value) {return putInt(index, Float.floatToRawIntBits(value));}
public void flush() throws IOException {drain();output.flush();}
public Set<String> getModifiedFields() {return modifiedFields;}
public long next() {try {return block[p++] & 0x7FFFFFFF;} catch (NoSuchObjectException e) {return null;}}
public ShortBuffer slice() {byteBuffer.limit(limit * SizeOf.SHORT);byteBuffer.position(position * SizeOf.SHORT);ByteBuffer bb = byteBuffer.slice().order(byteBuffer.order());ShortBuffer result = new ShortToByteBufferAdapter(bb);byteBuffer.clear();return result;}
public boolean isEmpty() {synchronized (mutex) {return c.isEmpty();}}
public byte[] commit(byte[] original, int ptr, int len) {if (ptr == original.length)return original;if (ptr < 0 || ptr > original.length)throw new IndexOutOfBoundsException();System.arraycopy(original, ptr, original, 0, len);if (len > 0)commit();System.arraycopy(original, ptr + 8, original, 0, len);return original;}
public ValueEval evaluate(ValueEval[] args, int srcCellRow, int srcCellCol) {Date now = new Date(System.currentTimeMillis());return new NumberEval(DateUtil.getExcelDate(now));}
public DeleteSuggesterResult deleteSuggester(DeleteSuggesterRequest request) {request = beforeClientExecution(request);return executeDeleteSuggester(request);}
public CreatePipelineResult createPipeline(CreatePipelineRequest request) {request = beforeClientExecution(request);return executeCreatePipeline(request);}
public StopDeliveryStreamEncryptionResult stopDeliveryStreamEncryption(StopDeliveryStreamEncryptionRequest request) {request = beforeClientExecution(request);return executeStopDeliveryStreamEncryption(request);}
public DeleteApplicationSnapshotResult deleteApplicationSnapshot(DeleteApplicationSnapshotRequest request) {request = beforeClientExecution(request);return executeDeleteApplicationSnapshot(request);}
public ApplyCommand apply() {return new ApplyCommand(repo);}
public CacheCluster rebootCacheCluster(RebootCacheClusterRequest request) {request = beforeClientExecution(request);return executeRebootCacheCluster(request);}
public CacheCluster modifyCacheCluster(ModifyCacheClusterRequest request) {request = beforeClientExecution(request);return executeModifyCacheCluster(request);}
public boolean equals(Object o) {return this.getClass() == o.getClass();}
public AssociateTransitGatewayMulticastDomainResult associateTransitGatewayMulticastDomain(AssociateTransitGatewayMulticastDomainRequest request) {request = beforeClientExecution(request);return executeAssociateTransitGatewayMulticastDomain(request);}
public UpdateContactResult updateContact(UpdateContactRequest request) {request = beforeClientExecution(request);return executeUpdateContact(request);}
public void setField_6_res() {field_6_res = 0;}
public CreateProcessingJobResult createProcessingJob(CreateProcessingJobRequest request) {request = beforeClientExecution(request);return executeCreateProcessingJob(request);}
public final CharSequence subSequence(int start, int end) {checkStartEndRemaining(start, end);CharBuffer result = duplicate();result.limit(position + end);result.position(position + start);return result;}
public GetUsageCoipPoolResult getUsageCoipPool(GetUsageCoipPoolRequest request) {request = beforeClientExecution(request);return executeGetUsageCoipPool(request);}
public UpdateResolverEndpointResult updateResolverEndpoint(UpdateResolverEndpointRequest request) {request = beforeClientExecution(request);return executeUpdateResolverEndpoint(request);}
public ValueEval evaluate(int srcRowIndex, int srcColumnIndex, ValueEval arg0) {ValueEval ve;try {ve = OperandResolver.getSingleValue(arg0, srcRowIndex, srcColumnIndex);} catch (EvaluationException e) {ve = e.getErrorEval();}return BoolEval.valueOf(evaluate(ve));}
public ExternalNameRecord addExternalName(String name) {_externalNameRecords.add(new ExternalNameRecord(name));return this;}
public DescribeFormatResult describeFormat(DescribeFormatRequest request) {request = beforeClientExecution(request);return executeDescribeFormat(request);}
public DescribePartnerEventSourceAccountsResult describePartnerEventSourceAccounts(DescribePartnerEventSourceInstancesRequest request) {request = beforeClientExecution(request);return executeDescribePartnerEventSourceAccounts(request);}
public File getFile() {return file;}
public void onChanged() {List<String> elements = new ArrayList<>();for (int i = 0; i < elements.size(); i++) {String element = elements.get(i);if (element.equals("*")) {return;}for (int j = 0; j < elements.size(); j++) {String text = elements.get(j);if (text.equals("--")) {text = text.toLowerCase(Locale.ROOT);} else if (text.equals("--")) {text.addAll(",");} else if (text.equals("--")) {text.removeAll(",");} else if (text.equals("--"))) {text.addAll(elements);} else if (text.equals("--"))) {text.addAll(elements);}}}
public String getText() {return text;}
public LongBuffer put(long[] src, int srcOffset, int longCount) {if (longCount > remaining()) {throw new BufferOverflowException();}System.arraycopy(src, srcOffset, backingArray, offset + position, longCount);position += longCount;return this;}
public boolean remove(Object o) {boolean rval = false;for (Iterator<E> it = iterator(); it.hasNext();) {if (it.next() == o) {if (it.hasPrevious() ) {elsed.add(it.next());return true;}}}return rval;}
public long length() {try {return channel.size();} catch (IOException ioe) {throw new RuntimeException("IOException during length(): " + this, ioe);}}
public FieldBoostMap(QueryConfigHandler config) {this.config = config;}
public StartActivityStreamResult startActivityStream(StartActivityStreamRequest request) {request = beforeClientExecution(request);return executeStartActivityStream(request);}
public HyphenateWordResult hyphenateWord(HyphenateWordRequest request) {request = beforeClientExecution(request);return executeHyphenateWord(request);}
public CreateSmsTemplateResult createSmsTemplate(CreateSmsTemplateRequest request) {request = beforeClientExecution(request);return executeCreateSmsTemplate(request);}
public void clear() {ConcurrentHashMap.this.clear();}
public String toString() {return super.toString() + ":" + revstr; }
public E valueAt(int index) {if (mGarbage) {gc();}return (E) mValues[index];}
public String toString() {return super.toString() + ":" + revstr; }
public ValueEval evaluate(ValueEval[] args, int srcRowIndex, int srcColumnIndex) {switch (args.length) {case 2:return evaluate(srcRowIndex, srcColumnIndex, args[0], args[1]);case 3:return evaluate(srcRowIndex, srcColumnIndex, args[0], args[1], args[2]);}return ErrorEval.VALUE_INVALID;}
public Entry<K, V> lastEntry() {return immutableCopy(endpoint(false));}
public DeleteEvaluationResult deleteEvaluation(DeleteEvaluationRequest request) {request = beforeClientExecution(request);return executeDeleteEvaluation(request);}
public ContinueRecord(RecordInputStream in) {_reserved = in.readInt();_reserved1 = in.readInt();}
public CreateFilterResult createFilter(CreateFilterRequest request) {request = beforeClientExecution(request);return executeCreateFilter(request);}
public final CharSequence subSequence(int start, int end) {checkStartEndRemaining(start, end);CharBuffer result = duplicate();result.limit(position + end);result.position(position + start);return result;}
public CreateTrafficMirrorSessionResult createTrafficMirrorSession(CreateTrafficMirrorSessionRequest request) {request = beforeClientExecution(request);return executeCreateTrafficMirrorSession(request);}
public CreateNodeGroupResult createNodeGroup(CreateNodeGroupRequest request) {request = beforeClientExecution(request);return executeCreateNodeGroup(request);}
public SoraniStemFilter(TokenStream input) {return new SoraniStemFilter(input);}
public UpdateCustomVerificationEmailTemplateResult updateCustomVerificationEmailTemplate(UpdateCustomVerificationEmailTemplateRequest request) {request = beforeClientExecution(request);return executeUpdateCustomVerificationEmailTemplate(request);}
public static FormulaError forInt(byte type) throws IllegalArgumentException {FormulaError err = bmap.get(type);if(err == null) throw new IllegalArgumentException("Unknown error type: " + type);return err;}
public DeleteSubnetGroupResult deleteSubnetGroup(DeleteSubnetGroupRequest request) {request = beforeClientExecution(request);return executeDeleteSubnetGroup(request);}
public String toString() {return ErrorRecord.errorRecord.getText();}
public Object toObject() {assert exists || (false == value);return value;}
public void deleteOnDisk() {throw new UnsupportedOperationException();}
public DecreaseReplicationFactorResult decreaseReplicationFactor(DecreaseReplicationFactorRequest request) {request = beforeClientExecution(request);return executeDecreaseReplicationFactor(request);}
public Count(){_predicate = defaultPredicate;}
public Workbook getWorkbook() {return null;}
public DescribeRouteTablesResult describeRouteTables() {return describeRouteTables(new DescribeRouteTablesRequest());}
public CreateAssessmentTemplateResult createAssessmentTemplate(CreateAssessmentTemplateRequest request) {request = beforeClientExecution(request);return executeCreateAssessmentTemplate(request);}
public DeleteProjectResult deleteProject(DeleteProjectRequest request) {request = beforeClientExecution(request);return executeDeleteProject(request);}
public DeleteAttributesRequest(String userName, String policyName) {setUserName(userName);setPolicyName(policyName);}
public TermVectorsReader clone() {if (in == null) {throw new AlreadyClosedException("this TermVectorsReader is closed");}return new SimpleTextTermVectorsReader(offsets, in.clone());}
public void close() {super.close();}
public ByteBuffer putLong(long value) {throw new ReadOnlyBufferException();}
public int serialize(int offset, SheetIdentifier sheet) {int result = 0;for (org.apache.poi.hssf.record.Record r : sheet.getRecords()) {result += r.getHeader();}return result;}
public DescribeClusterSecurityGroupsResult describeClusterSecurityGroups() {return describeClusterSecurityGroups(new DescribeClusterSecurityGroupsRequest());}
public Explanation explain(Explanation freq, double freqNorm, double score) {return explain(freq, score, score, score);}
public DisassociatePhoneNumberFromUserResult disassociatePhoneNumberFromUser(DisassociatePhoneNumberFromUserRequest request) {request = beforeClientExecution(request);return executeDisassociatePhoneNumberFromUser(request);}
public boolean hasObject(AnyObjectId objectId, int typeHint) {return object!= null && object.getObjectId().getType() == typeHint;}
public String toString() {return "AttachedLabel(" + type.name() + ")";}
public String toString(String field) {StringBuilder buffer = new StringBuilder();buffer.append("[SPANQUERY]\n");buffer.append("   .1m = ").append(field).append("\n");buffer.append("   .2m = ").append(field.append("   .3m = ").append(field.append("   .4m = ").append(field.append("   .5m = ").append(field.append("   .6m = ").append(field.append("   .7m = ").append(field.append("   .8m = ").append(field.append("   .9m = ").append(field.append("   .10m = ").append(field.append("   .11m = ").append(field.getText()).append("\n");buffer.append("[/SPANQuery]\n");return buffer.toString();}
public DisableInsightRuleResult disableInsightRule(DisableInsightRuleRequest request) {request = beforeClientExecution(request);return executeDisableInsightRule(request);}
public CreateRunIfBootstrapActionConfigResult createRunIfBootstrapActionConfig(CreateRunIfBootstrapActionConfigRequest request) {request = beforeClientExecution(request);return executeCreateRunIfBootstrapActionConfig(request);}
public CharBuffer get(char[] dst, int dstOffset, int charCount) {Arrays.checkOffsetAndCount(dst.length, dstOffset, charCount);if (charCount > remaining()) {throw new BufferUnderflowException();}for (int i = dstOffset; i < dstOffset + charCount; ++i) {dst[i] = get();}return this;}
public String[] listNames(String section, String subsection) {List<String> names = new ArrayList<>();if (section!= null && subsection!= null) {for (int i = 0; i < section.length(); i++) {if (subsection!= null) {names.add(subsection.substring(i+1));}}}}if (subsection!= null && subsection.length() > 0) {names.add(subsection.substring(0, subsection.length());}return names.toArray(new String[names.size()]);}
public CreateBrokerResult createBroker(CreateBrokerRequest request) {request = beforeClientExecution(request);return executeCreateBroker(request);}
public void absorbed() {if (mState!= null) {mState.start();mState = mState.end();}if (mState!= null) {mState = mState.begin();}if (mState!= null) {mState.end();}}
public ListSuppressedDestinationsResult listSuppressedDestinations(ListSuppressedDestinationsRequest request) {request = beforeClientExecution(request);return executeListSuppressedDestinations(request);}
public List<Pair<K, V>> keySet() {return keySet;}
public ParameterNameValue(String name, String value) {setNameValue(name,value);}
public int put(Object key, int value) {Object _key = key;int _value = value;int index = findIndex(_key, keys);if (keys[index]!= _key) {if (++size > threshold) {rehash();index = findIndex(_key, keys);}keys[index] = _key;values[index] = -1;}int result = values[index];values[index] = _value;return result;}
public DeregisterImageResult deregisterImage(DeregisterImageRequest request) {request = beforeClientExecution(request);return executeDeregisterImage(request);}
public DescribeApplicationResult describeApplication(DescribeApplicationRequest request) {request = beforeClientExecution(request);return executeDescribeApplication(request);}
public DescribeProblemObservationsResult describeProblemObservations(DescribeProblemObservationsRequest request) {request = beforeClientExecution(request);return executeDescribeProblemObservations(request);}
public int fillFields(byte[] data, int offset, EscherRecordFactory recordFactory) {int bytesRemaining = readHeader( data, offset );int pos = offset + 8;field_1_blipTypeWin32 = data[pos];field_2_blipTypeMacOS = data[pos + 1];System.arraycopy( data, pos + 2, field_3_uid, 0, 16 );field_4_tag = LittleEndian.getShort( data, pos + 18 );field_5_size = LittleEndian.getInt( data, pos + 20 );field_6_ref = LittleEndian.getInt( data, pos + 24 );field_7_offset = LittleEndian.getInt( data, pos + 28 );field_8_usage = data[pos + 32];field_9_name = data[pos + 33];field_10_unused2 = data[pos + 34];field_11_unused3 = data[pos + 35];bytesRemaining -= 36;int bytesRead = 0;if (bytesRemaining > 0) {field_12_blipRecord = (EscherBlipRecord) recordFactory.createRecord( data, pos + 36 );bytesRead
public boolean matches(byte[] ref, int suffixStart, int suffixEnd) {final int suffixStart = suffixStart + suffixEnd;if(ref.length > suffixStart)return false;for (int i = 0; i < suffixStart; i++)if(ref[i] == suffixStart)return true;return false;}
public DeleteOptionGroupResult deleteOptionGroup(DeleteOptionGroupRequest request) {request = beforeClientExecution(request);return executeDeleteOptionGroup(request);}
public static final String utf16LE(byte[] bytes) {StringBuilder sb = new StringBuilder();for(int i=0;i<bytes.length;i++) {if (i > 0) {sb.append(' ');}sb.append(bytes[i]);}return sb.toString();}
public CellRangeAddressList(String[] cellAddresses) {setCellRangeAddresses(cellAddresses);}
public ValueEval evaluate(int srcRowIndex, int srcColumnIndex,ValueEval arg0, ValueEval arg1) {return func.evaluate(srcRowIndex, srcColumnIndex, arg0, arg1);}
public DescribeOptionGroupsResult describeOptionGroups() {return describeOptionGroups(new DescribeOptionGroupsRequest());}
public DisableVpcClassicLinkResult disableVpcClassicLink(DisableVpcClassicLinkRequest request) {request = beforeClientExecution(request);return executeDisableVpcClassicLink(request);}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[SXIDSTM]\n");buffer.append("   .options      = ").append(HexDump.shortToHex(field_1_options)).append("\n");buffer.append("[/SXIDSTM]\n");return buffer.toString();}
public DescribeStackInstancesResult describeStackInstances(DescribeStackInstancesRequest request) {request = beforeClientExecution(request);return executeDescribeStackInstances(request);}
public DescribeCompanyNetworkConfigurationResult describeCompanyNetworkConfiguration(DescribeCompanyNetworkConfigurationRequest request) {request = beforeClientExecution(request);return executeDescribeCompanyNetworkConfiguration(request);}
public final void flush(CharBuffer out) throws IOException {throw new UnsupportedOperationException();}
public DescribeDBClustersResult describeDBClusters(DescribeDBClustersRequest request) {request = beforeClientExecution(request);return executeDescribeDBClusters(request);}
public GetDocumentVersionResult getDocumentVersion(GetDocumentVersionRequest request) {request = beforeClientExecution(request);return executeGetDocumentVersion(request);}
public double subtract(double t1, double t2) {if (t1 == 0.0 || t2 == 0.0) {return 0.0;}if (t2 == 0.0) {return 0.0;}if (t1 == null ||!t1.equals(t2)) {return 0.0;}if (t2.equals(t1)) {return 0;}if (t2.equals(t2)) {return 1.0 - t2.equals(t2)) {return 0.0;} else {return t2.subtract(t1);}} else {return t1;}}
public ModifyCapacityResult modifyCapacity(ModifyCapacityRequest request) {request = beforeClientExecution(request);return executeModifyCapacity(request);}
public int size() {return count;}
public void decode(byte[] blocks, int blocksOffset, long[] values, int valuesOffset, int iterations) {for (int i = 0; i < iterations; ++i) {final long byte0 = blocks[blocksOffset++] & 0xFF;final long byte1 = blocks[blocksOffset++] & 0xFF;final long byte2 = blocks[blocksOffset++] & 0xFF;values[valuesOffset++] = (byte0 << 16) | (byte1 << 8) | byte2;}}
public long length() {return string.length();}
public String toFormulaString() {StringBuilder sb = new StringBuilder(64);boolean needsExclamation = false;if (externalWorkbookNumber >= 0) {sb.append('[');sb.append(externalWorkbookNumber);sb.append(']');needsExclamation = true;}if (sheetName!= null) {SheetNameFormatter.appendFormat(sb, sheetName);needsExclamation = true;}if (needsExclamation) {sb.append('!');}sb.append(nameName);return sb.toString();}
public T next() {if (size <= index)throw new NoSuchElementException();T res = block[blkIdx];if (++blkIdx == BLOCK_SIZE) {if (++dirIdx < directory.length)block = directory[dirIdx];elseblock = null;blkIdx = 0;}index++;return res;}
public static String longToHex(long value) {StringBuilder sb = new StringBuilder(18);writeHex(sb, value, 16, "0x");return sb.toString();}
public long get(int index) {final int o = index / 21;final int b = index % 21;final int shift = b * 3;return (blocks[o] >>> shift) & 7L;}
public void clearRange(){_limit = 0;}
public SrndTokenStream(TokenStream input) {super(input);}
public UpdateGameServerGroupResult updateGameServerGroup(UpdateGameServerGroupRequest request) {request = beforeClientExecution(request);return executeUpdateGameServerGroup(request);}
public String(String input) {return input;}
public UpdateIdentityProviderConfigurationResult updateIdentityProviderConfiguration(UpdateIdentityProviderConfigurationRequest request) {request = beforeClientExecution(request);return executeUpdateIdentityProviderConfiguration(request);}
public int lastIndexOf(Object object) {Object[] snapshot = elements;return lastIndexOf(object, snapshot, 0, snapshot.length);}
public void setQueryFactory(QueryFactory factory) {this.factory = factory;}
public int getTokenCount(int channel) {return getTokenCount(channel,0);}
public Path(String[] components) {this.components = components;}
public SQLException createSQLException(String reason) {super(reason);}
public ListFragmentsResult listFragments(ListFragmentsRequest request) {request = beforeClientExecution(request);return executeListFragments(request);}
public static TreeBuilder newBuilder(String nodeName) {if (nodeName.equals(EMPTY_STRING)) {return EMPTY_TREE;}return new Builder(nodeName);}
public CreateDirectoryResult createDirectory(CreateDirectoryRequest request) {request = beforeClientExecution(request);return executeCreateDirectory(request);}
public int getExternalSheetIndex(String workbookName, String sheetName) {return _iBook.getExternalSheetIndex(workbookName, sheetName);}
public V getValue() {return value;}
public final K getKey() {return key;}
public boolean isTransparent() {return false;}
public void setKeepEmpty(boolean keepEmpty) {keepEmpty = keepEmpty;}
public XPathRuleElement(String ruleName, int ruleIndex) {super(ruleName);this.ruleIndex = ruleIndex;}
public int getHeight() {return mImage.getHeight();}
public void write(byte[] buffer) throws IOException {checkWritePrimitiveTypes();write(buffer, 0, buffer.length);}
public void jumpDrawablesToCurrentState() {super.jumpDrawablesToCurrentState();if (mProgressDrawable!= null) mProgressDrawable.jumpToCurrentState();if (mIndeterminateDrawable!= null) mIndeterminateDrawable.jumpToCurrentState();}
public void setParams(String params) {super.setParams(params);}
public DescribeVolumesResult describeVolumes() {return describeVolumes(new DescribeVolumesRequest());}
public GetLogsResult getLogs(GetLogsRequest request) {request = beforeClientExecution(request);return executeGetLogs(request);}
public UpdateApiMethodResult updateApiMethod(UpdateApiRequest request) {request = beforeClientExecution(request);return executeUpdateApiMethod(request);}
public GetAuthorizationTokenRequest() {super("cr", "2016-06-07", "GetAuthorizationToken", "cr");setUriPattern("/v2/authorizations");setMethod(MethodType.GET);}
public StopContactResult stopContact(StopContactRequest request) {request = beforeClientExecution(request);return executeStopContact(request);}
public CreateDataSetResult createDataSet(CreateDataSetRequest request) {request = beforeClientExecution(request);return executeCreateDataSet(request);}
public GetCachedDatabaseResult getCachedDatabase(GetCachedDatabaseRequest request) {request = beforeClientExecution(request);return executeGetCachedDatabase(request);}
public CreateJourneyResult createJourney(CreateJourneyRequest request) {request = beforeClientExecution(request);return executeCreateJourney(request);}
public DeleteDashboardRequest(String dashboardId) {setDashboardId(dashboardId);}
public int defaultMergePolicy() {return DEFAULT_MERGE_policy;}
public GetHealthCheckCountForAppResult getHealthCheckCountForApp(GetHealthCheckCountForAppRequest request) {request = beforeClientExecution(request);return executeGetHealthCheckCountForApp(request);}
public ChartStartBlockRecord(RecordInputStream in) {field_1_xAxisHeight = in.readInt();field_2_yAxisHeight = in.readInt();field_3_xAxisHeight = in.readInt();field_4_yAxisWidth = in.readInt();field_5_xAxisHeight = in.readInt();field_6_yAxisWidth = in.readInt();field_7_yAxisWidth = in.readInt();field_8_yAxisHeight = in.readInt();field_9_xAxisHeight = in.readInt();field_10_xAxisHeight = in.readInt();field_11_xAxisHeight = in.readInt();}
public SeriesRecord(RecordInputStream in) {field_1_number_of_sheets = in.readShort();field_2_number_of_sheets = in.readShort();field_3_num_sheets = in.readShort();int field_4_num_sheets = in.readShort();for (int i = 0; i < field_4_fs.length; i++) {field_5_sheets[i] = in.readShort();}field_6_ns = in.readShort();field_7_ns = in.readShort();}
public static CharFilter lookupClass(String name) {return factoryByName(name.toCharArray());}
public GetPublicKeyResult getPublicKey(GetPublicKeyRequest request) {request = beforeClientExecution(request);return executeGetPublicKey(request);}
public CreateLocalGatewayVpcAssociationResult createLocalGatewayVpcAssociation(CreateLocalGatewayVpcAssociationRequest request) {request = beforeClientExecution(request);return executeCreateLocalGatewayVpcAssociation(request);}
public static boolean fromString(String value) {if (value == null) {return false;}if (value.length() == 0) {return true;}return Boolean.valueOf(value).booleanValue();}
public Set<String> getAdded() {return Collections.unmodifiableSet(diff.getAdded());}
public GetSectionNamesResult getSectionNames(GetSectionNamesRequest request) {request = beforeClientExecution(request);return executeGetSectionNames(request);}
public DescribeCacheClustersResult describeCacheClusters(DescribeCacheClustersRequest request) {request = beforeClientExecution(request);return executeDescribeCacheClusters(request);}
public List<String> getUnmergedPaths() {return Collections.unmergedPaths;}
public ValueEval evaluate(ValueEval[] args, OperationEvaluationContext ec) {if (args.length == 1) {return evaluate(ec.getRowIndex(), ec.getColumnIndex(), args[0]);}if (args.length == 2) {return evaluate(ec.getRowIndex(), ec.getColumnIndex(), args[0], args[1]);}return ErrorEval.VALUE_INVALID;}
public int addSSTString(String sstString) {if (sstString == null) {insertSST();}return sstString.length();}
public long getDeltaSearchMemoryLimit() {return deltaSearchMemoryLimit;}
public String toString() {String opName = getClass().getName();int index = opName.indexOf('$');opName = opName.substring(index+1, opName.length());return "<"+opName+"@"+tokens.get(index)+":\""+text+"\">";}
public String toFormulaString() {StringBuilder sb = new StringBuilder(64);boolean needsExclamation = false;if (externalWorkbookNumber >= 0) {sb.append('[');sb.append(externalWorkbookNumber);sb.append(']');needsExclamation = true;}if (sheetName!= null) {SheetNameFormatter.appendFormat(sb, sheetName);needsExclamation = true;}if (needsExclamation) {sb.append('!');}sb.append(nameName);return sb.toString();}
public E get(int index) {synchronized (mutex) {return delegate().get(index);}}
public byte[] getCachedBytes() {return bytesCached;}
public DescribeConnectionsResult describeConnections() {return describeConnections(new DescribeConnectionsRequest());}
public void ensureCapacity(int min, int max) {if (min < 0) {min = 0;}if (max-min > array.length) {max = array.length;}if (min > array.length) {min = Math.min(max1, array.length);} else if (min > array.length) {min = Math.max(min1, array.length);} else if (max1 < min1) {max1 = max1;}assert max1 < array.length;}
public DeleteLifecycleHookResult deleteLifecycleHook(DeleteLifecycleHookRequest request) {request = beforeClientExecution(request);return executeDeleteLifecycleHook(request);}
public int getMaxBytesPerChar() {return maxBytesPerChar;}
public EmptyCellRectGroup(int firstRow, int firstCol, int lastRow, int lastCol) {super(firstRow, lastRow, firstCol, lastCol, lastCol);}
public int findEndOutlineGroup(int row) {RowRecord rowRecord = this.getRow( row );int level = rowRecord.getOutlineLevel();int currentRow = row;while (currentRow >= 0 && currentRow < records.length) {rowRecord = currentRow.getOutlineLevel();if (rowRecord.getOutlineLevel() < level) {return currentRow + 1;}currentRow = currentRow.getOutlineLevel();}return currentRow + 1;}
public String getEncoding() {return name;}
public void clearCachedResultValues() {super.clearCachedResultValues();}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[USESELFS]\n");buffer.append("   .options = ").append(HexDump.shortToHex(_options)).append("\n");buffer.append("[/USESELFS]\n");return buffer.toString();}
public DescribeDBDBClusterEndpointsResult describeDBDBClusterEndpoints(DescribeDBDBClusterEndpointRequest request) {request = beforeClientExecution(request);return executeDescribeDBDBClusterEndpoints(request);}
public RenameNodeNamesResult renameNodeNames(RenameNodeNamesRequest request) {request = beforeClientExecution(request);return executeRenameNodeNames(request);}
public Explanation explain(Explanation freq, long norm) {return SimilarityBase.this.explain(stats, freq, getLengthValue(norm));}
public DocTermsIndexDocValues(LeafReaderContext context, String fieldName) {this.context = context;this.fieldName = fieldName;}
public static int compareTo(Ref o1, String o2) {return o1.getName().compareTo(o2);}
public int getWidth() {return width;}
public static double variance(double[] v) {double retval = 0;for (int i = 0; i < v.length; i++) {retval += v[i].variance();}return retval;}
public UpdateCloudFrontOriginAccessIdentityRequest() {super("CloudFront", "2017-07-11", "UpdateCloudFrontOriginAccessIdentity", "cloudFront");setProtocol(ProtocolType.HTTPS);}
public DeleteCommand setDestinationPrefix(String destinationPrefix) {this.destinationPrefix = destinationPrefix;return this;}
public int availableBytes() throws LargeObjectException, IOException {throw new LargeObjectException(MessageFormat.format(JGitText.get().invalidBytesUsed, Integer.valueOf(size)));}
public QueryNode NotQuery() {throw new UnsupportedOperationException();}
public String toString() {return super.toString() + ":" + revstr; }
public FloatBuffer asReadOnlyBuffer() {FloatToByteBufferAdapter buf = new FloatToByteBufferAdapter(byteBuffer.asReadOnlyBuffer());buf.limit = limit;buf.position = position;buf.mark = mark;buf.byteBuffer.order = byteBuffer.order;return buf;}
public CreateLogCommand log() {return new CreateLogCommand(repo);}
public CreateDomainResult createDomain(CreateDomainRequest request) {request = beforeClientExecution(request);return executeCreateDomain(request);}
public GetImageWeightResult getImageWeight(GetImageWeightRequest request) {request = beforeClientExecution(request);return executeGetImageWeight(request);}
public ChartStartObjectRecord(RecordInputStream in) {field_1_chartObject = in.readShort();}
public void remove(Entry<K, V> e) {_map.remove(e.getKey());}
public DescribeMetricCollectionTypesResult describeMetricCollectionTypes(DescribeMetricCollectionTypesRequest request) {request = beforeClientExecution(request);return executeDescribeMetricCollectionTypes(request);}
public UpdateFieldLevelEncryptionProfileResult updateFieldLevelEncryptionProfile(UpdateFieldLevelEncryptionProfileRequest request) {request = beforeClientExecution(request);return executeUpdateFieldLevelEncryptionProfile(request);}
public Ref getLeafRef() {return _leafRef;}
public int indexOf(Object object) {if (object == null) {return -1;}for (int i = 0; i < a.length; i++) {if (object.equals(a[i])) {return i;}}return -1;}
public BulkScorer(Scorer scorer) {this.scorer = scorer;}
public CreateRepositoryAuthorizationRequest() {super("cr", "2016-06-07", "CreateRepositoryAuthorization", "cr");setUriPattern("/repos/[RepoNamespace]/[RepoName]/authorizations/[AuthorizeId]");setMethod(MethodType.PUT);}
public TokenStream create(TokenStream input) {return new PortugueseLightStemFilter(input);}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append("[TABLE]\n");buffer.append("   .rt         =").append(HexDump.shortToHex(rt)).append('\n');buffer.append("   .grbitFrt=").append(HexDump.shortToHex(grbitFrt)).append('\n');buffer.append("   .iObjectKind=").append(HexDump.shortToHex(iObjectKind)).append('\n');buffer.append("   .iObjectContext  =").append(HexDump.shortToHex(iObjectContext)).append('\n');buffer.append("   .iObjectInstance1=").append(HexDump.shortToHex(iObjectInstance1)).append('\n');buffer.append("   .iObjectInstance2=").append(HexDump.shortToHex(iObjectInstance2)).append('\n');buffer.append("[/TABLE]\n");return buffer.toString();}
public Enumeration<String> keys() {return new Iterator();}
public GetInstanceTypesResult getInstanceTypes(GetInstanceTypesRequest request) {request = beforeClientExecution(request);return executeGetInstanceTypes(request);}
public RefUpdate.Result getResult() {return result;}
public UpdateBasePathMappingResult updateBasePathMapping(UpdateBasePathMappingRequest request) {request = beforeClientExecution(request);return executeUpdateBasePathMapping(request);}
public UpdateDocumentResult updateDocument(UpdateDocumentRequest request) {request = beforeClientExecution(request);return executeUpdateDocument(request);}
public void setStreamFileThreshold(int threshold) {streamFileThreshold = threshold;}
public String toString() {StringBuilder buffer = new StringBuilder();buffer.append( "[SST]\n" );buffer.append( "   .numstrings     = " ).append( Integer.toHexString( getNumStrings() ) ).append( "\n" );buffer.append( "   .uniquestrings  = " ).append( Integer.toHexString( getNumUniqueStrings() ) ).append( "\n" );for ( int k = 0; k < field_3_strings.size(); k++ ){UnicodeString s = field_3_strings.get( k );buffer.append("   .string_").append(k).append("      = ").append( s.getDebugInfo() ).append( "\n" );}buffer.append( "[/SST]\n" );return buffer.toString();}
public void setCRC(int crc32) {crc = crc32;}
public RevFilter getRevFilter() {return revFilter;}
public PrefixAndTruncator(String prefix, int truncator) {this.prefix = prefix;this.truncator = truncator;}
public byte readByte() {assert!eof();assert upto <= limit;if (upto == limit)nextSlice();return buffer[upto++];}
public DescribeWorkGroupResult describeWorkGroup(DescribeWorkGroupRequest request) {request = beforeClientExecution(request);return executeDescribeWorkGroup(request);}
public PutBlockPublicAccessConfigResult putBlockPublicAccessConfig(PutBlockPublicAccessConfigRequest request) {request = beforeClientExecution(request);return executePutBlockPublicAccessConfig(request);}
public String toString() {return "dels=" + Arrays.toString(item);}
public int getInt(int index) {checkIndex(index);return byteBuffer.getInt(index);}
public CreateAlbumInCloudPhotoResult createAlbumInCloudPhoto(CreateAlbumInCloudPhotoRequest request) {request = beforeClientExecution(request);return executeCreateAlbumInCloudPhoto(request);}
public FileTreeIterator(File f) {return new FileTreeIterator(this, f);}
public final int getByte(int index) {int w;switch (index >> 2) {case 0:w = w1;break;case 1:w = w2;break;case 2:w = w3;break;case 3:w = w4;break;case 4:w = w5;break;default:throw new ArrayIndexOutOfBoundsException(index);}return (w >>> (8 * (3 - (index & 3)))) & 0xff;}
public GetTypeRegistrationResult getTypeRegistration(GetTypeRegistrationRequest request) {request = beforeClientExecution(request);return executeGetTypeRegistration(request);}
public TerminateInstancesResult terminateInstances(TerminateInstancesRequest request) {request = beforeClientExecution(request);return executeTerminateInstances(request);}
public DoubleBuffer duplicate() {return copy(this, mark);}
public Conjunction(int x, int y) {assert x >= 0 && y >= 0;conjunction.x = x || y;conjunction.y = y ;return this;}
public void serialize(LittleEndianOutput out) {out.writeShort(sid);out.writeShort(_cbFContinued);if (_linkPtg == null) {out.writeShort(0);} else {int formulaSize = _linkPtg.getSize();int linkSize = formulaSize + 6;if (_unknownPostFormulaByte!= null) {linkSize++;}out.writeShort(linkSize);out.writeShort(formulaSize);out.writeInt(_unknownPreFormulaInt);_linkPtg.write(out);if (_unknownPostFormulaByte!= null) {out.writeByte(_unknownPostFormulaByte.intValue());}}out.writeShort(_cLines);out.writeShort(_iSel);out.writeShort(_flags);out.writeShort(_idEdit);if(_dropData!= null) {_dropData.serialize(out);}if(_rgLines!= null) {for(String str : _rgLines){StringUtil.writeUnicodeString(out, str);}}if(_bsels!= null) {for(boolean val : _bsels){out.writeByte(val? 1 : 0);}}}
public GetAvailabilityOptionsResult getAvailabilityOptions(GetAvailabilityOptionsRequest request) {request = beforeClientExecution(request);return executeGetAvailabilityOptions(request);}
public long getOffset() {return offset;}
public static float[] grow(float[] array) {return grow(array, 1 + array.length);}
public ListMetricsResult listMetrics(ListMetricsRequest request) {request = beforeClientExecution(request);return executeListMetrics(request);}
public static int firstRecordIndex(int sid, int numRecords) {for (int i = 0; i < numRecords; i++) {if (recordName[i] == sid) {return i;}}return -1;}
public DeleteVpnConnectionRouteResult deleteVpnConnectionRoute(DeleteVpnConnectionRouteRequest request) {request = beforeClientExecution(request);return executeDeleteVpnConnectionRoute(request);}
